<?php

Class Email_template_model extends Base_Model {

    public function __construct() {
        parent::__construct("email_templates");
    }

    public function GetFilesOfEmail($id) {
        $this->db->select('*');
        $this->db->from('site_images');
        $this->db->where('site_images.ImageType', 'EmailFile');
        $this->db->where('site_images.FileID', $id);
        $query = $this->db->get();
        return $query->result_array();
    }
}
