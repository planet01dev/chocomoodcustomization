<?php

Class User_notification_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("user_notifications");
    }

    public function getNotifications($user_id, $system_language_code = false)
    {
        $this->db->select("user_notifications.*,bookings.*, users_text.FullName as UserName, users.CompressedImage as UserImage, categories_text.Title as CategoryTitle, categories.Image as CategoryImage, tst.FullName as TechnicianFullName, ts.Email as TechnicianEmail, ts.Mobile as TechnicianMobile");
        $this->db->from('user_notifications');
        $this->db->join('bookings', 'user_notifications.BookingID = bookings.BookingID', 'LEFT');
        $this->db->join('users', 'bookings.UserID = users.UserID', 'LEFT');
        $this->db->join('users_text', 'users.UserID = users_text.UserID', 'LEFT');

        // technician details
        $this->db->join('users ts', 'bookings.TechnicianID = ts.UserID', 'LEFT');
        $this->db->join('users_text tst', 'ts.UserID = tst.UserID', 'LEFT');

        $this->db->join('categories', 'bookings.CategoryID = categories.CategoryID', 'left');
        $this->db->join('categories_text', 'categories.CategoryID = categories_text.CategoryID', 'left');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');
        $this->db->join('system_languages slcat', 'slcat.SystemLanguageID = categories_text.SystemLanguageID', 'Left');
        $this->db->where('user_notifications.UserID', $user_id);
        $this->db->where('system_languages.IsDefault', '1');
        if ($system_language_code) {
            $this->db->where('slcat.ShortCode', $system_language_code);
        } else {
            $this->db->where('slcat.IsDefault', '1');
        }
        $this->db->order_by('user_notifications.UserNotificationID', 'DESC');
        $result = $this->db->get();
        return $result->result_array();
    }
}