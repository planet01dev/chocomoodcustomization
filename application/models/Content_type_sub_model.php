<?php
class Content_type_sub_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("shape_content_type_sub");
    }

    public function getData($where = false, $system_language_code = false)
    {

        $this->db->select('shape_content_type_text.Title as Title,shape_content_type_sub_text.Title as subTitle,shape_content_type_sub.IsActive as IsActive,shape_content_type_sub.ContentTypeSubID as ContentTypeSubID');
        $this->db->from('shape_content_type_sub');
        $this->db->join('shape_content_type_sub_text', 'shape_content_type_sub.ContentTypeSubID = shape_content_type_sub.ContentTypeSubID', 'LEFT');
        
        $this->db->join('shape_content_type', 'shape_content_type.ContentTypeID = shape_content_type_sub.ContentTypeID', 'LEFT');
        $this->db->join('shape_content_type_text', 'shape_content_type_text.ContentTypeID = shape_content_type.ContentTypeID', 'LEFT');

        $this->db->join('system_languages', 'system_languages.SystemLanguageID = ' . $this->table . '_text.SystemLanguageID AND system_languages.SystemLanguageID = shape_content_type_text.SystemLanguageID');

        
        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }

        if ($where) {
            $this->db->where($where);
        }
        $this->db->where($this->table . '.Hide', '0');
        $result = $this->db->get();

        // echo $this->db->last_query();exit();
        return $result->result();
    }
}
