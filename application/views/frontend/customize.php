<?php if($customizations){ 
    foreach ($customizations as $key => $value) { ?>
        <section class="content customize first">
    <div class="bannerbox">
        <img src="<?php echo base_url($value->Image); ?>">
        <div class="captionbox">
            <div class="inner">
                <h2><?php echo $value->Title; ?></h2>
                <!--<?php echo $choco_msg->Description; ?>-->
                <a href="<?php echo base_url('customize/'.@$value->link);?>" class="btn btn-secondary"><?php echo lang('customize_now'); ?></a>
            </div>
        </div>
    </div>
</section>

        <?php 
    }

} ?>


<!--<section class="content three">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title">
                    <h4><?php echo lang('kilogram'); ?> <span><?php echo lang('of_high_quality_chocolate'); ?></span></h4>
                </div>
                <div class="procarousel">
                    <div class="owl-carousel owl-theme brands-slider">
                        <?php
                            foreach ($products as $product)
                            { ?>
                                <div class="item">
                                    <a href="<?php echo base_url() . 'product/detail/' . productTitle($product->ProductID); ?>">
                                        <img src="<?php echo base_url(get_images($product->ProductID, 'product', false)); ?>">
                                        <h4><?php echo $product->Title; ?></h4>
                                    </a>
                                </div>
                            <?php }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $choco_box = getPageContent(11, $lang) ?>
<section class="content customize left">
    <div class="bannerbox">
        <img src="<?php echo base_url($choco_box->Image); ?>">
        <div class="captionbox">
            <div class="inner">
                <h2><?php echo $choco_box->Title; ?></h2>
                <?php echo $choco_box->Description; ?>
                <a href="<?php echo base_url('customize/boxes/choco_box');?>" class="btn btn-secondary"><?php echo lang('customize_now'); ?></a>
            </div>
        </div>
    </div>
</section>

<?php $choco_shape = getPageContent(12, $lang) ?>
<section class="content customize">
    <div class="bannerbox">
        <img src="<?php echo base_url($choco_shape->Image); ?>">
        <div class="captionbox">
            <div class="inner">
                <h2><?php echo $choco_shape->Title; ?></h2>
                <?php echo $choco_shape->Description; ?>
                <a href="<?php echo base_url('customize/choco_shape'); ?>" class="btn btn-secondary"><?php echo lang('customize_now'); ?></a>
            </div>
        </div>
    </div>
</section>-->
