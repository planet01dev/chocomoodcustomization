<style>
body {
    overflow-x: hidden !important;
    position: inherit !important;
    overflow-y: auto !important;
}
</style>
<script src="https://www.paytabs.com/express/express_checkout_v3.js" ></script>
<section class="content checkout address done">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="alertbox">
                    <h5>
                        
                        <strong>Payment Amount : <?php echo $order['TotalAmount']; ?></strong>
                       
                       
                    </h5>

                </div>
            </div>
        </div>
        
    </div>
    
</section>
<!--<link rel="stylesheet" href="https://www.paytabs.com/theme/express_checkout/css/express.css">-->
<!--<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />-->
<div class="PT_express_checkout" style="display: none;"></div>

<script type="text/javascript">
Paytabs("#express_checkout").expresscheckout({
  settings: {
    merchant_id: "10042164",
    //merchant_id: "10066758",
    //10066758
    //10042164
    secret_key: "qhEGzYV3Lbil9hLdu3n9odvUJ6Bx60EwhicRh8U3jdosvaFZe39DhEx0K9yQ9cnrvziNHcQ1qfuVpaVEd4hvJOvuY3l7gIyMiRz0",
    //secret_key: "XTxB5CyZR3sZhYDZ5QYc50dVwySxMZhJTOkYrSYiVqidpWcuMbGScTNIsGfeFxXzlhxNaw66WojmkwQ7HxZTrMCTsS88p5HLnfyx",
    amount: "<?php echo str_replace( ',', '', $order['TotalAmount'] ); ?>",
    currency: "SAR",
    title: "<?php echo $this->session->userdata['user']->FullName; ?>",
    product_names : 'Products',
    order_id: <?php echo $order[0]->TempOrderID; ?>,
    url_redirect: "<?php echo base_url('order/updatePaymentStatus');?>",
    //url_redirect:"https://chocomood.henka.tech/",
    site_url:"https://chocomood.henka.tech/",
    display_customer_info: 1,
    display_billing_fields:0,
    display_shipping_fields: 0,
    language: "en",
    redirect_on_reject: 0,
    url_cancel:"<?php echo base_url('order/CancelPaymentPaymentOfOrder');?>"
    //url_cancel:"https://chocomood.henka.tech/"
    
  },
  customer_info: {
    first_name: "<?php echo $order[0]->FullName; ?>",
    last_name: "",
    phone_number: "",
    email_address: "<?php echo $order[0]->Email; ?>",
    country_code: "966"
  },
  billing_address: {
    full_address: "<?php echo $order[0]->FullName; ?>",
    city: "<?php echo $order[0]->UserCity; ?>",
    state:'<?php echo $order[0]->UserCity; ?>',
    country:'SAU',
    country_code: '966',
    postal_code: "<?php echo ($order[0]->POBox != '' ? $order[0]->POBox  : '11543'); ?>"
  }
});


$(document).ready(function () {
    setTimeout(function(){ $('.PT_open_popup').click(); }, 1000);
 
});
</script>
