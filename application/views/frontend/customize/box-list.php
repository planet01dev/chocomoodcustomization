  <section class=" container">
      <!-- bannar -->
      <div class="row box-item">
        <div class="col-md-12 box-image" style="background-image: url(<?= site_url('assets/frontend/images/')?>box-list-banner.jpg);">
            <div class="row align-items-end justify-content-start h-100 content-row">
                <div class="col-md-6 p-3 box-content rounded-right-top">
                    <h4 class="title">FANCY BOXES</h4>
                    <p class="description position-relative">
                        Add a touch of extravagance and originality to your chocolates with our Fantasy box that you can customize yourself!
                    </p>
                </div>
            </div>
        </div>
      </div>
        <div class="row mt-5 mb-5">
            <div class="col-md-4 wrapper border p-2">
              <!-- customize box  -->
              <a href="./customize-box-details.html">
                <!-- customize box content -->
                <div class="d-flex top">
                  <!-- 
                    customize box image
                    the first element from this box Gallary    
                  -->
                  <img class="m-auto d-block" src="<?= site_url('assets/frontend/images/')?>box.jpg" width="80%" alt="">
                </div>
                <div class="bottom">
                  <div class="left">
                    <div class="details w-100">
                      <h5 class="text-center">Rose Box 32 PCS Chocolate
                        <small>32 PCS / 115 SAR</small></h5>
                    </div>
                  </div>
                </div>
              </a>
              <!-- customize box product details -->
              <div class="inside">
                <div class="icon"><i class="fa fa-info-circle"></i></div>
                <div class="contents">
                  <table class="details-content d-block">
                    <tbody class="d-block">
                      <tr class="d-block">
                          <td class="text-left">Minimum Order</td>
                          <td class="text-left pl-3">1 KG</td>
                      </tr>
                      <tr class="d-block">
                          <td class="text-left">Price For KG</td>
                          <td class="text-left pl-3">60 SAR</td>
                      </tr>
                      <tr class="d-block">
                          <td class="text-left">Processing Time</td>
                          <td class="text-left pl-3">3 Days</td>
                      </tr>
                      <tr class="d-block">
                          <td class="text-left">Price for 1 PCS</td>
                          <td class="text-left pl-3">1.98 SAR</td>
                      </tr>
                      <tr class="d-block">
                          <td class="text-left">Quantity Per KG</td>
                          <td class="text-left pl-3">60 PCS</td>
                      </tr>                                 
                      <tr class="d-block">
                          <td class="text-left">Chocolate Type</td>
                          <td class="text-left pl-3">Brown</td>
                      </tr>                                 
                      <tr class="d-block">
                        <td class="text-left">Hight</td>
                          <td class="text-left pl-3">10mm</td>
                      </tr>                                 
                      <tr class="d-block">
                          <td class="text-left">Chocolate Type</td>
                          <td class="text-left pl-3">Brown</td>
                      </tr>                                 
                  </tbody>
                    </table>
                </div>
              </div>
            </div>
            <div class="col-md-4 wrapper border p-2">
              <!-- customize box  -->
                  <a href="customize-box-details-gm.html">
                    <!-- customize box content -->  
                    <div class="d-flex top">
                        <!-- 
                            customize box image
                            the firstt elemnt from this product Gallary    
                        -->  
                      <img class="m-auto d-block" src="<?= site_url('assets/frontend/images/')?>box.jpg" width="80%" alt="">
                    </div>
                    <div class="bottom">
                      <div class="left">
                        <div class="details w-100">
                          <h5 class="text-center">Rose Box 500 gm Chocolate
                            <small>500 gm / 115 SAR</small></h5>
                        </div>
                      </div>
                    </div>
                  </a>
                  <!-- customize shape product details -->
                  <div class="inside">
                    <div class="icon"><i class="fa fa-info-circle"></i></div>
                    <div class="contents">
                      <table class="details-content d-block">
                            <tbody class="d-block">
                      <tr class="d-block">
                          <td class="text-left">Minimum Order</td>
                          <td class="text-left pl-3">1 KG</td>
                      </tr>
                      <tr class="d-block">
                          <td class="text-left">Price For KG</td>
                          <td class="text-left pl-3">60 SAR</td>
                      </tr>
                      <tr class="d-block">
                          <td class="text-left">Processing Time</td>
                          <td class="text-left pl-3">3 Days</td>
                      </tr>
                      <tr class="d-block">
                          <td class="text-left">Price for 1 PCS</td>
                          <td class="text-left pl-3">1.98 SAR</td>
                      </tr>
                      <tr class="d-block">
                          <td class="text-left">Quantity Per KG</td>
                          <td class="text-left pl-3">60 PCS</td>
                      </tr>                                 
                      <tr class="d-block">
                          <td class="text-left">Chocolate Type</td>
                          <td class="text-left pl-3">Brown</td>
                      </tr>                                 
                      <tr class="d-block">
                        <td class="text-left">Hight</td>
                          <td class="text-left pl-3">10mm</td>
                      </tr>                                 
                      <tr class="d-block">
                          <td class="text-left">Chocolate Type</td>
                          <td class="text-left pl-3">Brown</td>
                      </tr>                                 
                  </tbody>
                        </table>
                    </div>
                  </div>
            </div>
            <div class="col-md-4 wrapper border p-2">
                  <!-- customize box  -->
                  <a href="./customize-box-details.html">
                    <!-- customize box content -->  
                    <div class="d-flex top">
                    <!-- 
                        customize box image
                        the fisrt elemnt from this box Gallary    
                    -->  
                      <img class="m-auto d-block" src="<?= site_url('assets/frontend/images/')?>box.jpg" width="80%" alt="">
                    </div>
                    <div class="bottom">
                      <div class="left">
                        <div class="details w-100">
                          <h5 class="text-center">Rose Box 32 PCS Chocolate
                            <small>32 PCS / 115 SAR</small></h5>
                        </div>
                      </div>
                    </div>
                  </a>
                  <!-- customize shape box details -->
                  <div class="inside">
                    <div class="icon"><i class="fa fa-info-circle"></i></div>
                    <div class="contents">
                      <table class="details-content d-block">
                        <tbody class="d-block">
                      <tr class="d-block">
                          <td class="text-left">Minimum Order</td>
                          <td class="text-left pl-3">1 KG</td>
                      </tr>
                      <tr class="d-block">
                          <td class="text-left">Price For KG</td>
                          <td class="text-left pl-3">60 SAR</td>
                      </tr>
                      <tr class="d-block">
                          <td class="text-left">Processing Time</td>
                          <td class="text-left pl-3">3 Days</td>
                      </tr>
                      <tr class="d-block">
                          <td class="text-left">Price for 1 PCS</td>
                          <td class="text-left pl-3">1.98 SAR</td>
                      </tr>
                      <tr class="d-block">
                          <td class="text-left">Quantity Per KG</td>
                          <td class="text-left pl-3">60 PCS</td>
                      </tr>                                 
                      <tr class="d-block">
                          <td class="text-left">Chocolate Type</td>
                          <td class="text-left pl-3">Brown</td>
                      </tr>                                 
                      <tr class="d-block">
                        <td class="text-left">Hight</td>
                          <td class="text-left pl-3">10mm</td>
                      </tr>                                 
                      <tr class="d-block">
                          <td class="text-left">Chocolate Type</td>
                          <td class="text-left pl-3">Brown</td>
                      </tr>                                 
                  </tbody>
                        </table>
                    </div>
                  </div>
            </div>
        </div>
            
    </section>
    <!-- end customize box categories -->