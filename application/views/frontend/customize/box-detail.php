 <!-- start customize box details -->
    <section class="customize-box-details container">
      <!-- details content -->
      <div
        class="row d-flex justify-content-between align-items-center details-content-info"
      >
        <!-- main detils -->
        <!-- name and description -->
        <div class="col-lg-6 col-md-12 col-xs-12 col-sm-12 order-lg-0 order-md-1">
          <!-- box details container -->
          <div class="product-details-container">
            <!-- details -->
            <div class="product-details">
              <!-- box name -->
              <h1 class="box-name">Rose Box 32 PCS Chocolate</h1>
              <!-- box details -->
              <p class="box-details">
                A coffer of 32 delights of personalized chocolate, embellished with delicate natural rose petals
              </p>
              <!-- box specifications  -->
              <div class="row">
                <!-- quantity -->
                <div class="col-md-12">
                  <!-- quantity title -->
                  <span class="specifications-title d-block">Quantity:</span>
                  <!-- number of pieces for 1 KG -->
                  <table class="specifications d-block w-100">
                    <tbody class="d-block">
                      <tr class="d-block">
                        <td class="w-auto">Number of pieces for 1 Box:</td>
                        <td class="w-auto">30 PC`S</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <!-- BOX DIMENSIONS -->
                <div class="col-md-6 specifications">
                  <!-- title -->
                  <span class="specifications-title d-block"
                    >BOX DIMENSIONS</span
                  >
                  <table class="specifications d-block w-100">
                    <tbody class="d-block">
                      <!-- kength -->
                      <tr class="d-block">
                        <td class="specifications-type">Length :</td>
                        <td class="text-right">44 mm</td>
                      </tr>
                      <!-- width -->
                      <tr>
                        <td class="specifications-type">Width :</td>
                        <td class="text-right">44 mm</td>
                      </tr>
                      <!-- hight -->
                      <tr>
                        <td class="specifications-type">Height :</td>
                        <td class="text-right">10 mm</td>
                      </tr>
                      <!-- width -->
                      <tr>
                        <td class="specifications-type">Weight :</td>
                        <td class="text-right">20 gm</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <!-- WINDOW DIMENSIONS -->
                <div class="col-md-6 specifications">
                  <!-- title -->
                  <span class="specifications-title d-block"
                    >WINDOW DIMENSIONS</span
                  >
                  <table class="specifications d-block w-100">
                    <tbody class="d-block">
                      <!-- lrngth -->
                      <tr class="d-block">
                        <td class="specifications-type">Length :</td>
                        <td class="text-right">32 mm</td>
                      </tr>
                      <!-- width -->
                      <tr>
                        <td class="specifications-type">Width :</td>
                        <td class="text-right">32 mm</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <!-- start customization button -->
                <!-- will go to custiomize option page -->
                <div class="col-xs-12 col-md-12">
                  <div class="pull-center-mobile start-customize">
                    <a href="../chocolate-box.html">
                      Start Customization
                      <span class="d-inline-block pl-2">
                        <i class="fa fa-pencil"></i>
                      </span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- start customize box gallary -->
        <div class="col-lg-6 col-md-12 col-xs-12 col-sm-12 order-first order-md-0">
          <!-- gallary content -->
          
          <div id="carousel" class="owl-carousel owl-theme customize-gallary mg-lft-rght">
            <!-- gallary grid -->
            <div class="item transtion">
              <!-- gallary item -->
              <img class="owl1-img" src="<?= site_url('assets/frontend/images/')?>box.jpg" width="50&" alt=""/>
            </div>
            <!-- gallary grid -->
            <div class="item transtion">
              <!-- gallary item -->
              <img class="owl1-img" src="<?= site_url('assets/frontend/images/')?>box.jpg" width="50&" alt="" />
            </div>
            <!-- gallary grid -->
            <div class="item transtion">
              <!-- gallary item -->
              <img class="owl1-img" src="<?= site_url('assets/frontend/images/')?>box.jpg" width="50&" alt="" />
            </div>
            <!-- gallary grid -->
            <div class="item transtion">
              <!-- gallary item -->
              <img class="owl1-img" src="<?= site_url('assets/frontend/images/')?>box.jpg"
                width="50&" alt=""
              />
            </div>
          </div>
        </div>
      </div>
      <!-- end customize box gallary -->
      <!-- start what`s inside -->
      <div class="row mt-5">
        <div class="col-12">
          <!-- title -->
          <h1 class="text-center what-inside-title">What`s Inside</h1>
        </div>
      </div>
      <div class="row mb-5">
        <div class="col-md-12 ">
          <div id="carousel2" class="owl-carousel owl-theme what-inside">
              <!-- chocolate item -->
              <div class="item">
                <img class="owl2-img" src="<?= site_url('assets/frontend/images/')?>ice1.png" alt="" width="100%">
              </div>
              <!-- chocolate item -->
              <div class="item">
                <img class="owl2-img" src="<?= site_url('assets/frontend/images/')?>ice2.png" alt="" width="100%">
              </div>
              <!-- chocolate item -->
              <div class="item">
                <img class="owl2-img" src="<?= site_url('assets/frontend/images/')?>ice3.png" alt="" width="100%">
              </div>
              <!-- chocolate item -->
              <div class="item">
                <img class="owl2-img" src="<?= site_url('assets/frontend/images/')?>ice4.png" alt="" width="100%">
              </div>
              <!-- chocolate item -->
              <div class="item">
                <img class="owl2-img" src="<?= site_url('assets/frontend/images/')?>ice5.png" alt="" width="100%">
              </div>
              <!-- chocolate item -->
              <div class="item">
                <img class="owl2-img" src="<?= site_url('assets/frontend/images/')?>ice6.png" alt="" width="100%">
              </div>
              <!-- chocolate item -->
              <div class="item">
                <img class="owl2-img" src="<?= site_url('assets/frontend/images/')?>ice7.png" alt="" width="100%">
              </div>
              <!-- chocolate item -->
              <div class="item">
                <img class="owl2-img" src="<?= site_url('assets/frontend/images/')?>ice1.png" alt="" width="100%">
              </div>
              <!-- chocolate item -->
              <div class="item">
                <img class="owl2-img" src="<?= site_url('assets/frontend/images/')?>ice2.png" alt="" width="100%">
              </div>
              <!-- chocolate item -->
              <div class="item">
                <img class="owl2-img" src="<?= site_url('assets/frontend/images/')?>ice3.png" alt="" width="100%">
              </div>
              <!-- chocolate item -->
              <div class="item">
                <img class="owl2-img" src="<?= site_url('assets/frontend/images/')?>ice4.png" alt="" width="100%">
              </div>
              <!-- chocolate item -->
              <div class="item">
                <img class="owl2-img" src="<?= site_url('assets/frontend/images/')?>ice5.png" alt="" width="100%">
              </div>
              <!-- chocolate item -->
              <div class="item">
                <img class="owl2-img" src="<?= site_url('assets/frontend/images/')?>ice6.png" alt="" width="100%">
              </div>
              <!-- chocolate item -->
              <div class="item">
                <img class="owl2-img" src="<?= site_url('assets/frontend/images/')?>ice4.png" alt="" width="100%">
              </div>
              <!-- chocolate item -->
              <div class="item">
                <img class="owl2-img" src="<?= site_url('assets/frontend/images/')?>ice3.png" alt="" width="100%">
              </div>
             
          </div>
        </div>


      </div>
      <!-- end whats inside -->
    </section>
    <!-- end customize box details -->
    <script>
    jQuery("#carousel").owlCarousel({
      items: 1,
      autoplay: true,
      nav: true,
      dots: true,
    });
    jQuery("#carousel2").owlCarousel({
      items: 5,
      autoplay: true,
      nav: true,
      dots: true
    });
    </script>