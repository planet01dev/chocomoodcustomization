<section class="container">
        <!-- choose your box item -->
        <a class="row box-item mb-3" href="customize-box-category.html">
            <!-- item
                add box image in background image which onode style attribute
            -->
            <div class="col-md-12 box-image" style="background-image: url(<?= site_url('assets/frontend/images/')?>banner-category1.jpg);">
                <!-- content -->
                <div class="row align-items-end justify-content-start h-100 content-row">
                    <div class="col-md-6 p-3 box-content rounded-right-top">
                        <!-- name -->
                        <h4 class="title">FANCY BOXES</h4>
                        <!-- description -->
                        <p class="description position-relative">
                            Add a touch of extravagance and originality to your chocolates with our Fantasy box that you can customize yourself!
                        </p>
                    </div>
                </div>
            </div>
        </a>
        <!-- choose your box item -->
        <a class="row box-item mb-3" href="customize-box-category.html">
            <!-- item
                add box image in background image which onode style attribute
            -->
            <div class="col-md-12 box-image" style="background-image: url(<?= site_url('assets/frontend/images/')?>banner-category2.jpg);">
                <!-- content -->
                <div class="row align-items-end justify-content-end h-100 content-row">
                    <div class="col-md-6 p-3 box-content rounded-left-top">
                        <!-- name -->
                        <h4 class="title">ROSE BOXES</h4>
                        <!-- description -->
                        <p class="description position-relative">
                            A coffer of 32 delights of personalized chocolate, embellished with delicate natural rose petals.
                        </p>
                    </div>
                </div>
            </div>
        </a>
    </section>