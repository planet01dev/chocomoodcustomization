   <style>
        .card::before {
            width: 100%;
            height: 100%;
            position: absolute;
            background: #0000006e;
            top: 0;
            left: 0;
            content: '';
            
        }

        .card-img-overlay a:hover{
            text-decoration: none;
        }
    </style>

    <section class="container mt-5">
        <div class="">
            <div class="row">
                <div class="col-md-6">
                    <div class="card bg-dark text-white mb-5" style="height: 40rem;">
                        <img src="<?= site_url('assets/frontend/images')?>/printable-box.jpg" class="card-img h-100" alt="...">
                        <a href="./printable-box/choose-box.html">
                            <div class="row align-items-end card-img-overlay">
                                <div class="col-12">
                                    <h5 class="card-title text-white">Printabale Box</h5>
                                    <p class="card-text text-white">Add a touch of extravagance and originality to your chocolates with our Fantasy box that you can customize yourself!</p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card bg-dark text-white mb-5" style="height: 40rem;">
                        <img src="<?= site_url('assets/frontend/images')?>/unprintable-box.jpg" class="card-img h-100" alt="...">
                        <a href="./unprintable-box/choose-box.html">
                            <div class="row align-items-end card-img-overlay">
                                <div class="col-12">
                                    <h5 class="card-title text-white">unprintabale Box</h5>
                                    <p class="card-text text-white">Add a touch of extravagance and originality to your chocolates with our Fantasy box that you can customize yourself!</p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>