<section class="chocolate-box-container container-fluid " id="customize-chocolate-box">
        <form class="form-row" action="" method="POST">
            <!-- start chocolate gallary for box -->
            <div class="col-xl-6 col-lg-12 col-md-12 my-5 ">
                <div class="row chocolate-for-box border rounded shadow py-4 ">
                    <!-- chocolate for box item -->
                    <div class="col-lg-4 col-md-4 col-sm-6 col-6 text-center chocolate-item ">
                        <!-- chocolate for box image -->
                        <img src="<?= site_url('assets/frontend/images/')?>ice1.png" alt="" class="" width="100px" height="100px">
                        <!-- chocolate for box content -->
                        <p class=" chocolate-title mt-2 text-muted">
                            <!-- name -->
                            Walnut & Chocolate <br>
                            <!-- price -->
                            <strong>20</strong>
                            <small>SAR</small>
                        </p>
                        <!-- counter -->
                        <div class="counter">
                            <!-- increae Button -->
                            <button type="button" class="btn btn-primary increase">+</button>
                            <!-- number of cocholate choosen 
                                data-chocolate that mean the chocolate name to send this value and count of number from this input in order table inside database
                            -->
                            <input class="form-control quantity-count" type="text" value="0" max="30"
                                name="chocolateName" data-chocolate="https://via.placeholder.com/150">
                                <!-- decresse button -->
                            <button type="button" class="btn btn-primary decrease">-</button>
                        </div>
                    </div>
                    <!-- chocolate for box item -->
                    <div class="col-lg-4 col-md-4 col-sm-6 col-6 text-center chocolate-item ">
                        <!-- chocolate for box image -->
                        <img src="<?= site_url('assets/frontend/images/')?>ic2.png" alt="" class="" width="100px" height="100px">
                        <!-- chocolate for box content -->
                        <p class=" chocolate-title mt-2 text-muted">
                            <!-- name -->
                            Walnut & Chocolate <br>
                            <!-- price -->
                            <strong>20</strong>
                            <small>SAR</small>
                        </p>
                        <!-- counter -->
                        <div class="counter">
                            <!-- increae Button -->
                            <button type="button" class="btn btn-primary increase">+</button>
                            <!-- number of cocholate choosen 
                                data-chocolate that mean the chocolate name to send this value and count of number from this input in order table inside database
                            -->
                            <input class="form-control quantity-count" type="text" value="0" max="30"
                                name="chocolateName" data-chocolate="https://via.placeholder.com/150">
                                <!-- decresse button -->
                            <button type="button" class="btn btn-primary decrease">-</button>
                        </div>
                    </div>
                    <!-- chocolate for box item -->
                    <div class="col-lg-4 col-md-4 col-sm-6 col-6 text-center chocolate-item ">
                        <!-- chocolate for box image -->
                        <img src="<?= site_url('assets/frontend/images/')?>ice3.png" alt="" class="" width="100px" height="100px">
                        <!-- chocolate for box content -->
                        <p class=" chocolate-title mt-2 text-muted">
                            <!-- name -->
                            Walnut & Chocolate <br>
                            <!-- price -->
                            <strong>20</strong>
                            <small>SAR</small>
                        </p>
                        <!-- counter -->
                        <div class="counter">
                            <!-- increae Button -->
                            <button type="button" class="btn btn-primary increase">+</button>
                            <!-- number of cocholate choosen 
                                data-chocolate that mean the chocolate name to send this value and count of number from this input in order table inside database
                            -->
                            <input class="form-control quantity-count" type="text" value="0" max="30"
                                name="chocolateName" data-chocolate="https://via.placeholder.com/150">
                                <!-- decresse button -->
                            <button type="button" class="btn btn-primary decrease">-</button>
                        </div>
                    </div>
                    <!-- chocolate for box item -->
                    <div class="col-lg-4 col-md-4 col-sm-6 col-6 text-center chocolate-item ">
                        <!-- chocolate for box image -->
                        <img src="<?= site_url('assets/frontend/images/')?>ice4.png" alt="" class="" width="100px" height="100px">
                        <!-- chocolate for box content -->
                        <p class=" chocolate-title mt-2 text-muted">
                            <!-- name -->
                            Walnut & Chocolate <br>
                            <!-- price -->
                            <strong>20</strong>
                            <small>SAR</small>
                        </p>
                        <!-- counter -->
                        <div class="counter">
                            <!-- increae Button -->
                            <button type="button" class="btn btn-primary increase">+</button>
                            <!-- number of cocholate choosen 
                                data-chocolate that mean the chocolate name to send this value and count of number from this input in order table inside database
                            -->
                            <input class="form-control quantity-count" type="text" value="0" max="30"
                                name="chocolateName" data-chocolate="https://via.placeholder.com/150">
                                <!-- decresse button -->
                            <button type="button" class="btn btn-primary decrease">-</button>
                        </div>
                    </div>
                    <!-- chocolate for box item -->
                    <div class="col-lg-4 col-md-4 col-sm-6 col-6 text-center chocolate-item ">
                        <!-- chocolate for box image -->
                        <img src="<?= site_url('assets/frontend/images/')?>ice5.png" alt="" class="" width="100px" height="100px">
                        <!-- chocolate for box content -->
                        <p class=" chocolate-title mt-2 text-muted">
                            <!-- name -->
                            Walnut & Chocolate <br>
                            <!-- price -->
                            <strong>20</strong>
                            <small>SAR</small>
                        </p>
                        <!-- counter -->
                        <div class="counter">
                            <!-- increae Button -->
                            <button type="button" class="btn btn-primary increase">+</button>
                            <!-- number of cocholate choosen 
                                data-chocolate that mean the chocolate name to send this value and count of number from this input in order table inside database
                            -->
                            <input class="form-control quantity-count" type="text" value="0" max="30"
                                name="chocolateName" data-chocolate="https://via.placeholder.com/150">
                                <!-- decresse button -->
                            <button type="button" class="btn btn-primary decrease">-</button>
                        </div>
                    </div>
                    <!-- chocolate for box item -->
                    <div class="col-lg-4 col-md-4 col-sm-6 col-6 text-center chocolate-item ">
                        <!-- chocolate for box image -->
                        <img src="<?= site_url('assets/frontend/images/')?>ice6.png" alt="" class="" width="100px" height="100px">
                        <!-- chocolate for box content -->
                        <p class=" chocolate-title mt-2 text-muted">
                            <!-- name -->
                            Walnut & Chocolate <br>
                            <!-- price -->
                            <strong>20</strong>
                            <small>SAR</small>
                        </p>
                        <!-- counter -->
                        <div class="counter">
                            <!-- increae Button -->
                            <button type="button" class="btn btn-primary increase">+</button>
                            <!-- number of cocholate choosen 
                                data-chocolate that mean the chocolate name to send this value and count of number from this input in order table inside database
                            -->
                            <input class="form-control quantity-count" type="text" value="0" max="30"
                                name="chocolateName" data-chocolate="https://via.placeholder.com/150">
                                <!-- decresse button -->
                            <button type="button" class="btn btn-primary decrease">-</button>
                        </div>
                    </div>
                    <!-- chocolate for box item -->
                    <div class="col-lg-4 col-md-4 col-sm-6 col-6 text-center chocolate-item ">
                        <!-- chocolate for box image -->
                        <img src="<?= site_url('assets/frontend/images/')?>ice1.png" alt="" class="" width="100px" height="100px">
                        <!-- chocolate for box content -->
                        <p class=" chocolate-title mt-2 text-muted">
                            <!-- name -->
                            Walnut & Chocolate <br>
                            <!-- price -->
                            <strong>20</strong>
                            <small>SAR</small>
                        </p>
                        <!-- counter -->
                        <div class="counter">
                            <!-- increae Button -->
                            <button type="button" class="btn btn-primary increase">+</button>
                            <!-- number of cocholate choosen 
                                data-chocolate that mean the chocolate name to send this value and count of number from this input in order table inside database
                            -->
                            <input class="form-control quantity-count" type="text" value="0" max="30"
                                name="chocolateName" data-chocolate="https://via.placeholder.com/150">
                                <!-- decresse button -->
                            <button type="button" class="btn btn-primary decrease">-</button>
                        </div>
                    </div>
                    <!-- chocolate for box item -->
                    <div class="col-lg-4 col-md-4 col-sm-6 col-6 text-center chocolate-item ">
                        <!-- chocolate for box image -->
                        <img src="<?= site_url('assets/frontend/images/')?>ice2.png" alt="" class="" width="100px" height="100px">
                        <!-- chocolate for box content -->
                        <p class=" chocolate-title mt-2 text-muted">
                            <!-- name -->
                            Walnut & Chocolate <br>
                            <!-- price -->
                            <strong>20</strong>
                            <small>SAR</small>
                        </p>
                        <!-- counter -->
                        <div class="counter">
                            <!-- increae Button -->
                            <button type="button" class="btn btn-primary increase">+</button>
                            <!-- number of cocholate choosen 
                                data-chocolate that mean the chocolate name to send this value and count of number from this input in order table inside database
                            -->
                            <input class="form-control quantity-count" type="text" value="0" max="30"
                                name="chocolateName" data-chocolate="https://via.placeholder.com/150">
                                <!-- decresse button -->
                            <button type="button" class="btn btn-primary decrease">-</button>
                        </div>
                    </div>
                    <!-- chocolate for box item -->
                    <div class="col-lg-4 col-md-4 col-sm-6 col-6 text-center chocolate-item ">
                        <!-- chocolate for box image -->
                        <img src="<?= site_url('assets/frontend/images/')?>ice3.png" alt="" class="" width="100px" height="100px">
                        <!-- chocolate for box content -->
                        <p class=" chocolate-title mt-2 text-muted">
                            <!-- name -->
                            Walnut & Chocolate <br>
                            <!-- price -->
                            <strong>20</strong>
                            <small>SAR</small>
                        </p>
                        <!-- counter -->
                        <div class="counter">
                            <!-- increae Button -->
                            <button type="button" class="btn btn-primary increase">+</button>
                            <!-- number of cocholate choosen 
                                data-chocolate that mean the chocolate name to send this value and count of number from this input in order table inside database
                            -->
                            <input class="form-control quantity-count" type="text" value="0" max="30"
                                name="chocolateName" data-chocolate="https://via.placeholder.com/150">
                                <!-- decresse button -->
                            <button type="button" class="btn btn-primary decrease">-</button>
                        </div>
                    </div>
                    <!-- chocolate for box item -->
                    <div class="col-lg-4 col-md-4 col-sm-6 col-6 text-center chocolate-item ">
                        <!-- chocolate for box image -->
                        <img src="<?= site_url('assets/frontend/images/')?>ice4.png" alt="" class="" width="100px" height="100px">
                        <!-- chocolate for box content -->
                        <p class=" chocolate-title mt-2 text-muted">
                            <!-- name -->
                            Walnut & Chocolate <br>
                            <!-- price -->
                            <strong>20</strong>
                            <small>SAR</small>
                        </p>
                        <!-- counter -->
                        <div class="counter">
                            <!-- increae Button -->
                            <button type="button" class="btn btn-primary increase">+</button>
                            <!-- number of cocholate choosen 
                                data-chocolate that mean the chocolate name to send this value and count of number from this input in order table inside database
                            -->
                            <input class="form-control quantity-count" type="text" value="0" max="30"
                                name="chocolateName" data-chocolate="https://via.placeholder.com/150">
                                <!-- decresse button -->
                            <button type="button" class="btn btn-primary decrease">-</button>
                        </div>
                    </div>
                    <!-- chocolate for box item -->
                    <div class="col-lg-4 col-md-4 col-sm-6 col-6 text-center chocolate-item ">
                        <!-- chocolate for box image -->
                        <img src="<?= site_url('assets/frontend/images/')?>ice5.png" alt="" class="" width="100px" height="100px">
                        <!-- chocolate for box content -->
                        <p class=" chocolate-title mt-2 text-muted">
                            <!-- name -->
                            Walnut & Chocolate <br>
                            <!-- price -->
                            <strong>20</strong>
                            <small>SAR</small>
                        </p>
                        <!-- counter -->
                        <div class="counter">
                            <!-- increae Button -->
                            <button type="button" class="btn btn-primary increase">+</button>
                            <!-- number of cocholate choosen 
                                data-chocolate that mean the chocolate name to send this value and count of number from this input in order table inside database
                            -->
                            <input class="form-control quantity-count" type="text" value="0" max="30"
                                name="chocolateName" data-chocolate="https://via.placeholder.com/150">
                                <!-- decresse button -->
                            <button type="button" class="btn btn-primary decrease">-</button>
                        </div>
                    </div>
                    <!-- chocolate for box item -->
                    <div class="col-lg-4 col-md-4 col-sm-6 col-6 text-center chocolate-item ">
                        <!-- chocolate for box image -->
                        <img src="<?= site_url('assets/frontend/images/')?>ice6.png" alt="" class="" width="100px" height="100px">
                        <!-- chocolate for box content -->
                        <p class=" chocolate-title mt-2 text-muted">
                            <!-- name -->
                            Walnut & Chocolate <br>
                            <!-- price -->
                            <strong>20</strong>
                            <small>SAR</small>
                        </p>
                        <!-- counter -->
                        <div class="counter">
                            <!-- increae Button -->
                            <button type="button" class="btn btn-primary increase">+</button>
                            <!-- number of cocholate choosen 
                                data-chocolate that mean the chocolate name to send this value and count of number from this input in order table inside database
                            -->
                            <input class="form-control quantity-count" type="text" value="0" max="30"
                                name="chocolateName" data-chocolate="https://via.placeholder.com/150">
                                <!-- decresse button -->
                            <button type="button" class="btn btn-primary decrease">-</button>
                        </div>
                    </div>
                    <!-- chocolate for box item -->
                    <div class="col-lg-4 col-md-4 col-sm-6 col-6 text-center chocolate-item ">
                        <!-- chocolate for box image -->
                        <img src="<?= site_url('assets/frontend/images/')?>ice1.png" alt="" class="" width="100px" height="100px">
                        <!-- chocolate for box content -->
                        <p class=" chocolate-title mt-2 text-muted">
                            <!-- name -->
                            Walnut & Chocolate <br>
                            <!-- price -->
                            <strong>20</strong>
                            <small>SAR</small>
                        </p>
                        <!-- counter -->
                        <div class="counter">
                            <!-- increae Button -->
                            <button type="button" class="btn btn-primary increase">+</button>
                            <!-- number of cocholate choosen 
                                data-chocolate that mean the chocolate name to send this value and count of number from this input in order table inside database
                            -->
                            <input class="form-control quantity-count" type="text" value="0" max="30"
                                name="chocolateName" data-chocolate="https://via.placeholder.com/150">
                                <!-- decresse button -->
                            <button type="button" class="btn btn-primary decrease">-</button>
                        </div>
                    </div>
                    <!-- chocolate for box item -->
                    <div class="col-lg-4 col-md-4 col-sm-6 col-6 text-center chocolate-item ">
                        <!-- chocolate for box image -->
                        <img src="<?= site_url('assets/frontend/images/')?>ice2.png" alt="" class="" width="100px" height="100px">
                        <!-- chocolate for box content -->
                        <p class=" chocolate-title mt-2 text-muted">
                            <!-- name -->
                            Walnut & Chocolate <br>
                            <!-- price -->
                            <strong>20</strong>
                            <small>SAR</small>
                        </p>
                        <!-- counter -->
                        <div class="counter">
                            <!-- increae Button -->
                            <button type="button" class="btn btn-primary increase">+</button>
                            <!-- number of cocholate choosen 
                                data-chocolate that mean the chocolate name to send this value and count of number from this input in order table inside database
                            -->
                            <input class="form-control quantity-count" type="text" value="0" max="30"
                                name="chocolateName" data-chocolate="https://via.placeholder.com/150">
                                <!-- decresse button -->
                            <button type="button" class="btn btn-primary decrease">-</button>
                        </div>
                    </div>
                    <!-- chocolate for box item -->
                    <div class="col-lg-4 col-md-4 col-sm-6 col-6 text-center chocolate-item ">
                        <!-- chocolate for box image -->
                        <img src="<?= site_url('assets/frontend/images/')?>ice3.png" alt="" class="" width="100px" height="100px">
                        <!-- chocolate for box content -->
                        <p class=" chocolate-title mt-2 text-muted">
                            <!-- name -->
                            Walnut & Chocolate <br>
                            <!-- price -->
                            <strong>20</strong>
                            <small>SAR</small>
                        </p>
                        <!-- counter -->
                        <div class="counter">
                            <!-- increae Button -->
                            <button type="button" class="btn btn-primary increase">+</button>
                            <!-- number of cocholate choosen 
                                data-chocolate that mean the chocolate name to send this value and count of number from this input in order table inside database
                            -->
                            <input class="form-control quantity-count" type="text" value="0" max="30"
                                name="chocolateName" data-chocolate="https://via.placeholder.com/150">
                                <!-- decresse button -->
                            <button type="button" class="btn btn-primary decrease">-</button>
                        </div>
                    </div>
                    <!-- chocolate for box item -->
                    <div class="col-lg-4 col-md-4 col-sm-6 col-6 text-center chocolate-item ">
                        <!-- chocolate for box image -->
                        <img src="<?= site_url('assets/frontend/images/')?>ice4.png" alt="" class="" width="100px" height="100px">
                        <!-- chocolate for box content -->
                        <p class=" chocolate-title mt-2 text-muted">
                            <!-- name -->
                            Walnut & Chocolate <br>
                            <!-- price -->
                            <strong>20</strong>
                            <small>SAR</small>
                        </p>
                        <!-- counter -->
                        <div class="counter">
                            <!-- increae Button -->
                            <button type="button" class="btn btn-primary increase">+</button>
                            <!-- number of cocholate choosen 
                                data-chocolate that mean the chocolate name to send this value and count of number from this input in order table inside database
                            -->
                            <input class="form-control quantity-count" type="text" value="0" max="30"
                                name="chocolateName" data-chocolate="https://via.placeholder.com/150">
                                <!-- decresse button -->
                            <button type="button" class="btn btn-primary decrease">-</button>
                        </div>
                    </div>
                    <!-- chocolate for box item -->
                    <div class="col-lg-4 col-md-4 col-sm-6 col-6 text-center chocolate-item ">
                        <!-- chocolate for box image -->
                        <img src="<?= site_url('assets/frontend/images/')?>ice5.png" alt="" class="" width="100px" height="100px">
                        <!-- chocolate for box content -->
                        <p class=" chocolate-title mt-2 text-muted">
                            <!-- name -->
                            Walnut & Chocolate <br>
                            <!-- price -->
                            <strong>20</strong>
                            <small>SAR</small>
                        </p>
                        <!-- counter -->
                        <div class="counter">
                            <!-- increae Button -->
                            <button type="button" class="btn btn-primary increase">+</button>
                            <!-- number of cocholate choosen 
                                data-chocolate that mean the chocolate name to send this value and count of number from this input in order table inside database
                            -->
                            <input class="form-control quantity-count" type="text" value="0" max="30"
                                name="chocolateName" data-chocolate="https://via.placeholder.com/150">
                                <!-- decresse button -->
                            <button type="button" class="btn btn-primary decrease">-</button>
                        </div>
                    </div>
                    <!-- chocolate for box item -->
                    <div class="col-lg-4 col-md-4 col-sm-6 col-6 text-center chocolate-item ">
                        <!-- chocolate for box image -->
                        <img src="<?= site_url('assets/frontend/images/')?>ice6.png" alt="" class="" width="100px" height="100px">
                        <!-- chocolate for box content -->
                        <p class=" chocolate-title mt-2 text-muted">
                            <!-- name -->
                            Walnut & Chocolate <br>
                            <!-- price -->
                            <strong>20</strong>
                            <small>SAR</small>
                        </p>
                        <!-- counter -->
                        <div class="counter">
                            <!-- increae Button -->
                            <button type="button" class="btn btn-primary increase">+</button>
                            <!-- number of cocholate choosen 
                                data-chocolate that mean the chocolate name to send this value and count of number from this input in order table inside database
                            -->
                            <input class="form-control quantity-count" type="text" value="0" max="30"
                                name="chocolateName" data-chocolate="https://via.placeholder.com/150">
                                <!-- decresse button -->
                            <button type="button" class="btn btn-primary decrease">-</button>
                        </div>
                    </div>
                    <!-- chocolate for box item -->
                    <div class="col-lg-4 col-md-4 col-sm-6 col-6 text-center chocolate-item ">
                        <!-- chocolate for box image -->
                        <img src="<?= site_url('assets/frontend/images/')?>ice1.png" alt="" class="" width="100px" height="100px">
                        <!-- chocolate for box content -->
                        <p class=" chocolate-title mt-2 text-muted">
                            <!-- name -->
                            Walnut & Chocolate <br>
                            <!-- price -->
                            <strong>20</strong>
                            <small>SAR</small>
                        </p>
                        <!-- counter -->
                        <div class="counter">
                            <!-- increae Button -->
                            <button type="button" class="btn btn-primary increase">+</button>
                            <!-- number of cocholate choosen 
                                data-chocolate that mean the chocolate name to send this value and count of number from this input in order table inside database
                            -->
                            <input class="form-control quantity-count" type="text" value="0" max="30"
                                name="chocolateName" data-chocolate="https://via.placeholder.com/150">
                                <!-- decresse button -->
                            <button type="button" class="btn btn-primary decrease">-</button>
                        </div>
                    </div>
                    <!-- chocolate for box item -->
                    <div class="col-lg-4 col-md-4 col-sm-6 col-6 text-center chocolate-item ">
                        <!-- chocolate for box image -->
                        <img src="<?= site_url('assets/frontend/images/')?>ice2.png" alt="" class="" width="100px" height="100px">
                        <!-- chocolate for box content -->
                        <p class=" chocolate-title mt-2 text-muted">
                            <!-- name -->
                            Walnut & Chocolate <br>
                            <!-- price -->
                            <strong>20</strong>
                            <small>SAR</small>
                        </p>
                        <!-- counter -->
                        <div class="counter">
                            <!-- increae Button -->
                            <button type="button" class="btn btn-primary increase">+</button>
                            <!-- number of cocholate choosen 
                                data-chocolate that mean the chocolate name to send this value and count of number from this input in order table inside database
                            -->
                            <input class="form-control quantity-count" type="text" value="0" max="30"
                                name="chocolateName" data-chocolate="https://via.placeholder.com/150">
                                <!-- decresse button -->
                            <button type="button" class="btn btn-primary decrease">-</button>
                        </div>
                    </div>
                    <!-- chocolate for box item -->
                    <div class="col-lg-4 col-md-4 col-sm-6 col-6 text-center chocolate-item ">
                        <!-- chocolate for box image -->
                        <img src="<?= site_url('assets/frontend/images/')?>ice3.png" alt="" class="" width="100px" height="100px">
                        <!-- chocolate for box content -->
                        <p class=" chocolate-title mt-2 text-muted">
                            <!-- name -->
                            Walnut & Chocolate <br>
                            <!-- price -->
                            <strong>20</strong>
                            <small>SAR</small>
                        </p>
                        <!-- counter -->
                        <div class="counter">
                            <!-- increae Button -->
                            <button type="button" class="btn btn-primary increase">+</button>
                            <!-- number of cocholate choosen 
                                data-chocolate that mean the chocolate name to send this value and count of number from this input in order table inside database
                            -->
                            <input class="form-control quantity-count" type="text" value="0" max="30"
                                name="chocolateName" data-chocolate="https://via.placeholder.com/150">
                                <!-- decresse button -->
                            <button type="button" class="btn btn-primary decrease">-</button>
                        </div>
                    </div>
                    <!-- chocolate for box item -->
                    <div class="col-lg-4 col-md-4 col-sm-6 col-6 text-center chocolate-item ">
                        <!-- chocolate for box image -->
                        <img src="<?= site_url('assets/frontend/images/')?>ice4.png" alt="" class="" width="100px" height="100px">
                        <!-- chocolate for box content -->
                        <p class=" chocolate-title mt-2 text-muted">
                            <!-- name -->
                            Walnut & Chocolate <br>
                            <!-- price -->
                            <strong>20</strong>
                            <small>SAR</small>
                        </p>
                        <!-- counter -->
                        <div class="counter">
                            <!-- increae Button -->
                            <button type="button" class="btn btn-primary increase">+</button>
                            <!-- number of cocholate choosen 
                                data-chocolate that mean the chocolate name to send this value and count of number from this input in order table inside database
                            -->
                            <input class="form-control quantity-count" type="text" value="0" max="30"
                                name="chocolateName" data-chocolate="https://via.placeholder.com/150">
                                <!-- decresse button -->
                            <button type="button" class="btn btn-primary decrease">-</button>
                        </div>
                    </div>
                    <!-- chocolate for box item -->
                    <div class="col-lg-4 col-md-4 col-sm-6 col-6 text-center chocolate-item ">
                        <!-- chocolate for box image -->
                        <img src="<?= site_url('assets/frontend/images/')?>ice5.png" alt="" class="" width="100px" height="100px">
                        <!-- chocolate for box content -->
                        <p class=" chocolate-title mt-2 text-muted">
                            <!-- name -->
                            Walnut & Chocolate <br>
                            <!-- price -->
                            <strong>20</strong>
                            <small>SAR</small>
                        </p>
                        <!-- counter -->
                        <div class="counter">
                            <!-- increae Button -->
                            <button type="button" class="btn btn-primary increase">+</button>
                            <!-- number of cocholate choosen 
                                data-chocolate that mean the chocolate name to send this value and count of number from this input in order table inside database
                            -->
                            <input class="form-control quantity-count" type="text" value="0" max="30"
                                name="chocolateName" data-chocolate="https://via.placeholder.com/150">
                                <!-- decresse button -->
                            <button type="button" class="btn btn-primary decrease">-</button>
                        </div>
                    </div>
                    <!-- chocolate for box item -->
                    <div class="col-lg-4 col-md-4 col-sm-6 col-6 text-center chocolate-item ">
                        <!-- chocolate for box image -->
                        <img src="<?= site_url('assets/frontend/images/')?>ice6.png" alt="" class="" width="100px" height="100px">
                        <!-- chocolate for box content -->
                        <p class=" chocolate-title mt-2 text-muted">
                            <!-- name -->
                            Walnut & Chocolate <br>
                            <!-- price -->
                            <strong>20</strong>
                            <small>SAR</small>
                        </p>
                        <!-- counter -->
                        <div class="counter">
                            <!-- increae Button -->
                            <button type="button" class="btn btn-primary increase">+</button>
                            <!-- number of cocholate choosen 
                                data-chocolate that mean the chocolate name to send this value and count of number from this input in order table inside database
                            -->
                            <input class="form-control quantity-count" type="text" value="0" max="30"
                                name="chocolateName" data-chocolate="https://via.placeholder.com/150">
                                <!-- decresse button -->
                            <button type="button" class="btn btn-primary decrease">-</button>
                        </div>
                    </div>

                </div>
            </div>
            <!-- end chocolate gallary for box -->
            <!-- start chocolate in box -->
            <div class="col-xl-6 col-lg-12 col-md-12 mt-5 order-first order-lg-last">
                <div class="row w-auto chocolate-box-details">
                    <div class="col-md-12 position-relative">
                        <!-- this is the image for box -->
                        <img id="box" src="<?= site_url('assets/frontend/images/')?>box.jpg" alt="" class="" width="100%" height="100%">
                        <!-- this is the container which save chocolate and view it over box image 
                            - must add the type for box in => data-box-type attribute => [by gm , by pcs]
                            - must add th max quantity for box in => data-max-quantity attribute
                            - must add th max number of rows for box in => data--count-rows attribute
                            - must add th add the max number for each row for box in => data-chocolate-count-in-rows attribute
                                - it must be in order and the sepration between each number is -
                        -->
                        <div class="row w-100 p-4 position-absolute chocolate-choosen" data-box-type="PCS" data-max-quantity="30"
                            data-count-rows="5" data-chocolate-count-in-rows="6-6-6-6-6">
                            <div class="col-md-12 chocolate-choosen-container"></div>
                        </div>
                    </div>
                    <!-- this is the box information will hidden in mobile  -->
                    <div class="col-md-12 title-and-description d-xl-block d-none">
                        <!-- title  -->
                        <h2 class="title mt-4"> 30 Chocolate PCS Box </h2>
                        <!-- description -->
                        <p>Lorem ipsum dolor sit amet. Beatae perferendis ex voluptatibus libero sint molestiae officia
                            tenetur labore eos, possimus, facere doloribus suscipit architecto perspiciatis hic! Sunt
                            sit at accusantium.</p>
                    </div>
                    <!-- box dimensions -->
                    <div class="col-md-12 details d-xl-block d-none">
                        <h4 class="title mt-4"> <strong>Box Dimensions</strong></h4>
                        <table>
                            <tr>
                                <td>Length:</td>
                                <td class="pl-3">244 mm</td>
                            </tr>
                            <tr>
                                <td>width:</td>
                                <td class="pl-3">244 mm</td>
                            </tr>
                        </table>
                        <!-- order nots -->
                        <h4 class=" title mt-4 "> <strong>Order Notes</strong> </h4>
                        <table>
                            <tr>
                                <td>Minimum Order:</td>
                                <td class="pl-3">1 Pieces</td>
                            </tr>
                            <tr>
                                <td>Processing Time:</td>
                                <td class="pl-3">3 Days</td>
                            </tr>

                        </table>
                        <!-- price for 1 bpx -->
                        <h4 class=" title mt-4"><strong>Price</strong> <small class="pl-3"> 250.00</small> <small
                                class="pl-3">SAR </small></h4>
                    </div>
                    <!-- counter -->
                    <div class="col-md-12 quantity d-xl-block d-none">
                        <div class="row justify-content-between align-items-center">
                            <div class="col-3 ">
                                <h4 class="title"><strong>Quantity</strong></h4>
                            </div>
                            <div class="col-6 box-counter text-right">
                                <div class="counter">
                                    <!-- increas -->
                                    <button type="button" class="btn btn-primary increase">+</button>
                                     <!-- number of box choosen 
                                            data-box that mean the box name to send this value and count of number from this input in order table inside database
                                        -->
                                    <input class="form-control quantity-count" type="text" value="1"
                                        name="chocolateName" data-box="https://via.placeholder.com/150">
                                    <button type="button" class="btn btn-primary decrease">-</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- add to cart -->
                    <div class="col-md-12 d-xl-block d-none">
                        <button type="submit" class="btn btn-primary btn-lg btn-block mt-2 add-to-cart">Add to Cart
                        </button>
                    </div>
                </div>
            </div>
            <!-- this will shown in mobile only and the same data i explaind in the previous item -->
            <div class="col-md-12 order-last d-xl-none d-md-block chocolate-box-details">
                <div class="col-md-12 title-and-description">
                    <h2 class="title mt-4"> 30 Chocolate PCS Box </h2>
                    <p>Lorem ipsum dolor sit amet. Beatae perferendis ex voluptatibus libero sint molestiae officia
                        tenetur labore eos, possimus, facere doloribus suscipit architecto perspiciatis hic! Sunt sit at
                        accusantium.</p>
                </div>
                <div class="col-md-12 details">
                    <h4 class="title mt-4"> <strong>Box Dimensions</strong></h4>
                    <table>
                        <tr>
                            <td>Length:</td>
                            <td class="pl-3">244 mm</td>
                        </tr>
                        <tr>
                            <td>width:</td>
                            <td class="pl-3">244 mm</td>
                        </tr>
                    </table>
                    <h4 class=" title mt-4 "> <strong>Order Notes</strong> </h4>
                    <table>
                        <tr>
                            <td>Minimum Order:</td>
                            <td class="pl-3">1 Pieces</td>
                        </tr>
                        <tr>
                            <td>Processing Time:</td>
                            <td class="pl-3">3 Days</td>
                        </tr>

                    </table>
                    <h4 class=" title mt-4"><strong>Price</strong> <small class="pl-3"> 250.00</small> <small
                            class="pl-3">SAR </small></h4>
                </div>
                <div class="col-md-12 quantity">
                    <div class="row justify-content-between align-items-center">
                        <div class="col-xs-4 col-6 ">
                            <h4 class="title"><strong>Quantity</strong></h4>
                        </div>
                        <div class="col-xs-8 col-6 box-counter text-right">
                            <div class="counter">
                                <button type="button" class="btn btn-primary increase">+</button>
                                <input class="form-control quantity-count" type="text" value="1" name="chocolateName"
                                    data-box="https://via.placeholder.com/150">
                                <button type="button" class="btn btn-primary decrease">-</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" id="add-to-cart">
                    <button type="submit" class="btn btn-primary btn-lg btn-block mt-2 add-to-cart">Add to Cart
                    </button>
                </div>
            </div>
        </form>
        <a class="btn btn-primary fixed-bottom rounded-circle" id="show-box-btn" href="#body"><i
                class="fas fa-arrow-up"></i></a>
    </section>