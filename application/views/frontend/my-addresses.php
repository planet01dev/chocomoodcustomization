<style>
    .content.products .inbox i.add_wishlist_to_cart {
        position: absolute;
        top: 10px;
        right: 55px;
        bottom: auto;
        left: auto;
        color: #fb7176;
        font-size: 20px;
    }
    .msgbox {
        overflow: scroll;
        height: 600px;
    }
    .msgreceive {
        padding-left: 40px !important;
    }
</style>
<script src="https://js.pusher.com/4.4/pusher.min.js"></script>
<section class="content products checkout address myaccount">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php echo lang('my_account'); ?> <span><?php echo $user->FullName; ?></span></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="wbox">
                    <ul class="nav nav-tabs tabsInBoxes">
                        <li>
                            <a  href="<?php echo base_url('account/profile');?>">
                                <?php echo lang('my_information'); ?>
                                <p class="text-truncate">Dummy Text Alternatively you can take full manual control</p>
                                <span class="iconEd"><i class="fa fa-user"></i></span>
                            </a>
                        </li>
                        <li>
                            <a  href="<?php echo base_url('account/orders');?>">
                                <?php echo lang('orders'); ?>
                                <p class="text-truncate">Dummy Text Alternatively you can take full manual control</p>
                                <span class="iconEd"><i class="fa fa-shopping-cart"></i></span>
                            </a>
                        </li>
                        <li class="active">
                            <a  href="<?php echo base_url('account/addresses');?>">
                                <?php echo lang('my_addresses'); ?>
                                <p class="text-truncate">Dummy Text Alternatively you can take full manual control</p>
                                <span class="iconEd"><i class="fa fa-address-card"></i></span>
                            </a>
                        </li>
                        <li>
                            <a  href="<?php echo base_url('account/wishlist');?>">
                                <?php echo lang('wishlist_items'); ?>
                                <p class="text-truncate">Dummy Text Alternatively you can take full manual control</p>
                                <span class="iconEd"><i class="fa fa-heart"></i></span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                    <div id="address" class="tab-pane fade in active">
                            <ul class="nav nav-tabs tabsInBoxes forAddress">
                                <li>
                                    <a href="javascript:void(0);" >
                                        <?php echo lang('addresses'); ?><small style="font-size: 10px;position: absolute;top: 37px;left: 0;"><?php echo lang('add_and_remove_favorite_address'); ?></small><span class="iconEd"><i class="fa fa-address-card"></i></span>
                                        <p class="text-truncate"><strong>STATIC TEXT </strong> Dummy Text Alternatively you can take full manual control</p>
                                        
                                    </a>
                                </li>
                                <li class="border-none text-right">
                                    <a href="<?php echo base_url('address/add'); ?>"><button class="btn btn-primary"><?php echo lang('add_new_address'); ?>&nbsp;&nbsp;<i class="fa fa-plus"></i></button></a>
                                </li>
                            </ul>
                            <?php if ($addresses && count($addresses) > 0){
                                        foreach ($addresses as $key => $address){ ?>

                                        <div class="address-card" id="AddressID<?php echo $address->AddressID ?>">
                                            <div class="card-header">
                                                <div class="card-title"><?php echo ($key == 0 ? lang('primary') : lang('secondary')); ?></div>
                                            </div>
                                            <div class="card-content">
                                                <h5><?php echo lang('addresses'); ?><span class="iconEd"><i class="fa fa-address-card"></i></span>
                                                    <span class="dropdown">
                                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i class="fa fa-ellipsis-v"></i>
                                                        </button>
                                                        <span class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                            <a class="dropdown-item" href="<?php echo base_url('address/edit/'.$address->AddressID);?>"><?php echo lang('edit') ;?></a>
                                                            <a class="dropdown-item"  onclick="removeIt('address/delete', 'AddressID', <?php echo $address->AddressID ?>);" href="javascript:void(0);"><?php echo lang('delete') ;?></a>
                                                        </span>
                                                    </span>
                                                </h5>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <h6><?php echo lang('Name'); ?></h6>
                                                        <p><?php echo $address->RecipientName; ?></p>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h6><?php echo lang('Address'); ?></h6>
                                                        <p>  <?php echo $address->Street; ?>, <?php echo $address->DistrictTitle; ?>, <?php echo $address->CityTitle; ?></p>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <h6><?php echo lang('PhoneNumber'); ?></h6>
                                                        <p><?php echo $address->MobileNo; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <?php
                                       
                                    }
                            }else{ ?>

                                <p><?php echo lang('no_address_found'); ?></p>
                                <?php

                            }
                            ?>
                            <div class="row nmp">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if (isset($_GET['p'])) { ?>
    <script>
        $('a[href="#<?php echo $_GET['p']; ?>"]').tab('show');
    </script>
<?php } ?>
<script>
    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;
    var pusher = new Pusher('a796cb54d7c4b4ae4893', {
        cluster: 'ap2',
        forceTLS: true
    });
    var channel = pusher.subscribe('Chocomood_Ticket_Channel');
    channel.bind('Chocomood_Ticket_Event', function (data) {
        var my_html = data.my_html;
        var TicketID = data.TicketID;
        $('.TicketID' + TicketID).html(my_html);
        $('.TicketID' + TicketID).animate({scrollTop: $('.msgbox').prop("scrollHeight")}, 1000);
    });
    $(document).ready(function () {
        $('.msgbox').animate({scrollTop: $('.msgbox').prop("scrollHeight")}, 1000);
    });
</script>
