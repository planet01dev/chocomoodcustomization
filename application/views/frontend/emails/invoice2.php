<?php 
if(isset($_POST['submit'])){
	// Always set content-type when sending HTML email
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $to = "adnan.khalid@gloria9.com"; // this is your Email address
    $from = 'test@test.com'; // this is the sender's Email address
    $first_name = $_POST['first_name'];
    $subject = "Form submission" . $first_name;
	$message = '
    <table style="width: 650px;border: 0;" align="center" cellspacing="2">
    <tbody>
     <tr>
       <td>
         <table>
           <tr>
             <td style="vertical-align: bottom; ">
               <table style="width: 325px;">
                 <tr>
                   <td style="vertical-align:top;  "><img src="https://chocomood.henka.tech/assets/frontend/images/invoice/logo.jpg" width="150" alt=""></td>
                    <td>
                       <p style=" margin-left: 10px; font-size:13px;" ><strong>BILL FROM:</strong><br>
                         <Strong>CHOCOMOOD</Strong><br>                      
                          4811 Al Imam<br> Al Shafi -<br> Al Faisaliyah <br>Dist.
                          Unit No 55<br>
                          Jeddah 8261 - 23447<br>
                          Kingdom of Saudi Arabia</p>
                    </td>
                 </tr>
                 <tr>
                    <td colspan="2">
                       <h4 style="border-bottom: 3px solid black; margin-bottom: -3px; font-size:13px;">BILL TO:</h4>
                    </td>
                 </tr>
               </table>
             </td>
             <td style="vertical-align: bottom;width:325px; ">
               <table style="border-bottom: 3px solid black; margin-bottom: 21px;">
                 <tr>
                    <td style="vertical-align:top; font-size: 14px;">
                       <strong>VAT REG No.</strong>
                       <strong>765241788012345</strong>
                    </td>
                    <td style="vertical-align:top;">
                       <img src="https://chocomood.henka.tech/assets/frontend/images/invoice/barCode.jpg" width="150" style="margin-bottom: 20px;">
                    </td>
                 </tr>
                 <tr>
                    <td >
                       <strong style="font-size:13px; vertical-align:top;">ORDER DETAILS:</strong>
                    </td>
                    <td >
                       <img src="https://chocomood.henka.tech/assets/frontend/images/invoice/userIcon.jpg" style=" float: left; margin: 15px 0px 0px 0px; width: 18px;">
                       <strong class="text" style="font-size:13px; vertical-align:top;">CUSTOMER ID: 000510</strong>
                    </td>
                 </tr>
               </table>
             </td>
           </tr>
         </table>
       </td>
     </tr>
       <tr>
          <td  style="border-bottom: 2px solid black;">
             <table>
                <tr>
                   <td style="vertical-align: top;">
                      <table style="width: 325px;">
                         <tr>
                            <td style="font-size:13px; vertical-align: top;">
                               <img src="https://chocomood.henka.tech/assets/frontend/images/invoice/userIcon.jpg" style="width: 18px; float: left;">
                              <p style="margin: 0px 17px;line-height: 1;"><strong>Rami Youssef Khalil</strong><br>
                              +966 508074066</p>
                            </td>
                            <td style="font-size:13px; vertical-align: top;">
                               <img src="https://chocomood.henka.tech/assets/frontend/images/invoice/MapIcon.jpg" style="width: 18px; float: left;" >
                              <p style=" margin: 0px 17px;line-height: 1;"> <strong>Address</strong><br>
                                   4811 Al Imam<br> Al Shafi -<br> Al Faisaliyah <br>Dist.
                                   Unit No 55<br>
                                   Jeddah 8261 - 23447<br>
                                   Kingdom of Saudi Arabia</p>
                            </td>
                         </tr>
                         <tr>
                            <td colspan="2" style="font-size:13px;">
                               <h4 style="border-bottom: 3px solid black; margin: 10px 0px 25px;">RECIPIENT INFORMATION:</h4>
                            </td>
                         </tr>
                         <tr>
                            <td style="vertical-align: top; font-size:13px;">
                               <img src="https://chocomood.henka.tech/assets/frontend/images/invoice/userIcon.jpg" style="width: 18px; float: left;">
                              <p style="margin: 0px;line-height: 1;"> <strong>Abdullah Al Habob</strong><br>
                               +966 583670967</p>
                            </td>
                            <td style="vertical-align: top; font-size:13px;">
                               <img src="https://chocomood.henka.tech/assets/frontend/images/invoice/MapIcon.jpg" style="width: 18px; float: left;" >
                                   <p style=" margin: 0px 17px;line-height: 1;"><strong>Address</strong><br>
                                   4811 Al Imam<br> Al Shafi -<br> Al Faisaliyah <br>Dist.
                                   Unit No 55<br>
                                   Jeddah 8261 - 23447<br>
                                   Kingdom of Saudi Arabia</p>
                            </td>
                         </tr>
                      </table>
                   </td>
                   <td style=" vertical-align: top;">
                      <table cellspacing="3" cellpadding="0" style="width: 325px;">
                         
                         <tr>
                            <td height="30" ><img src="https://chocomood.henka.tech/assets/frontend/images/invoice/invoiceNo.jpg" style="border-left: 1px solid black; border-bottom: 1px solid black; float: left; margin-right: 5px; padding: 0 4px 12px 8px; margin-bottom: 10px; width: 18px;"><p style="font-size:13px;margin: 0px;line-height: 1;"><strong>Invoice No.:</strong><br>8937640990</p></td>
                            <td><img src="https://chocomood.henka.tech/assets/frontend/images/invoice/calender.jpg" style="border-left: 1px solid black; border-bottom: 1px solid black; float: left; margin-right: 5px; padding: 0 4px 12px 8px; margin-bottom: 10px; width: 18px;"><p style="font-size:13px; margin: 0px;line-height: 1;"><strong>Date of issue:</strong><br>08 / 12 / 2019 </p></td>
                         </tr>
                         <tr>
                            <td height="30"><img src="https://chocomood.henka.tech/assets/frontend/images/invoice/timeIcon.jpg" style="border-left: 1px solid black; border-bottom: 1px solid black; float: left; margin-right: 5px; padding: 0 4px 12px 8px; margin-bottom: 10px; width: 18px;"><p style="font-size:13px; margin: 0px;line-height: 1;"><strong>Time of issue:</strong><br>03:31:33 pm </p></td>
                            <td><img src="https://chocomood.henka.tech/assets/frontend/images/invoice/dropBox.jpg" style="border-left: 1px solid black; border-bottom: 1px solid black; float: left; margin-right: 5px; padding: 0 4px 12px 8px; margin-bottom: 10px; width: 18px;"><p style="font-size:13px;margin: 0px;line-height: 1;"><strong>Date of Shipment.:</strong><br>09/ 12/ 2019</p></td>
                         </tr>
                         <tr>   
                            <td height="30"><img src="https://chocomood.henka.tech/assets/frontend/images/invoice/cardIcon.jpg" style="border-left: 1px solid black; border-bottom: 1px solid black; float: left; margin-right: 5px; padding: 0 4px 12px 8px; margin-bottom: 10px; width: 18px;"><p style="font-size:13px; margin: 0px;line-height: 1;"><strong>Payment Method.:</strong><br>Cash on Delivery</p></td>
                            <td><img src="https://chocomood.henka.tech/assets/frontend/images/invoice/pStore.jpg" style="border-left: 1px solid black; border-bottom: 1px solid black; float: left; margin-right: 5px; padding: 0 4px 12px 8px; margin-bottom: 10px; width: 18px;"><p style="font-size:13px; margin: 0px;line-height: 1;"><strong>Pick Up Store:</strong><br>03:31: 33 pm</p></td>
                         </tr>
                         <tr>
                            <td height="30"><img src="https://chocomood.henka.tech/assets/frontend/images/invoice/codeUSed.jpg" style="border-left: 1px solid black; border-bottom: 1px solid black; float: left; margin-right: 5px; padding: 0 4px 12px 8px; margin-bottom: 10px; width: 18px;"><p style="font-size:13px; margin: 0px;line-height: 1;"><strong>Promo Code Used:</strong><br>BLACK0938</p></td>
                            <td><img src="https://chocomood.henka.tech/assets/frontend/images/invoice/codeUSed.jpg" style="border-left: 1px solid black; border-bottom: 1px solid black; float: left; margin-right: 5px; padding: 0 4px 12px 8px; margin-bottom: 10px; width: 18px;"><p style="font-size:13px; margin: 0px;line-height: 1;"><strong>Code Used:</strong><br>BLACK0938</p></td>
                         </tr>
                         <tr>
                            <td height="30"><img src="https://chocomood.henka.tech/assets/frontend/images/invoice/typeShipment.jpg" style="border-left: 1px solid black; border-bottom: 1px solid black; float: left; margin-right: 5px; padding: 0 4px 12px 8px; margin-bottom: 10px; width: 18px;"><p style="font-size:13px; margin: 0px;line-height: 1;"><strong>Type of Shipment:</strong><br>Express</p></td>
                            <td><img src="https://chocomood.henka.tech/assets/frontend/images/invoice/trackOrder.jpg" style="border-left: 1px solid black; border-bottom: 1px solid black; float: left; margin-right: 5px; padding: 0 4px 12px 8px; margin-bottom: 10px; width: 18px;"><p style="font-size:13px; margin: 0px;line-height: 1;"><strong>Tracking Order No:</strong><br>893784672</p></td>
                         </tr>
                         <tr>
                            <td height="30"><img src="https://chocomood.henka.tech/assets/frontend/images/invoice/dateDeliver.jpg" style="border-left: 1px solid black; border-bottom: 1px solid black; float: left; margin-right: 5px; padding: 0 4px 12px 8px; margin-bottom: 10px; width: 18px;"> <p style="font-size:13px; margin: 0px;line-height: 1;"><strong>Date of Delivery:</strong><br>09/ 12/ 2019</p></td>
                            <td><img src="https://chocomood.henka.tech/assets/frontend/images/invoice/status.jpg" style="border-left: 1px solid black; border-bottom: 1px solid black; float: left; margin-right: 5px; padding: 0 4px 12px 8px; margin-bottom: 10px; width: 18px;"><p style="font-size:13px; margin: 0px;line-height: 1;"><strong>Status:</strong><br>Pending</p></td>
                         </tr>
                      </table>
                   </td>
                </tr>
             </table>
          </td>
       </tr>
       <tr>
          <td>
           <h2 style="font-size: 18px;  margin-top: 15px;">Invoice</h2>
         </td>
       </tr>
       <tr>
          <td style="width: 350px;">
             <table cellspacing="0" align="center" cellpadding="7" border="1" style="font-size:13px;">
                         <thead>
                             <tr>
                                 <th>SN</th>   
                                 <th>SKU</th>
                                 <th>Discription</th>
                                 <th>Unit</th>
                                 <th>QTY</th>
                                 <th>Price</th>
                                 <th>Sub Total</th>
                                 <th>VAT</th>
                                 <th>VAT Rate</th>
                                 <th>TOTAL</th>
                             </tr>
                         </thead>
                         <tbody>
                             <tr>
                                 <td>1</td>
                                 <td>5780525</td>
                                 <td>5780525AKCESORIA PŁYWACKIEA</td>
                                 <td></td>
                                 <td>2</td>
                                 <td>15.40 SR</td>
                                 <td>30.80 SR</td>
                                 <td>5%</td>
                                 <td>1.54 SR</td>
                                 <td>32.34 SR</td>
                             </tr>
                            <tr>
                                 <td>2</td>
                                 <td>5780526</td>
                                 <td>5780525AKCESORIA PŁYWACKIEA</td>
                                 <td></td>
                                 <td>2</td>
                                 <td>15.40 SR</td>
                                 <td>30.80 SR</td>
                                 <td>5%</td>
                                 <td>1.54 SR</td>
                                 <td>32.34 SR</td>
                             </tr>
                             <tr>
                                 <td>3</td>
                                 <td>5780527</td>
                                 <td>5780525AKCESORIA PŁYWACKIEA</td>
                                 <td></td>
                                 <td>2</td>
                                 <td>15.40 SR</td>
                                 <td>30.80 SR</td>
                                 <td>5%</td>
                                 <td>1.54 SR</td>
                                 <td>32.34 SR</td>
                             </tr>
                             <tr>
                                 <td>4</td>
                                 <td>5780528</td>
                                 <td>5780525AKCESORIA PŁYWACKIEA</td>
                                 <td></td>
                                 <td>2</td>
                                 <td>15.40 SR</td>
                                 <td>30.80 SR</td>
                                 <td>5%</td>
                                 <td>1.54 SR</td>
                                 <td>32.34 SR</td>
                             </tr>
                             <tr>
                                 <td>5</td>
                                 <td>5780529</td>
                                 <td>5780525AKCESORIA PŁYWACKIEA</td>
                                 <td></td>
                                 <td>2</td>
                                 <td>15.40 SR</td>
                                 <td>30.80 SR</td>
                                 <td>5%</td>
                                 <td>1.54 SR</td>
                                 <td>32.34 SR</td>
                             </tr>
                             <tr>
                                 <td colspan="6" style="text-align: right;"><strong>Total</strong></td>
                                 <td>154 SAR</td>
                                 <td>5%</td>
                                 <td>7.7 SR</td>
                                 <td>161.7 SR</td>
                             </tr>
                         </tbody>
                         <tfoot>
                            <tr>
                               <td style="border:none;" colspan="3"><p>Transactions made based on gross total</p></td>
                               <td style="border:none; text-align: right;" colspan="6"><strong>Shipment Charges:</strong> <img src="https://chocomood.henka.tech/assets/frontend/images/invoice/shipping.jpg" style=" float: right; margin-left: 5px; width: 18px;"></td>
                               <td>161.7 SR</td>
                            </tr>
                            <tr>
                               <td style="border:none; text-align: right;" colspan="9"><strong>Discount:</strong><img src="https://chocomood.henka.tech/assets/frontend/images/invoice/cardDiscount.jpg" style=" float: right; margin-left: 5px; width: 18px;"></td>
                               <td class="text-center clr">161.7 SR</td>
                            </tr>
                            <tr>
                               <td style="border:none; text-align: right;" colspan="9"><strong>Grand Total:</strong> <img src="https://chocomood.henka.tech/assets/frontend/images/invoice/grandTotal.jpg" style=" float: right; margin-left: 5px; width: 18px;"></td>
                               <td class="text-center clr">161.7 SR</td>
                            </tr>
                         </tfoot>
                     </table>
          </td>
       </tr>
       <tr>
          <td>
             <h2 style="text-align: center; margin-top: 50px; font-size: 15px;">THANK YOU FOR YOUR BUSINESS!</h2>
             <p style="text-align: center; font-size:13px;"><b>PLEASE NOTE:</b> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          </td>
       </tr>
    </tbody>
 </table>
 
    
	';

    $headers .= "From:" . $from;
    mail($to,$subject,$message,$headers);
    echo "Mail Sent. Thank you " . $first_name . ", we will contact you shortly.";
    }
?>

<!DOCTYPE html>
<head>
<title>Form submission</title>
</head>
<body>

<form action="" method="post">
First Name: <input type="text" name="first_name"><br>
<input type="submit" name="submit" value="Submit">
</form>

</body>
</html> 

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Invoice</title>
  </head>
  <body>

  <div class="container-fluid ">
    <div class="row content ">
        <div class="col-md-6 bill-info">
            <div class="row">
                <div class="col-md-6 logo">
                    <img src="logo-2.jpg">
                </div>
                <div class="col-md-6 address">
                    <h5>
                        BILL FROM:
                    </h5>
                    <h5>
                        CHOCOMOOD
                    </h5>
                    <p>4811 Al Imam Al Shafi - Al Faisaliyah Dist.<br>
                        Unit No 55<br>
                        Jeddah 8261 - 23447<br>
                        Kingdom of Saudi Arabia</p>
                </div>
            </div>

            <div class="col-md-12">
                <h4 class="bill-text">BILL TO:</h4>
                <div class="row">
                    <div class="col-md-6 client-info">
                        <img src="https://chocomood.henka.tech/assets/frontend/images/invoice/userIcon.jpg"
                            class="user-icon">
                        <strong>Rami Youssef Khalil</strong>
                        <p>+966 508074066</p>
                    </div>
                    <div class="col-md-6 client-info">
                        <img src="https://chocomood.henka.tech/assets/frontend/images/invoice/MapIcon.jpg"
                            class="user-icon">
                        <strong>Address</strong>
                        <p>4811 Al Imam Al Shafi - Al Faisaliyah Dist.<br>
                            Unit No 55<br>
                            Jeddah 8261 - 23447<br>
                            Kingdom of Saudi Arabia</p>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <h4 class="recipient-info">RECIPIENT INFORMATION:</h4>
                <div class="row">
                    <div class="col-md-6 client-info">
                        <img src="https://chocomood.henka.tech/assets/frontend/images/invoice/userIcon.jpg"
                            class="user-icon">
                        <strong>Abdullah Al Habob</strong>
                        <p>+966 583670967</p>
                    </div>
                    <div class="col-md-6 client-info">
                        <img src="https://chocomood.henka.tech/assets/frontend/images/invoice/MapIcon.jpg"
                            class="user-icon">
                        <strong>Address</strong>
                        <p>4811 Al Imam Al Shafi - Al Faisaliyah Dist.<br>
                            Unit No 55<br>
                            Jeddah 8261 - 23447<br>
                            Kingdom of Saudi Arabia</p>
                    </div>
                </div>
            </div>

        </div>
        <!--2nd Column-->
        <div class="col-md-6 shopping-info">
            <div class="row receipt-no">
                <div class="col-md-6">
                    <h4 class="reg-no">VAT REG No.</h4>
                    <h3>765241789012345</h3>
                </div>
                <div class="col-md-6">
                    <img src="https://chocomood.henka.tech/assets/frontend/images/invoice/barCode.jpg"
                        class="img-responsive">
                </div>
            </div>

            <div class="row mb-3 details">
                <div class="col-md-6">
                    <strong class="text">ORDER DETAILS:</strong>
                </div>
                <div class="col-md-6">
                    <img src="https://chocomood.henka.tech/assets/frontend/images/invoice/userIcon.jpg" class="id-icon">
                    <strong class="text">CUSTOMER ID: 000510</strong>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 order-info">
                    <img src="https://chocomood.henka.tech/assets/frontend/images/invoice/invoiceNo.jpg" class="icon">
                    <strong>Invoice No.:</strong>
                    <p>8937640990</p>
                </div>
                <div class="col-md-6 order-info">
                    <img src="https://chocomood.henka.tech/assets/frontend/images/invoice/calender.jpg" class="icon">
                    <strong>Date of issue:</strong>
                    <p>08/12/2019</p>
                </div>

                <div class="col-md-6 order-info">
                    <img src="https://chocomood.henka.tech/assets/frontend/images/invoice/timeIcon.jpg" class="icon">
                    <strong>Time of issue:</strong>
                    <p>03:31:33 pm</p>
                </div>
                <div class="col-md-6 order-info">
                    <img src="https://chocomood.henka.tech/assets/frontend/images/invoice/dropBox.jpg" class="icon">
                    <strong>Date of Shipment.:</strong>
                    <p>09/12/2019</p>
                </div>

                <div class="col-md-6 order-info">
                    <img src="https://chocomood.henka.tech/assets/frontend/images/invoice/cardIcon.jpg" class="icon">
                    <strong>Payment Menthod.:</strong>
                    <p>Cash on Delivery</p>
                </div>
                <div class="col-md-6 order-info">
                    <img src="https://chocomood.henka.tech/assets/frontend/images/invoice/pStore.jpg" class="icon">
                    <strong>Pick Up Store:</strong>
                    <p>Jeddah / Phalastine Street</p>
                </div>

                <div class="col-md-6 order-info">
                    <img src="https://chocomood.henka.tech/assets/frontend/images/invoice/codeUSed.jpg" class="icon">
                    <strong>Promo Code Used:</strong>
                    <p>BLACK0938</p>
                </div>
                <div class="col-md-6 order-info">
                    <img src="https://chocomood.henka.tech/assets/frontend/images/invoice/codeUSed.jpg" class="icon">
                    <strong>Code Used:</strong>
                    <p>BLACK0938</p>
                </div>

                <div class="col-md-6 order-info">
                    <img src="https://chocomood.henka.tech/assets/frontend/images/invoice/typeShipment.jpg"
                        class="icon">
                    <strong>Type of Shipment:</strong>
                    <p>Express</p>
                </div>
                <div class="col-md-6 order-info">
                    <img src="https://chocomood.henka.tech/assets/frontend/images/invoice/trackOrder.jpg" class="icon">
                    <strong>Tracking Order No.</strong>
                    <p>893784672</p>
                </div>

                <div class="col-md-6 order-info">
                    <img src="https://chocomood.henka.tech/assets/frontend/images/invoice/dateDeliver.jpg" class="icon">
                    <strong>Date of Delivery:</strong>
                    <p>09/12/2019</p>
                </div>
                <div class="col-md-6 order-info">
                    <img src="https://chocomood.henka.tech/assets/frontend/images/invoice/status.jpg" class="icon">
                    <strong>Status:</strong>
                    <p>Pending</p>
                </div>

            </div>
        </div>
    </div>


    <div class="row customer-invoice">
        <div class="col-md-12">
            <h1>Invoice</h1>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                <H6>SN</H6>
                                            </th>
                                            <th class="text-center">
                                                <h6>SKU</h6>
                                            </th>
                                            <th class="text-center">
                                                <h6>Discription</h6>
                                            </th>
                                            <th class="text-center">
                                                <h6>Unit</h6>
                                            </th>
                                            <th class="text-center">
                                                <h6>QTY</h6>
                                            </th>
                                            <th class="text-center">
                                                <h6>Price</h6>
                                            </th>
                                            <th class="text-center">
                                                <h6>Sub Total</h6>
                                            </th>
                                            <th class="text-center">
                                                <h6>VAT</h6>
                                            </th>
                                            <th class="text-center">
                                                <h6>VAT Rate</h6>
                                            </th>
                                            <th class="text-center">
                                                <h6>TOTAL</h6>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center">1</td>
                                            <td class="text-center">5780525</td>
                                            <td class="text-center">5780525AKCESORIA PŁYWACKIEA</td>
                                            <td></td>
                                            <td class="text-center">2</td>
                                            <td class="text-center">15.40 SR</td>
                                            <td class="text-center">30.80 SR</td>
                                            <td class="text-center">5%</td>
                                            <td class="text-center">1.54 SR</td>
                                            <td class="text-center">32.34 SR</td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">2</td>
                                            <td class="text-center">5780526</td>
                                            <td class="text-center">5780525AKCESORIA PŁYWACKIEA</td>
                                            <td></td>
                                            <td class="text-center">2</td>
                                            <td class="text-center">15.40 SR</td>
                                            <td class="text-center">30.80 SR</td>
                                            <td class="text-center">5%</td>
                                            <td class="text-center">1.54 SR</td>
                                            <td class="text-center">32.34 SR</td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">3</td>
                                            <td class="text-center">5780527</td>
                                            <td class="text-center">5780525AKCESORIA PŁYWACKIEA</td>
                                            <td></td>
                                            <td class="text-center">2</td>
                                            <td class="text-center">15.40 SR</td>
                                            <td class="text-center">30.80 SR</td>
                                            <td class="text-center">5%</td>
                                            <td class="text-center">1.54 SR</td>
                                            <td class="text-center">32.34 SR</td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">4</td>
                                            <td class="text-center">5780528</td>
                                            <td class="text-center">5780525AKCESORIA PŁYWACKIEA</td>
                                            <td></td>
                                            <td class="text-center">2</td>
                                            <td class="text-center">15.40 SR</td>
                                            <td class="text-center">30.80 SR</td>
                                            <td class="text-center">5%</td>
                                            <td class="text-center">1.54 SR</td>
                                            <td class="text-center">32.34 SR</td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">5</td>
                                            <td class="text-center">5780529</td>
                                            <td class="text-center">5780525AKCESORIA PŁYWACKIEA</td>
                                            <td></td>
                                            <td class="text-center">2</td>
                                            <td class="text-center">15.40 SR</td>
                                            <td class="text-center">30.80 SR</td>
                                            <td class="text-center">5%</td>
                                            <td class="text-center">1.54 SR</td>
                                            <td class="text-center">32.34 SR</td>
                                        </tr>
                                        <tr>
                                            <td class="text-right" colspan="6"><strong>Total</strong></td>
                                            <td class="text-center clr">154 SAR</td>
                                            <td class="text-center clr">5%</td>
                                            <td class="text-center clr">7.7 SR</td>
                                            <td class="text-center clr">161.7 SR</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td class="text-left no-borders" colspan="3">
                                                <p>Transactions made based on gross total</p>
                                            </td>
                                            <td class="text-right no-borders" colspan="6"><strong>Shipment
                                                    Charges:</strong> <img
                                                    src="https://chocomood.henka.tech/assets/frontend/images/invoice/shipping.jpg"
                                                    class="td-icon"></td>
                                            <td class="text-center clr">161.7 SR</td>
                                        </tr>
                                        <tr>
                                            <td class="text-right no-borders" colspan="9"><strong>Discount:</strong><img
                                                    src="https://chocomood.henka.tech/assets/frontend/images/invoice/cardDiscount.jpg"
                                                    class="td-icon"></td>
                                            <td class="text-center clr">161.7 SR</td>
                                        </tr>
                                        <tr>
                                            <td class="text-right no-borders" colspan="9"><strong>Grand Total:</strong>
                                                <img src="https://chocomood.henka.tech/assets/frontend/images/invoice/grandTotal.jpg"
                                                    class="td-icon"></td>
                                            <td class="text-center clr">161.7 SR</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row note">
        <div class="col-md-12">
            <h2 class="text-center">THANK YOU FOR YOUR BUSINESS!</h2>
            <p><b>PLEASE NOTE:</b> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                incididunt ut la
                bore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                aliquip ex e
                a commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                fugiat nulla p
                ariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim
                id est laborum.
            </p>
        </div>
    </div>
</div>
</body>
</html>

<style type="text/css">
	.content {
		margin: 30px;
		border-bottom: 3px solid black;	
	}
	.address p {
		font-size: 18px;
	}
	.address h5 {
		font-size: 20px;
	}
	.bill-text {
		border-bottom: 3px solid black;
		width: 90%;
		
	}

	.recipient-info {
		border-bottom: 3px solid black;
		width: 90%;
	}
	
	.text {
		font-size: 20px;
	}
	.reg-no {
		margin-top: 20px;
	}
	.receipt-no {
		
		width: 90%;
		margin-bottom: 20px;
	}
	.details {
		border-bottom: 3px solid black;
	}
	.user-info {
		margin-bottom: 10px;
		border-bottom: 3px solid black;

	}
	.customer-invoice {
		margin: 50px;
	}
	.note {
		margin-top: 50px;
		margin-bottom: 30px;
		margin-left: 50px;
		margin-right: 50px;
		text-align: center; 
	}
	.note h2 {
		margin-bottom: 25px;
	}
	td.no-borders {
		border: none;
	}
	td.clr {
		background-color: #E5F6AA;
	} 
	.client-info p {
		margin-left: 40px;
		font-size: 18px;
	}
	.client-info strong {
		font-size: 20px;
	}
	.recipient-info p {
		margin-left: 40px;
		font-size: 18px;
	}
	.recipient-info strong {
		font-size: 20px;
	}
	.order-info p {
		margin-left: 55px;
	}
	.icon {
    border-left: 1px solid black;
    border-bottom: 1px solid black;
    float: left;
    margin-right: 10px;
    padding: 0 10px 24px 14px;
    margin-bottom: 15px;
    width: 60px;
    height: 60px;
	}
	.img-responsive {
		max-width: 100%;
	}
	.user-icon {
    padding: 0 5px 20px 5px;
    margin-top: 10px;
    height: 50px;
	}
	.id-icon {
	padding: 0 5px 5px 5px;
    height: 40px;
	}
	.td-icon {
	padding: 0 5px 5px 5px;
    height: 40px;
	}
</style>