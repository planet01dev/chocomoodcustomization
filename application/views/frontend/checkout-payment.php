<section class="content checkout address payment">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Shopping <span>Check Out</span></h2>
            </div>
            <div class="col-md-8">
                <div class="wbox">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Shipping And Payment
                                        Address</a>
                                    <a href="<?php echo base_url('address'); ?>"><span>Change</span></a>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <?php
                                    if (isset($_COOKIE['CollectFromStore']) && $_COOKIE['CollectFromStore'] == 1) { ?>
                                        <h5><?php echo lang('Collect_From_Store'); ?></h5>
                                    <?php } else { ?>
                                        <h5 style="font-weight: bold;"><?php echo lang('Delivery_Address'); ?></h5>
                                        <h5>
                                            <?php echo $address[0]->RecipientName; ?>
                                            <span><?php echo $address[0]->MobileNo; ?></span>
                                        </h5>
                                        <h5>
                                            <span><?php echo lang('email'); ?>: <?php echo $address[0]->Email; ?></span>
                                            <span><?php echo lang('city'); ?>: <?php echo $address[0]->CityTitle; ?></span>
                                            <span><?php echo lang('district'); ?>: <?php echo $address[0]->DistrictTitle; ?></span>
                                            
                                            <span><?php echo lang('street'); ?>: <?php echo $address[0]->Street; ?></span>
                                            
                                        </h5>
                                        <h5>
                                            <a href="https://www.google.com/maps/search/?api=1&amp;query=<?php echo $address[0]->Latitude; ?>,<?php echo $address[0]->Longitude; ?>"
                                               target="_blank">
                                               <?php echo lang('google_pin_link'); ?>
                                            </a>
                                        </h5>
                                        <br>
                                        <!--<h5 style="font-weight: bold;">Payment Collection Address</h5> -->
                                        <?php
                                        if ($address[0]->AddressID == $address_for_payment_collection[0]->AddressID)
                                        { ?>
                                            <!--<h5>Same as shipping address</h5>-->
                                        <?php } else { ?>
                                            <!--<h5>
                                                <?php echo $address_for_payment_collection[0]->RecipientName; ?>
                                                <span><?php echo $address_for_payment_collection[0]->MobileNo; ?></span>
                                            </h5>
                                            <h5>
                                                <span>Email: <?php echo $address_for_payment_collection[0]->Email; ?></span>
                                                <span>City: <?php echo $address_for_payment_collection[0]->CityTitle; ?></span>
                                                <span>District: <?php echo $address_for_payment_collection[0]->DistrictTitle; ?></span>
                                                <span>Building No: <?php echo $address_for_payment_collection[0]->BuildingNo; ?></span>
                                                <span>Street: <?php echo $address_for_payment_collection[0]->Street; ?></span>
                                                <span>P.O Box: <?php echo $address_for_payment_collection[0]->POBox; ?></span>
                                                <span>Zip Code: <?php echo $address_for_payment_collection[0]->ZipCode; ?></span>
                                            </h5>
                                            <h5>
                                                <a href="https://www.google.com/maps/search/?api=1&amp;query=<?php echo $address_for_payment_collection[0]->Latitude; ?>,<?php echo $address_for_payment_collection[0]->Longitude; ?>"
                                                   target="_blank">
                                                    Google Pin Link
                                                </a>
                                            </h5> -->
                                        <?php }
                                        ?>
                                    <?php }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Shipment Method</a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <h4>Shipment Method</h4>
                                    <?php
                                    $shipment_methods = getTaxShipmentCharges('Shipment');
                                    foreach ($shipment_methods as $shipment_method) { ?>
                                        <label class="customradio default"><?php echo $shipment_method->Title; ?>
                                            <input type="radio" name="shipment_method"
                                                   value="<?php echo $shipment_method->TaxShipmentChargesID; ?>" <?php echo($shipment_method->TaxShipmentChargesID == $this->session->userdata('ShipmentMethodIDForBooking') ? 'checked' : ''); ?>
                                                   onclick="changeShipmentMethod(this.value);">
                                            <span class="checkmark"></span>
                                        </label>
                                    <?php }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Payment
                                        Method</a>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <h4>Payment Method<span>All transactions are secured and encrypted</span></h4>
                                    <label class="customradio default">Cash On Delivery
                                        <input type="radio" name="payment_method" value="COD" onclick="changePaymentMethod(this.value);" checked>
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="customradio default">Credit / Debit Card
                                        <input type="radio" name="payment_method" value="Card" onclick="changePaymentMethod(this.value);">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="customradio default">Sadad
                                        <input type="radio" name="payment_method" value="Sadad" onclick="changePaymentMethod(this.value);">
                                        <span class="checkmark"></span>
                                    </label>
                                    <!--<label class="customcheck" style="border: none !important;">
                                        I accept the <a href="javascript:void(0)" data-toggle="modal" data-target="#TermsConditions">Terms and conditions</a>
                                        <input type="checkbox" class="acceptTerms">
                                        <span class="checkmark"></span>
                                    </label>
                                    <button type="button" class="btn btn-primary" onclick="placeOrder();">Complete
                                        Order
                                    </button>-->
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">Select Branch</a>
                                </h4>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <h4>Store Brach<span>Select your brach and confirm if product available.</span></h4>
                                    
                                   <?php if(!empty($available_cities)){ ?>

                        
                         <div class="form-group edSelectStyle">
                            <select class="form-control" id="StoreID" name="StoreID">
                                <?php 
                                $ontime_array = array();
                                
                                $brach_option = '';
                                foreach ($available_cities as $key => $value) {
                                            if($value->DistrictID != ''){
                                            $branch_delivery_district = getBranchDistrict($value->DistrictID,$language); 
                                            if($branch_delivery_district){
                                                foreach ($branch_delivery_district as $district) {
                                                    
                                                        $brach_option .= '<option value="'.$district->DistrictID.'" class="hide store-'.$value->StoreID.'" '.($key != 0 ? 'style="display:none;"':'').'>'.$district->Title.'</option>';
                                                    

                                                    
                                                }
                                            }
                                            if(!in_array($value->CityID,$ontime_array)){
                                                    if($key != 0){
                                                        echo  '</optgroup>';
                                                    }
                                                echo '<optgroup label="'.$value->CityTitle.'">';
                                            }
                                            echo '<option value="'.$value->StoreID.'">'.$value->StoreTitle.'</option>';
                                            $ontime_array[] = $value->CityID;
                                    ?>
                                <?php }  } ?>
                                </select>
                            </div>
                     <?php } ?>

                     <?php if(!empty($available_cities)){ ?>

                        
                         <div class="form-group edSelectStyle">
                            <select class="form-control" name="BranchDeliveryDistrictID" id="BranchDeliveryDistrictID">
                               <?php echo $brach_option; ?>
                            </select>
                            </div>
                     <?php } ?>
                                    <label class="customcheck" style="border: none !important;">
                                        I accept the <a href="javascript:void(0)" data-toggle="modal" data-target="#TermsConditions">Terms and conditions</a>
                                        <input type="checkbox" class="acceptTerms">
                                        <span class="checkmark"></span>
                                    </label>
                                    <button type="button" class="btn btn-primary" onclick="placeOrder();">Complete
                                        Order
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="wbox side">
                    <div class="row">
                        <div class="col-md-12">
                            <h6>Basket</h6>
                        </div>
                    </div>
                    <?php
                    $total = 0;
                    foreach ($cart_items as $cart_item) { ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <?php
                                if ($cart_item->ItemType == 'Product') { ?>
                                    <img src="<?php echo base_url(get_images($cart_item->ProductID, 'product', false)); ?>">
                                    <h5><?php echo $cart_item->Title; ?>
                                        <span><?php echo $cart_item->Quantity; ?><?php echo ($cart_item->IsCorporateItem ? ' kg' : 'pcs'); ?></span>
                                    </h5>
                                <?php } elseif ($cart_item->ItemType == 'Customized Shape') { ?>
                                    <img src="<?php echo base_url($cart_item->CustomizedShapeImage); ?>">
                                    <h5>Customized Shape
                                        <span><?php echo $cart_item->Quantity; ?>pcs</span>
                                    </h5>
                                <?php } else { ?>
                                    <a href="javascript:void(0);" class="chocobox_detail" title="Click to view whats inside" data-box_id="<?php echo $cart_item->CustomizedBoxID; ?>" data-pids="<?php echo $cart_item->CustomizedOrderProductIDs; ?>" data-box_type="<?php echo $cart_item->ItemType; ?>">
                                        <img src="<?php echo front_assets("images/".$cart_item->ItemType.".png"); ?>">
                                        <h5><?php echo $cart_item->ItemType; ?>
                                            <span><?php echo $cart_item->Quantity; ?>pcs</span>
                                        </h5>
                                    </a>
                                <?php } ?>

                                <div class="price"><?php echo number_format($cart_item->TempItemPrice, 2); ?> <?php echo lang('sar'); ?></div>
                            </div>
                        </div>
                        <?php $total += $cart_item->TempItemPrice * $cart_item->Quantity;
                    }
                    ?>
                    <div class="row last">
                        <div class="col-md-12">
                            <?php
                            if ($this->session->userdata('order_coupon')) {
                                $order_coupon = $this->session->userdata('order_coupon');
                                $coupon_code = $order_coupon['CouponCode'];
                                $coupon_discount_percentage = $order_coupon['DiscountPercentage'];
                                $coupon_discount_availed = ($order_coupon['DiscountPercentage'] / 100) * $total;
                                $total = $total - $coupon_discount_availed;
                                ?>
                                <p><span>Promo Applied:</span> <strong><?php echo $coupon_code; ?></strong></p>
                                <p><span>Promo Discount %:</span>
                                    <strong><?php echo $coupon_discount_percentage; ?></strong></p>
                                <p><span>Promo Discount Availed:</span> <strong><?php echo $coupon_discount_availed; ?>
                                        <?php echo lang('sar'); ?></strong></p>
                            <?php } ?>
                            <!--<h6>
                                <span>Shipping VAT 5%</span>
                                <strong>20.00 sar 7.00 sar</strong>
                            </h6>-->
                            <ol>
                                <?php
                                $ShipmentMethodID = $this->session->userdata('ShipmentMethodIDForBooking');
                                $shipping_amount = 0;
                                $shipment_method = getSelectedShippingMethodDetail($ShipmentMethodID, $language);
                                if ($shipment_method) {
                                    $shipping_title = $shipment_method->Title;
                                    $shipping_factor = $shipment_method->Type == 'Fixed' ? number_format($shipment_method->Amount, 2) . ' SAR' : $shipment_method->Amount . '%';
                                    if ($shipment_method->Type == 'Fixed') {
                                        $shipping_amount = $shipment_method->Amount;
                                    } elseif ($shipment_method->Type == 'Percentage') {
                                        $shipping_amount = ($shipment_method->Amount / 100) * ($total);
                                    }
                                    ?>
                                    <li>
                                                <span><i class="fa fa-truck"
                                                         aria-hidden="true"></i> <?php echo $shipping_title; ?></span>
                                        <strong id="ShippingAmount"><?php echo number_format($shipping_amount, 2); ?>
                                            <?php echo lang('sar'); ?></strong>
                                    </li>
                                <?php }
                                ?>
                                <?php
                                $total_tax = 0;
                                $taxes = getTaxShipmentCharges('Tax');
                                foreach ($taxes as $tax) {
                                    $tax_title = $tax->Title;
                                    $tax_factor = $tax->Type == 'Fixed' ? number_format($tax->Amount, 2) . ' SAR' : $tax->Amount . '%';
                                    if ($tax->Type == 'Fixed') {
                                        $tax_amount = $tax->Amount;
                                    } elseif ($tax->Type == 'Percentage') {
                                        $tax_amount = ($tax->Amount / 100) * ($total + $shipping_amount);
                                    }
                                    ?>
                                    <li>
                                                <span><i class="fa fa-file-text-o"
                                                         aria-hidden="true"></i> <?php echo $tax_title; ?> <?php echo $tax_factor; ?></span>
                                        <strong id="TaxAmount"><?php echo number_format($tax_amount, 2); ?> <?php echo lang('sar'); ?></strong>
                                    </li>
                                    <?php
                                    $total_tax += $tax_amount;
                                }
                                $total = $total + $shipping_amount + $total_tax;
                                ?>
                            </ol>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-secondary">
                                <span>Grand Total</span><strong><?php echo number_format($total, 2); ?> <?php echo lang('sar'); ?></strong>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    // Code to disable browser back button
    $(document).ready(function () {
        $('#StoreID').on('change',function(){

            var store_va = $("#StoreID option:selected").val();
            $('.hide').hide();
            $('.store-'+store_va).show();
            $('.store-'+store_va).attr('style', 'display: block !important');;
        });
       /* window.history.pushState(null, "", window.location.href);
        window.onpopstate = function () {
            window.history.pushState(null, "", window.location.href);
        };*/
    });
</script>