    <!-- main js  -->
    <script type="text/javascript" src="<?php echo front_assets(); ?>zoom_img/js/main.js"></script>
    <!-- js bootstrap -->
    <!-- <script type="text/javascript" src="<?php echo front_assets(); ?>zoom_img/js/library/bootstrap/bootstrap.min.js"></script> -->
    <!-- js font awesome -->
    <!-- <script type="text/javascript" src="<?php echo front_assets(); ?>zoom_img/js/library/font awesome/all.min.js"></script> -->
    <!-- js drift -->
    <script type="text/javascript" src="<?php echo front_assets(); ?>zoom_img/js/library/drift/Drift.min.js"></script>
    <!-- js draft Zoom || Product Img Zoom -->
    <script type="text/javascript" src="<?php echo front_assets(); ?>zoom_img/js/library/drift/drift.js"></script>
    <!-- Js folder -->
    <!-- <script type="text/javascript" src="<?php echo front_assets(); ?>zoom_img/js/parts/chocolate-box.js"></script>
    <script type="text/javascript" src="<?php echo front_assets(); ?>zoom_img/js/parts/customize-box.js"></script>
    <script type="text/javascript" src="<?php echo front_assets(); ?>zoom_img/js/parts/customize-shape.js"></script> -->



    <!-- <link rel="stylesheet" type="text/css" href="<?php echo front_assets(); ?>zoom_img/css/rtl.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo front_assets(); ?>zoom_img/css/style.css"/> -->
    <link rel="stylesheet" type="text/css" href="<?php echo front_assets(); ?>zoom_img/css/parts/product-details.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo front_assets(); ?>zoom_img/css/parts/slider.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo front_assets(); ?>zoom_img/css/parts/customer-comment.css"/>
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo front_assets(); ?>zoom_img/css/parts/invoice.css"/> -->
    <link rel="stylesheet" type="text/css" href="<?php echo front_assets(); ?>zoom_img/css/parts/rating/rating.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo front_assets(); ?>zoom_img/css/parts/rating/set-rating.css"/>
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo front_assets(); ?>zoom_img/css/parts/customize/chocolate-box.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo front_assets(); ?>zoom_img/css/parts/customize/customize-box.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo front_assets(); ?>zoom_img/css/parts/customize/customize-mask-png.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo front_assets(); ?>zoom_img/css/parts/customize/customize-shape.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo front_assets(); ?>zoom_img/css/parts/customize/customize-shape-category.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo front_assets(); ?>zoom_img/css/parts/customize/customize-shape-details.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo front_assets(); ?>zoom_img/css/parts/customize/custom-new.css"/> -->
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo front_assets(); ?>zoom_img/css/library/bootstrap/bootstrap.min.css" /> -->
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo front_assets(); ?>zoom_img/css/library/font awesome/all.min.css" /> -->
    <link rel="stylesheet" type="text/css" href="<?php echo front_assets(); ?>zoom_img/css/library/drift/drift-basic.css" />
    <style>
        .ar .drift-zoom-pane {
            left: 100%;
            width:80%;
            height:60%;
        }
        .drift-zoom-pane {
            left: -78%;
            width:79%;
            height:60%;
        }
    </style>



<?php
                $ratings = productRatings($product->ProductID);

                ?>
<div class="modal fade" id="chk-avail" tabindex="1" role="dialog" aria-labelledby="exampleModalCenterTitle"
  aria-hidden="true">

  <!-- Add .modal-dialog-centered to .modal-dialog to vertically center the modal -->
  <div class="modal-dialog modal-dialog-centered modWl mi-modal" role="document">


    <div class="modal-content mod-cont">
      <div class="modal-header mod_hed" >
        <h5 class="modal-title hdTxt" id="exampleModalLongTitle"><?php echo lang('check');?> <span><?php echo lang('availability'); ?></span> </h5>
      </div>
      <div class="modal-body mod-bod">
        <div class="frmcity">
           <p><strong><?php echo lang('please_select_city'); ?></strong></p>
              <div class="form-group edSelectstyle  white" >
                   <select class="fmod form-control check_availability" style="">
                      <option><?php echo lang('check');?> <?php echo lang('availability'); ?></option>
                       <?php 
                       $branches_html = '';
                       $available = false;

                       if($available_cities){
                        $ontime_array = array();
                                               
                                  foreach ($available_cities as $key => $value) {
                                            $qty  = "";
                                            $unit = ($product->PriceType == 'kg')? "Grams" : lang('pcs');
                                            if($product->PriceType == 'kg'){
                                                $qty = $value->GramQuantity;
                                            }else{
                                                $qty = $value->Quantity;
                                            }
                                            if($qty > 0){
                                                $available = true;
                                            }

                                            if($qty == '' || $qty == 0){
                                                $quantity = '<span class="redTxt">Sold Out</span>';
                                            }elseif($qty >= 6 && $qty <= 10){
                                                $quantity = '<span class="gTxt">'.$qty.' '.lang('pcs').' '.lang('available').' </span>';
                                            }elseif($qty <= 5 && $qty > 0){
                                                $quantity = '<span class="orangeTxt">'.lang('last').' '.$qty.' '.$unit.'</span>';
                                            }else{
                                                $quantity = '<span class="gTxt">'.lang('available').'</span>';
                                            }




                                                if(!in_array($value->CityID,$ontime_array)){?>
                                        
                                        <option value="<?php echo $value->CityID; ?>"><?php echo $value->CityTitle; ?></option> 

                                    <?php
                                }

                                $branches_html .= '<div class="branchTXT check_availability_city city-'.$value->CityID.'" style="display:none;">
                                                 <p class="hTxt"><strong>'.$value->StoreTitle.'</strong><br>
                                                 <span>'.$value->Address.'</span><br>
                                                '.$quantity.'</p>
                                             </div>';
                                $ontime_array[] = $value->CityID;
                                 }
                       }

                       ?>
                                                                                        
                      </select>
                      <p id="show_availability" style="display: none;"></p>
                  </div>
              </div>
        <div class="rsltTxt">
                         <h6><?php echo lang('results'); ?></h6>
                     </div>

                     <?php echo $branches_html; ?>
        </div>
        <div class="modal-footer mod_fot text-center">
            <div class="text-right cls-bt">
                <button type="button" class="btn-success close-icon" data-dismiss="modal"><?php echo lang('Close'); ?></button>
            </div>
        </div>
      </div>
    </div>
</div>
<?php
if ($this->session->userdata('user')) {
    $IsProductPurchased = IsProductPurchased($this->session->userdata['user']->UserID, $product->ProductID);
    if ($IsProductPurchased) {
        $UserCanReviewRate = true;
    } else {
        $UserCanReviewRate = false;
    }
} else {
    $UserCanReviewRate = false;
}
if ($product->IsCorporateProduct == 1 && $product->CorporateMinQuantity > 0) {
    $MinQuantity = $product->CorporateMinQuantity;
    $Price = $product->CorporatePrice;
    $MinQClass = "bordered";
    $PriceClass = "";
    $PriceType = lang('price_type_kg');
    $IsCorporateItem = 1;
} else {
    $MinQuantity = 1;
    $MinQClass = "";
    $PriceClass = "bordered";
    $Price = $product->Price;
    if ($product->PriceType == 'kg') {
        $PriceType = lang('price_type_kg');
    } else {
        $PriceType = lang('price_type_item');
    }
    $IsCorporateItem = 0;
}
?>
<?php
// offer logic here
$IsOnOffer = false;
$DiscountDescription = '';
$offer_product = checkProductIsInAnyOffer($product->ProductID);
$ProductDiscountedPrice = $product->Price;
if(!empty($offer_product)){
    $Price = $product->Price;
    $DiscountDescription = $offer_product['Description'];
    $IsOnOffer = true;
    $DiscountType = $offer_product['DiscountType'];
    $DiscountFactor = $offer_product['Discount'];
    if ($DiscountType == 'percentage') {
        $Discount = ($DiscountFactor / 100) * $Price;
        if ($Discount > $Price) {
            $ProductDiscountedPrice = 0;
        } else {
            $ProductDiscountedPrice = $Price - $Discount;
        }
    } elseif ($DiscountType == 'per item') {
        $Discount = $DiscountFactor;
        if ($Discount > $Price) {
            $ProductDiscountedPrice = 0;
        } else {
            $ProductDiscountedPrice = $Price - $DiscountFactor;
        }
    } else {
        $Discount = 0;
        if ($Discount > $Price) {
            $ProductDiscountedPrice = 0;
        } else {
            $ProductDiscountedPrice = $Price;
        }
    }

}

/*$IsOnOffer = false;
$DiscountDescription = '';
if ($product->IsCorporateProduct == 0 && $offer_id != "") {
    $CurrentDate = date('Y-m-d');
    $offer_id = base64_decode($offer_id);
    $offer = array($offer_id);
    $offer_products = $this->Offer_model->getOfferProducts($offer);
    if ($offer_products) {
        foreach ($offer_products as $offer_product) {
            $OfferID = $offer_product->OfferID;
            $ProductIDsOffer = explode(',', $offer_product->ProductID);
            if (in_array($product->ProductID, $ProductIDsOffer)) {
                $OfferValidFrom = $offer_product->ValidFrom;
                $OfferValidTo = $offer_product->ValidTo;
                if ($CurrentDate >= $OfferValidFrom && $CurrentDate <= $OfferValidTo) {
                    $IsOnOffer = true;
                    $DiscountDescription = $offer_product->Title . ": " . $offer_product->Description;
                    $DiscountType = $offer_product->DiscountType;
                    $DiscountFactor = $offer_product->Discount;
                    if ($DiscountType == 'percentage') {
                        $Discount = ($DiscountFactor / 100) * $Price;
                        if ($Discount > $Price) {
                            $ProductDiscountedPrice = 0;
                        } else {
                            $ProductDiscountedPrice = $Price - $Discount;
                        }
                    } elseif ($DiscountType == 'per item') {
                        $Discount = $DiscountFactor;
                        if ($Discount > $Price) {
                            $ProductDiscountedPrice = 0;
                        } else {
                            $ProductDiscountedPrice = $Price - $DiscountFactor;
                        }
                    } else {
                        $Discount = 0;
                        if ($Discount > $Price) {
                            $ProductDiscountedPrice = 0;
                        } else {
                            $ProductDiscountedPrice = $Price;
                        }
                    }
                }
                break;
            }
        }
    }
}*/
?>
<style>
    #PriceT {
        padding: 0 !important;
    }
    div#slideshow {
        overflow: visible;
    }
    /*.mz-expand .mz-figure > img {
        right: 0;
        margin: 0 auto !important;
        display: inline-block;
        width: auto !important;
    }*/
</style>
    <section class="content titlarea">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2><?php echo $product->Title; ?></h2>
                    <ul>
                        <?php
                        if ($product->IsCorporateProduct == 1) { ?>
                            <li><a href="<?php echo base_url('corporate'); ?>"><?php echo lang('corporate'); ?></a></li>
                            <li><a href="<?php echo base_url('corporate/products'); ?>"><?php echo lang('products'); ?></a></li>
                        <?php } else { ?>
                            <li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
                            <li><a href="<?php echo base_url('product'); ?>"><?php echo lang('products'); ?></a></li>
                            <li>
                                <?php $first_cate = explode(',',$product->CategoryID);

                                    

                                    $first_child_cate = explode(',',$product->SubCategoryID);

                                    

                                 ?>
                                <a href="<?php echo base_url('product/category/'.strtolower(str_replace(' ','-',categoryName($first_cate[0], $language)).'-c'.$first_cate[0])); ?>"><?php echo categoryName($first_cate[0], $language); ?></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('product?q='.strtolower(str_replace(' ','-',categoryName($first_child_cate[0], $language)).'-s'.$first_child_cate[0])); ?>"><?php echo categoryName($first_child_cate[0], $language); ?></a>
                            </li>
                            <!-- <li><?php echo $product->Title; ?></li> -->
                        <?php }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="content single-products">
        <div class="container-fluid responsve-width-containr product-details">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 d-lg-block mr-lg-auto ml-lg-auto">
                    <div class="imggallery text-center">
                        <!-- <div class="app-figure" id="zoom-fig"> -->
                            <?php
                                if(isset($product_images[0]->ImageName)) {
                                    ?>
                                        <!-- <a id="Zoom-1 " class="" title="Chocomood"
                                            href="<?php echo base_url($product_images[0]->ImageName); ?>?h=1400"
                                            data-zoom-image-2x="<?php echo base_url($product_images[0]->ImageName); ?>?h=2800"
                                            data-image-2x="<?php echo base_url($product_images[0]->ImageName); ?>?h=800" > -->
                                            <img id="main" class="drift-demo-trigger w-100 mb-3 rounded" 
                                            src="<?php echo base_url($product_images[0]->ImageName); ?>?h=400" 
                                            data-zoom="<?php echo base_url($product_images[0]->ImageName); ?>?h=400"
                                            alt=""/>
                                        <!-- </a> -->
                                    <?php
                                }
                            ?>
                            <div id="thumbs" class="product-img-slider row p-3">
                                <div class="col-3 p-0">
                                    <?php
                                        $ed = 1;
                                        if($product_images){
                                        foreach ($product_images as $product_image) { ?>
                                        <!-- <a
                                            data-zoom-id="Zoom-1"
                                            href="<?php echo base_url($product_image->ImageName); ?>?h=1400"
                                            data-image="<?php echo base_url($product_image->ImageName); ?>?h=400"
                                            data-zoom-image-2x="<?php echo base_url($product_image->ImageName); ?>?h=2800"
                                            data-image-2x="<?php echo base_url($product_image->ImageName); ?>?h=800"
                                        > -->
                                            <img class="rounded p-1 w-100 h-auto transtion" src="<?php echo base_url($product_image->ImageName); ?>?h=60"/>
                                        <!-- </a> -->
                                            <?php $ed++;
                                        } }
                                    ?>
                                </div>
                            </div>
                        <!-- </div> -->
                    </div>
                </div>
                <!-- Start Product Details -->
                <div class="col-xl-5 col-lg-5 col-md-4 col-sm-6">
                    <div class="product-info product-img-Zoom">
                        <div class="row ar-row">
                            <div class="col-xs-12">
                                <h2 class="mi-product-title"><?php echo $product->Title; ?></h2>
                                <p class="mi-product-desc"><?php echo $product->Description; ?></p>
                            </div>
                            <div class="col-xs-12">
                                <h5 class="mi-txt-price"><?php echo lang('price'); ?> 
                                <?php
                                $Price1 = get_taxt_amount($Price)+$Price;
                                if ($IsOnOffer) { ?>
                                    
                                    <span class="mi-currency" style=""><?php echo lang('SAR'); ?></span>
                                    <span id="PriceP" class="mi-PriceP" style="text-decoration: line-through;"><?php echo number_format($Price1, 2); ?></span>
                                    <!-- <span style="text-decoration: line-through;"
                                          id="PriceT"><?php echo $PriceType; ?></span> -->
                                    <span style="font-weight: bold;" class="offered_product"
                                          title="<?php echo $DiscountDescription; ?>"><?php echo number_format($ProductDiscountedPrice, 2); ?> <?php /*echo lang('SAR'); */?></span>
                                    <!-- <span style="font-weight: bold;" class="offered_product"
                                          id="PriceT"><?php echo $PriceType; ?></span> -->
                                     <span id="PriceT"><?php echo lang('including_tax'); ?></span>
                                    <input type="hidden" value="<?php echo number_format($ProductDiscountedPrice, 2); ?>" id="prod_price"/>
                                    <?php
                                    $ProductPriceForCart = $ProductDiscountedPrice;
                                } else { ?>
                                    <span class="mi-currency"><?php echo lang('SAR'); ?></span>
                                    <span id="PriceP" class="mi-PriceP"><?php echo number_format($Price1, 2); ?></span>
                                    <input type="hidden" value="<?php echo number_format($Price1, 2); ?>" id="prod_price"/>
                                    <!--<span id="PriceT"><?php echo $PriceType; ?> (including tax)</span>-->
                                    <span id="PriceT"><?php echo lang('including_tax'); ?></span>
                                    <?php
                                    $ProductPriceForCart = $Price;
                                }
                                ?>
                                </h5>
                                <?php
                                if ($product->OutOfStock == 1) { ?>
                                    <small style="font-weight: bold;color: red;">(<?php echo lang('Out_Of_Stock'); ?>)</small>
                                <?php }

                                if(!empty($available_cities)){
                                    if($product->OutOfStock != 1){
                                ?>
                                <span class="stock-normal">
                                    <i class="fa fa-check-circle" style="font-size:1.6rem;color: #00ae42;"></i> 
                                    In stock online
                                </span>
                            <?php } ?>
                            </div>
                        
                        </div>
                        <div class="row ar-row">
                            <div class="col-xs-6 col-md-6">
                                <div class="ratings mi-ratings">
                                    <!-- <div id="showRating"></div> -->
                                    <div class="mi-rate">
                                        <input type="radio" id="star5" name="rate" value="5" />
                                        <label for="star5" title="text">5 stars</label>
                                        <input type="radio" id="star4" name="rate" value="4" />
                                        <label for="star4" title="text">4 stars</label>
                                        <input type="radio" id="star3" name="rate" value="3" />
                                        <label for="star3" title="text">3 stars</label>
                                        <input type="radio" id="star2" name="rate" value="2" />
                                        <label for="star2" title="text">2 stars</label>
                                        <input type="radio" id="star1" name="rate" value="1" />
                                        <label for="star1" title="text">1 star</label>
                                      </div>
                                    <p><?php echo productAverageRating($product->ProductID); ?> <?php echo lang('avg_rated_by'); ?> <?php echo $ratings['total_ratings_count']; ?> <?php echo lang('People'); ?></p>
                                </div>
                            </div>
                            <div class="col-xs-6 col-md-6">
                                <?php
                            if ($this->session->userdata('user')) {
                                ?>
                                        <a href="javascript:void(0);" onclick="addToWishlist(<?php echo $product->ProductID.',\''.  $product->PriceType.'\''?> )"><span class="mi-favorite glyphicon glyphicon-heart-empty"></span></a>
                            <?php }else
                            {
                                ?>
                                    <a href="javascript:void(0);" onclick="proceedToCheckout();"><span class="mi-favorite glyphicon glyphicon-heart-empty"></span></a>
                                <?php
                            } ?>
                            </div>
                        </div>
                        <div class="row ar-row">
                            <div class="col-xs-6">
                                <h4 class="no-border"><?php echo lang('specifications'); ?></h4> 
                            </div>
                             <div class="col-xs-6 mi-nutritionBtn">
                                <button class="nutration-facts-btn nutrition-pad-left nutrition-pad-right"><a class="pointer mi-pointer" data-toggle="modal" data-target="#nutritionInfo"><?php echo lang('nutrition_info'); ?></a></button>
                            </div>
                        </div>
                        <h4></h4>
                        <div class="row ar-row">
                            <div class="col-xs-12 col-md-12">
                        <?php if($available){ ?>
                        <?php echo $product->Specifications; ?>
                        <?php } ?>
                            </div>
                        </div>
                        <?php if ($product->IsCorporateProduct == 1) { ?>
                            <h5 class="MinimumQty <?php echo $MinQClass; ?>"><?php echo lang('Minimum_Qty'); ?>
                                <span><?php echo $MinQuantity; ?> <?php echo lang('kg'); ?></span></h5>
                        <?php } ?>
                        <h5 class="<?php echo $PriceClass; ?> mi-bordered"></h5>
                        

                        
                         
                        <div class="row ar-row" >
                            <div class="col-lg-12">
                                <div class="row" style="margin-bottom:10px">
                                    <div class="col-xs-<?= ($product->PriceType == 'kg')?'12':'12';?>">
                                        <h6 class="mi-title"><?php echo lang('quantity'); ?></h6>
                                    </div>
                                </div>
                                    
                                <div class="row">   
                                    <?php if($product->PriceType != 'kg'){ ?>
                                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                        <input id="after" name="Quantity" class="form-control Quantity" type="number"
                                        value="<?php echo $MinQuantity; ?>" min="1"/>
                                        </div>
                                    <?php }else{ ?>
                                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-5 col-xs-5">
                                            <!-- <div class="col-xs-5" > 
                                                <h6><?php echo lang('package'); ?></h6>    
                                            </div> -->
                                        
                                            <div class="form-group edSelectStyle mg-top-5">
                                                <select class="form-control Package mi-package" name="package" required>
                                                <option disabled Selected>Select Package</option> 
                                                    <?php
                                                    $count = 0;
                                                     foreach(@$product_packages as $k => $v) {
                                                    ?>
                                                    <option <?= (@$v['DefaultPackagesID'] == @$v['PackagesID'])? 'Selected' : '' ?> data-piece-weight="<?= $v['PerPiecePrice']?>"  data-gram-price="<?= $v['PerGramPrice']?>" data-weight="<?= $v['quantity']?>" data-min="<?= $v['MinimumPackage']?>" data-max="<?= $v['MaximumPackage']?>" value = "<?= $v['PackagesProductID']?>"><?= $v['Title'] ?></option>
                                                    <?php 
                                                    $count++;
                                                } ?>
                                                  
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xl-5 col-lg-5 col-md-6 col-sm-7 col-xs-7 text-center"  >
                                            <input id="after" min="1" name="Quantity" class="form-control Quantity mi-quantity" type="number"value="<?php echo $MinQuantity; ?>" />
                                            <!-- <div class="form-group edSelectStyle ">
                                                <select class="form-control Quantity">
                                                    <option value = "0.250"><?php echo ($language == 'AR' ? 'ربع ٢٥٠ غرام' : '250 Grams'); ?></option>
                                                    <option value = "0.500" ><?php echo ($language == 'AR' ? ' نصف ٥٠٠ غرام' : '500 Grams'); ?></option>
                                                  
                                                    <option value = "1"><?php echo ($language == 'AR' ? 'كيلو ١٠٠٠ غرام' : '1 KG'); ?></option>
                                                   
                                                  
                                                </select>
                                            </div> -->
                                        </div>
                                        <?php } ?>
                                        
                                        

                                        <input type="hidden" class="min" name="min">
                                        <input type="hidden" class="max" name="max">

                                        <input type="hidden" class="ProductID" name="ProductID"
                                            value="<?php echo $product->ProductID; ?>">
                                        <input type="hidden" class="ProductPrice" name="ProductPrice"
                                            value="<?php echo $ProductPriceForCart; ?>">
                                        <input type="hidden" class="IsCorporateItem" value="<?php echo $IsCorporateItem; ?>">
                                        <input type="hidden" class="ItemType" value="Product">

                                        <input id="IsCorporateProductOriginal" type="hidden"
                                            value="<?php echo $product->IsCorporateProduct; ?>">
                                        <input id="CorporateMinQuantityOriginal" type="hidden"
                                            value="<?php echo $product->CorporateMinQuantity; ?>">
                                        <input id="ProductPriceOriginal" type="hidden"
                                            value="<?php echo number_format($product->Price, 2); ?>">
                                        <input id="CorporateProductPriceOriginal" type="hidden"
                                            value="<?php echo number_format($product->CorporatePrice, 2); ?>">
                                        <input id="PriceTypeOriginal" type="hidden"
                                            value="<?php echo($product->PriceType == 'item' ? lang('price_type_item') : lang('price_type_kg')); ?>">
                                        <input id="PriceType" type="hidden"
                                            value="<?= $product->PriceType ; ?>">
                                        <input id="weight" type="hidden"
                                            value="<?= $product->Weight ; ?>">
                                    
                                    
                                
                              

                            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-xs-12 changeOneMobile <?php echo($lang == 'AR' ? 'text-left' : 'text-right'); ?>">
                            
                            <?php if($available){ ?>
                            <button type="button" style="" class=" mi-check-availability-btn " data-toggle="modal" data-target="#chk-avail"><?php echo lang('check');?> <?php echo lang('availability'); ?></button>
                        <?php }else{ ?>
                            <button type="button" style="" class=" mi-check-availability-btn noStocl"><?php echo lang('Out_Of_Stock'); ?></button>
                        <?php } ?>
                            
                     <?php } ?>

                            </div>
                        </div>
                    <div class="col-md-12 col-xs-12">
                                    <h6>TOTAL: <span id="total">0.00</span></h6>
                                </div>

                            <?php if($available){ ?>
                            <div class="col-md-12 col-xs-12">
                                <button type="button" class="btn btn-primary "
                                        onclick="addToCart();"><?php echo lang('add_to_basket'); ?></button>
                            </div>
                             <?php } ?>
                             <div class="col-md-12 col-xs-12">
                                <button type="button" class="btn btn-primary buy_now" data-url="<?php echo base_url('cart'); ?>"  onclick="addToCart(1);">
                                    <?php echo lang('buy_now'); ?>
                                </button>
                            </div>
                        </div>
                      </div>
                    </div>
                </div>
                <aside class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                <!-- Product Action -->
                <div class="mi-product-action rounded border-right  p-4">
                    <div class="row mi-row order-list mt-4">
                        <!-- payment methods -->
                        <div class="col-lg-12 col-md-12 col-xs-12 d-flex align-items-start">
                            <span class="mi-order-icon mi-disp-inline-block mi-pad-right-0-25 mi-mg-left-1-5 mi-mg-right-0-5">
                                <a class="pay-method" href="#">
                                    <i class="fa fa-dollar mi-order-icon mi-disp-inline " aria-hidden="true"></i>
                                    <!-- <span class="src-only">Settings</span> -->
                                </a>
                                <span class="mi-order-title mi-disp-inline"> Payment Methods</span>
                            </span>
                        </div>
                        <div class="row mi-buy-methods">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-xs-4">
                                <div class="mi-method_option  transtion">
                                    <img src="<?php echo front_assets(); ?>images/c1.png" alt="mada">
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-xs-4">
                                <div class=" transtion mi-method_option">
                                    <img src="<?php echo front_assets(); ?>images/c4.png" alt="mastrer-card">
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-xs-4">
                                <div class="mi-method_option  transtion">
                                    <img src="<?php echo front_assets(); ?>images/COD.png" alt="pay">
                                </div>
                            </div>
                            <!--<div class="col-xl-4 col-lg-4 col-md-4 col-xs-4">-->
                            <!--    <div class="mi-method_option  transtion">-->
                            <!--        <img src="<?php echo front_assets(); ?>images/stcpay.jpg" alt="stcpay">-->
                            <!--    </div>-->
                            <!--</div>-->
                            <!--<div class="col-xl-4 col-lg-3 col-md-4 col-4">-->
                            <!--    <div class="method_option rounded transtion">-->
                            <!--        <img src="https://demo.henka.tech/assets/frontend/images/mada.png" alt="mada">-->
                            <!--    </div>-->
                            <!--</div>-->
                            <!--<div class="col-xl-4 col-lg-3 col-md-4 col-4">-->
                            <!--    <div class="method_option rounded transtion">-->
                            <!--        <img src="https://demo.henka.tech/assets/frontend/images/paypal.jpg" alt="paypal">-->
                            <!--    </div>-->
                            <!--</div>-->
                        </div>
                    </div>
                    <a href="<?= site_url('faq');?>">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <span class="mi-order-icon mi-disp-inline ">
                                <svg class="svg-inline--fa fa-store fa-w-20" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="store" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 616 512" data-fa-i2svg=""><path fill="currentColor" d="M602 118.6L537.1 15C531.3 5.7 521 0 510 0H106C95 0 84.7 5.7 78.9 15L14 118.6c-33.5 53.5-3.8 127.9 58.8 136.4 4.5.6 9.1.9 13.7.9 29.6 0 55.8-13 73.8-33.1 18 20.1 44.3 33.1 73.8 33.1 29.6 0 55.8-13 73.8-33.1 18 20.1 44.3 33.1 73.8 33.1 29.6 0 55.8-13 73.8-33.1 18.1 20.1 44.3 33.1 73.8 33.1 4.7 0 9.2-.3 13.7-.9 62.8-8.4 92.6-82.8 59-136.4zM529.5 288c-10 0-19.9-1.5-29.5-3.8V384H116v-99.8c-9.6 2.2-19.5 3.8-29.5 3.8-6 0-12.1-.4-18-1.2-5.6-.8-11.1-2.1-16.4-3.6V480c0 17.7 14.3 32 32 32h448c17.7 0 32-14.3 32-32V283.2c-5.4 1.6-10.8 2.9-16.4 3.6-6.1.8-12.1 1.2-18.2 1.2z"></path></svg>
                            </span>
                            <div class="mi-disp-inline">
                                <span class="mi-order-title mi-disp-inline-block"><?php echo lang('click_and_collect'); ?></span>
                                <p class="mi-order-content">   
                                    <?php echo lang('order_this_product_now_and_collect_it_from_a_store_of_your_choice'); ?>
                                    <br>
                                    <span class="learn-more"><?php echo lang('learn_more'); ?></span>
                                </p>    
                            </div>
                        </div>                       
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <span class="mi-order-icon mi-disp-inline ">
                                <svg class="svg-inline--fa fa-shipping-fast fa-w-20" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="shipping-fast" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" data-fa-i2svg=""><path fill="currentColor" d="M624 352h-16V243.9c0-12.7-5.1-24.9-14.1-33.9L494 110.1c-9-9-21.2-14.1-33.9-14.1H416V48c0-26.5-21.5-48-48-48H112C85.5 0 64 21.5 64 48v48H8c-4.4 0-8 3.6-8 8v16c0 4.4 3.6 8 8 8h272c4.4 0 8 3.6 8 8v16c0 4.4-3.6 8-8 8H40c-4.4 0-8 3.6-8 8v16c0 4.4 3.6 8 8 8h208c4.4 0 8 3.6 8 8v16c0 4.4-3.6 8-8 8H8c-4.4 0-8 3.6-8 8v16c0 4.4 3.6 8 8 8h208c4.4 0 8 3.6 8 8v16c0 4.4-3.6 8-8 8H64v128c0 53 43 96 96 96s96-43 96-96h128c0 53 43 96 96 96s96-43 96-96h48c8.8 0 16-7.2 16-16v-32c0-8.8-7.2-16-16-16zM160 464c-26.5 0-48-21.5-48-48s21.5-48 48-48 48 21.5 48 48-21.5 48-48 48zm320 0c-26.5 0-48-21.5-48-48s21.5-48 48-48 48 21.5 48 48-21.5 48-48 48zm80-208H416V144h44.1l99.9 99.9V256z"></path></svg>
                            </span>
                            <div class="mi-disp-inline">
                                <span class="mi-order-title mi-disp-inline-block"><?php echo lang('standard_shipment'); ?></span>
                                <p class="mi-order-content">   
                                    <?php echo lang('order_this_product_now_and_collect_it_from_a_store_of_your_choice'); ?>
                                    <br>
                                    <span class="learn-more"><?php echo lang('learn_more'); ?></span>
                                </p>    
                            </div>
                        </div>                       
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <span class="mi-order-icon mi-disp-inline ">
                                <svg class="svg-inline--fa fa-biking fa-w-20" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="biking" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" data-fa-i2svg=""><path fill="currentColor" d="M400 96a48 48 0 1 0-48-48 48 48 0 0 0 48 48zm-4 121a31.9 31.9 0 0 0 20 7h64a32 32 0 0 0 0-64h-52.78L356 103a31.94 31.94 0 0 0-40.81.68l-112 96a32 32 0 0 0 3.08 50.92L288 305.12V416a32 32 0 0 0 64 0V288a32 32 0 0 0-14.25-26.62l-41.36-27.57 58.25-49.92zm116 39a128 128 0 1 0 128 128 128 128 0 0 0-128-128zm0 192a64 64 0 1 1 64-64 64 64 0 0 1-64 64zM128 256a128 128 0 1 0 128 128 128 128 0 0 0-128-128zm0 192a64 64 0 1 1 64-64 64 64 0 0 1-64 64z"></path></svg>
                            </span>
                            <div class="mi-disp-inline">
                                <span class="mi-order-title mi-disp-inline-block"><?php echo lang('fast_delivery'); ?></span>
                                <p class="mi-order-content">   
                                <?php echo lang('delivered_in_business_days'); ?><span class="learn-more"><?php echo lang('learn_more'); ?></span>.
                                </p>    
                            </div>
                        </div>                       
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <span class="mi-order-icon mi-disp-inline ">
                                <svg class="svg-inline--fa fa-truck fa-w-20" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="truck" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" data-fa-i2svg=""><path fill="currentColor" d="M624 352h-16V243.9c0-12.7-5.1-24.9-14.1-33.9L494 110.1c-9-9-21.2-14.1-33.9-14.1H416V48c0-26.5-21.5-48-48-48H48C21.5 0 0 21.5 0 48v320c0 26.5 21.5 48 48 48h16c0 53 43 96 96 96s96-43 96-96h128c0 53 43 96 96 96s96-43 96-96h48c8.8 0 16-7.2 16-16v-32c0-8.8-7.2-16-16-16zM160 464c-26.5 0-48-21.5-48-48s21.5-48 48-48 48 21.5 48 48-21.5 48-48 48zm320 0c-26.5 0-48-21.5-48-48s21.5-48 48-48 48 21.5 48 48-21.5 48-48 48zm80-208H416V144h44.1l99.9 99.9V256z"></path></svg>
                            </span>
                            <div class="mi-disp-inline">
                                <span class="mi-order-title mi-disp-inline-block"><?php echo lang('free_delivery'); ?></span>
                                <p class="mi-order-content">   
                                <?php echo lang('free_delivery_start_from_250_sar'); ?><span class="learn-more"><?php echo lang('learn_more'); ?></span>.
                                </p>    
                            </div>
                        </div>                       
                    </div>
                    </a>
                    <a href="<?= site_url('return-policy');?>">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <span class="mi-order-icon mi-disp-inline ">
                                <svg class="svg-inline--fa fa-exclamation-circle fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="exclamation-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg>
                            </span>
                            <div class="mi-disp-inline">
                                <span class="mi-order-title mi-disp-inline-block"><?php echo lang('fast_delivery'); ?></span>
                                <p class="mi-order-content">   
                                <?php echo lang('up_to_14_days_from_the_date_of_order'); ?><span class="learn-more"><?php echo lang('learn_more'); ?></span>.
                                </p>    
                            </div>
                        </div>                       
                    </div>
                    </a>
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="mi-share-on-social">
                                <a class="facebook transtion" href="<?= @$site_setting->FacebookUrl?>"><svg class="svg-inline--fa fa-facebook fa-w-16" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="facebook" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M504 256C504 119 393 8 256 8S8 119 8 256c0 123.78 90.69 226.38 209.25 245V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.28c-30.8 0-40.41 19.12-40.41 38.73V256h68.78l-11 71.69h-57.78V501C413.31 482.38 504 379.78 504 256z"></path></svg><!-- <i class="fab fa-facebook"></i> --> </a>
                                <a class="twitter transtion" href="<?= @$site_setting->TwitterUrl?>"><svg class="svg-inline--fa fa-twitter fa-w-16" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="twitter" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"></path></svg><!-- <i class="fab fa-twitter"></i> --> </a>
                                <a class="whatsapp transtion" href=" https://wa.me/<?= @$site_setting->Whatsapp?>"><svg style="color: green;" class="svg-inline--fa fa-whatsapp fa-w-14" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="whatsapp" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M380.9 97.1C339 55.1 283.2 32 223.9 32c-122.4 0-222 99.6-222 222 0 39.1 10.2 77.3 29.6 111L0 480l117.7-30.9c32.4 17.7 68.9 27 106.1 27h.1c122.3 0 224.1-99.6 224.1-222 0-59.3-25.2-115-67.1-157zm-157 341.6c-33.2 0-65.7-8.9-94-25.7l-6.7-4-69.8 18.3L72 359.2l-4.4-7c-18.5-29.4-28.2-63.3-28.2-98.2 0-101.7 82.8-184.5 184.6-184.5 49.3 0 95.6 19.2 130.4 54.1 34.8 34.9 56.2 81.2 56.1 130.5 0 101.8-84.9 184.6-186.6 184.6zm101.2-138.2c-5.5-2.8-32.8-16.2-37.9-18-5.1-1.9-8.8-2.8-12.5 2.8-3.7 5.6-14.3 18-17.6 21.8-3.2 3.7-6.5 4.2-12 1.4-32.6-16.3-54-29.1-75.5-66-5.7-9.8 5.7-9.1 16.3-30.3 1.8-3.7.9-6.9-.5-9.7-1.4-2.8-12.5-30.1-17.1-41.2-4.5-10.8-9.1-9.3-12.5-9.5-3.2-.2-6.9-.2-10.6-.2-3.7 0-9.7 1.4-14.8 6.9-5.1 5.6-19.4 19-19.4 46.3 0 27.3 19.9 53.7 22.6 57.4 2.8 3.7 39.1 59.7 94.8 83.8 35.2 15.2 49 16.5 66.6 13.9 10.7-1.6 32.8-13.4 37.4-26.4 4.6-13 4.6-24.1 3.2-26.4-1.3-2.5-5-3.9-10.5-6.6z"></path></svg><!-- <i class="fab fa-whatsapp"></i> --> </a>
                            </div>
                        </div>                       
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php if($whats_inside){ ?>
    <section class="content rateproduct">
        <div class="container-fluid responsve-width-containr">
            <div class="row">
                <div class="col-md-12">
                    <div class="title">
                        <h3><strong><span><?php echo lang('what'); ?></span><?php echo lang('Inside'); ?></strong></h3>
                    </div>
                </div>
            </div>
            <div class="owl-carousel owl-theme brands-slider">
                <?php foreach ($whats_inside as $key => $value) { ?>
                        
                        <div class="item">
                            <a href="javascript:void(0);">
                                <img src="<?php echo base_url($value->InsideImage); ?>">
                                <h4><?php echo ($language == 'EN' ? $value->InsideTitle : $value->InsideTitleAr); ?></h4>
                            </a>
                        </div>
                


                    <?php
                }

                ?>
                
            </div>
        </div>
    </section>

<?php } ?>
    <section class="content rateproduct">
        <div class="container-fluid responsve-width-containr">
            <div class="row">
                <div class="col-md-12">
                    <div class="title">
                        <h3 class="mi-ratngProduct-heading"><strong><span><?php echo lang('rate_the'); ?></span> <?php echo lang('product'); ?></strong></h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <?php if ($UserCanReviewRate && $review) { ?>
                        <h6 class="heading"><?php echo lang('Your_review'); ?></h6>
                        <div class="row" style="margin-bottom:20px;">
                            <div class="col-md-7 col-sm-7 col-xs-7">
                                <h6 class="cusNameEd"><?php echo lang('Customer_Name'); ?></h6><!-- hard coded -->
                                <h6><span><?php echo date('d.m.Y h:i A', strtotime($review->CreatedAt)) ?></span>
                                </h6>
                                <p class="cname"><strong><?php echo $review->Title; ?></strong></p>
                            </div>
                            <div class="col-md-5 col-sm-5 col-xs-5">
                                <div class="ratings">
                                <span>Rate it</span>
                                    <?php
                                    if ($rating) { ?>
                                        <div class="alreadyRated"></div>
                                    <?php } else { ?>
                                        <div class="giveRating"></div>
                                    <?php }
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <p><?php echo $review->Comment; ?></p>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($UserCanReviewRate && !$review) { ?>
                        <form action="<?php echo base_url('product/saveReview'); ?>" method="post" class="ajaxForm"
                              id="productReviewForm">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12"><h6 class="heading"><?php echo lang('Give_Your_Review'); ?></h6></div>
                                <div class="col-md-7 col-sm-7 col-xs-7">
                                    <input type="text" name="Title" placeholder="<?= lang('title')?>" class="form-control required">
                                    <input type="hidden" name="ProductID" value="<?php echo $product->ProductID; ?>">
                                </div>
                                <div class="col-md-5 col-sm-5 col-xs-5">
                                    <div class="ratings">
                                        <span><?php echo lang('Rate_it'); ?></span>
                                        <?php
                                        if ($rating) { ?>
                                            <div class="alreadyRated"></div>
                                        <?php } else { ?>
                                            <div class="giveRating"></div>
                                        <?php }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <textarea placeholder="<?= lang('Review')?>" name="Comment"
                                              class="form-control required"></textarea>
                                    <input type="submit" name="" class="btn btn-primary" value="<?= lang('submit')?>">
                                </div>
                            </div>
                        </form>
                    <?php } ?>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <h6 class="mi-heading"><?php echo lang('Other_Reviews'); ?></h6>
                        </div>
                        <?php
                        if ($all_reviews) {
                            foreach ($all_reviews as $item) {
                                ?>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <h6><?php echo $item->FullName; ?>
                                        <span><?php echo date('d.m.Y h:i A', strtotime($item->CreatedAt)) ?></span></h6>
                                    <p class="cname"><strong><?php echo $item->Title; ?></strong></p>
                                    <p><?php echo $item->Comment; ?></p>
                                </div>
                            <?php }
                        } else { ?>
                        <!-- <div class="col-md-12 col-sm-12 col-xs-12">
                            <?php
                            if ($review) { ?>
                                <h6 class="mi-heading"><?php echo lang('No_other_reviews_yet'); ?></h6>
                            <?php } else { ?>
                                <h6 class="mi-heading"><?php echo lang('No_reviews_yet'); ?></h6>
                            <?php }
                            ?>
                        </div> -->
                        <div class="col-md-12 col-sm-12 col-xs-12">
                              <div class="row rating-form-and-comments">
                
                                <!-- Customer Comments View-->
                                <div class="col-md-12">
                                    <div class="row ">
                                        <div class="col-md-12">
                                            <h3 class="review-title text-left"><?= lang('Review')?></h3>
                                        </div>
                
                                        <div class="col-md-12">
                                            <img class="review-icon" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHwAAAB8CAMAAACcwCSMAAAAXVBMVEX///8AAAABAQH7+/uQkJA5OTnw8PAlJSWysrJQUFCcnJzBwcHs7Oympqajo6Pg4OAaGhpqamp2dnYLCwu5ublvb2/U1NSAgIBlZWUvLy8gICDHx8fa2toqKipHR0fLqfcpAAABwUlEQVRoge2a7XqCMAxGqZ2MIcqAfTl193+ZA2QMC9oUXpyr7/mNnKdJmgg0CAghhBBCCCEekEaxhPUc7nclJMe7c6UWElShXtHuVOiu5HB7LHTXcnTkH0q5WkhSXl+GtVfy8raHx8scijo9YPtRvtThZfTHMT3YvDdy63VZUxtQu6scaneWI/NeV7tcjq0695UDIz9GDrOPkqPsXbmODNbJGTko792CC3st9c2UK2jVdVceFsaU6cuxkR8tL+0ZVG4Pe8cNiPxJtWuT3+uy3oAtedLDN3WRSzpcvl12+awmLEQu2efGiD3+7Fpyg5jy/y2XVHtPfs1q78nhYRfcS88lTyTPDtFM8rAQyF/A8vPz/MLK0QWnNwL2jdyLfU75n8jb9poI0I0cXe1OW82LJnMTK9exvcfE6CbDeX6/cp0KCMHydp4/22n3ObzDSd7B+tlk1nY26UxyBzyRs73qxPLKv6T9J+PVVrvjxyXzxe8QbDJeyNnhKL8fuVLb3cqJXY6rdtHJgVMWsLCPZLI8miKf/F1PelRlwK32U+U793z/EE91B8E+sx0VGT4/8rWa7i6xHRUZBqImhBBCCCHkVvgGGeEuiNLMnJcAAAAASUVORK5CYII=">
                                            <h6 class="review-state text-left">
                                                <strong class="d-block">
                                                    <?= lang('Be_the_first_to_review')?>
                                                </strong><br>
                                                <?= lang('What_do_you_think_about_this_product')?>
                                            </h6>
                                        </div>
                                        <div class="align-items-center col-md-12 d-flex">
                                            <button type="button" onclick="scrollWin()" class="Write-review-btn"> <?= lang('Write_Review')?> </button>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                        <?php }
                        ?>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <?php
                        if ($all_reviews) {?>
                    <div class="ratingbox">
                        <div class="rate">
                            <input type="radio" id="star5" name="rate" value="5" />
                            <label for="star5" title="text">5 stars</label>
                            <input type="radio" id="star4" name="rate" value="4" />
                            <label for="star4" title="text">4 stars</label>
                            <input type="radio" id="star3" name="rate" value="3" />
                            <label for="star3" title="text">3 stars</label>
                            <input type="radio" id="star2" name="rate" value="2" />
                            <label for="star2" title="text">2 stars</label>
                            <input type="radio" id="star1" name="rate" value="1" />
                            <label for="star1" title="text">1 star</label>
                        </div>
                        <?php 
                            $t_rating_count = $ratings['rating_1_count'] + $ratings['rating_2_count'] + $ratings['rating_3_count'] + $ratings['rating_4_count'] + $ratings['rating_5_count'];
                        ?>
                        <div class="row mi-rating-total">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mi-pos1">
                                <!--<span class="glyphicon glyphicon-user"></span><?= (isset($total_reviews->total_reviews)) ? number_format($total_reviews->total_reviews) : 0?> total-->
                                <span><?= (isset($t_rating_count)) ? number_format((number_format($t_rating_count) / 15 ), 2, '.', ''): 0 ?>
                                <?php echo lang('out_of_5_stars'); ?></span>    
                            </div>
                             <!--<br/> -->
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mi-pos2">
                                <span class="glyphicon glyphicon-user"></span><?= (isset($t_rating_count)) ? number_format($t_rating_count) : 0 ?> <?php echo lang('star_1'); ?>        
                            </div>
                        </div>
                        <?php
                        if($t_rating_count == 0)
                        {
                            $t_rating_count = 1;
                        }
                        ?>
                        <div class="row">
                            <div class="col-md-offset-3 col-sm-offset-6">
                              <div class="row">

                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label style="font-size:16px;min-width: 50px;"><?php echo lang('star_1'); ?></label>
                                </div>
                                <div class="col-md-8 col-sm-8 col-xs-8">
                                    <div class="progress">
                                        <div class="progress-bar"  role="progressbar" aria-valuemin="0" aria-valuemax="100" style="border-radius:0px; width:<?=$ratings['rating_1_count']/$t_rating_count *100;?>%">
                                            <span class="sr-only"><?= $ratings['rating_1_count']/$t_rating_count *100; ?>% <?php echo lang('Complete'); ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <span class="mi-count"><?php echo $ratings['rating_1_count']/$t_rating_count *100; ?> %</span>
                                </div>

                              </div>
                            </div>
                        </div>
                    
                        <div class="row">
                            <div class="col-md-offset-3 col-sm-offset-6">
                              <div class="row">

                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label style="font-size:16px;min-width: 50px;"><?php echo lang('star_2'); ?></label>
                                </div>
                                <div class="col-md-8 col-sm-8 col-xs-8">
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="border-radius:0px; width:<?=$ratings['rating_2_count']/$t_rating_count *100;?>%">
                                            <span class="sr-only"><?php echo $ratings['rating_2_count']/$t_rating_count *100; ?>% <?php echo lang('Complete'); ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <span class="mi-count"><?php echo $ratings['rating_2_count']/$t_rating_count *100; ?> %</span>
                                </div>

                              </div>
                            </div>
                        </div>
                    
                        <div class="row">
                            <div class="col-md-offset-3 col-sm-offset-6">
                              <div class="row">

                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label style="font-size:16px;min-width: 50px;"><?php echo lang('star_3'); ?></label>
                                </div>
                                <div class="col-md-8 col-sm-8 col-xs-8">
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="border-radius:0px; width:<?=$ratings['rating_3_count']/$t_rating_count *100;?>%">
                                            <span class="sr-only"><?php echo $ratings['rating_3_count']/$t_rating_count *100; ?>% <?php echo lang('Complete'); ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <span class="mi-count"><?php echo $ratings['rating_3_count']/$t_rating_count *100; ?> %</span>
                                </div>

                              </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-offset-3 col-sm-offset-6">
                              <div class="row">

                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label style="font-size:16px;min-width: 50px;"><?php echo lang('star_4'); ?></label>
                                </div>
                                <div class="col-md-8 col-sm-8 col-xs-8">
                                    <div class="progress">
                                        <div class="progress-bar"  role="progressbar"  aria-valuemin="0" aria-valuemax="100" style="border-radius:0px; width:<?=$ratings['rating_4_count']/$t_rating_count *100;?>%">
                                            <span class="sr-only"><?php echo $ratings['rating_4_count']/$t_rating_count *100; ?>% <?php echo lang('Complete'); ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <span class="mi-count"><?php echo $ratings['rating_4_count']/$t_rating_count *100; ?> %</span>
                                </div>

                              </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-offset-3 col-sm-offset-6">
                              <div class="row">

                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label style="font-size:16px;min-width: 50px;"><?php echo lang('star_5'); ?></label>
                                </div>
                                <div class="col-md-8 col-sm-8 col-xs-8">
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar"  aria-valuemin="0" aria-valuemax="100" style="border-radius:0px; width:<?=$ratings['rating_5_count']/$t_rating_count *100;?>%">
                                            <span class="sr-only"><?php echo $ratings['rating_5_count']/$t_rating_count *100; ?>% <?php echo lang('Complete'); ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <span class="mi-count"><?php echo $ratings['rating_5_count']/$t_rating_count *100; ?> %</span>
                                </div>

                              </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
<?php if ($product->IsCorporateProduct == 1) { ?>
    <script>
        $(document).on('keyup', '.Quantity', function () {
            setTimeout(function () {
                updateFields();
            }, 500);
        });
        $(document).on('click', '.input-group-btn', function () {
            var operator = $(this).find('button').text();
            if (operator == '+' || operator == '-') {
                setTimeout(function () {
                    updateFields();
                }, 100);
            }
        });
        function updateFields() {
            var IsCorporateItem = $('.IsCorporateItem').val();
            var IsCorporateProductOriginal = $('#IsCorporateProductOriginal').val();
            var CorporateMinQuantityOriginal = $('#CorporateMinQuantityOriginal').val();
            var ProductPriceOriginal = $('#ProductPriceOriginal').val();
            var CorporateProductPriceOriginal = $('#CorporateProductPriceOriginal').val();
            var PriceTypeOriginal = $('#PriceTypeOriginal').val();
            var Quantity = $('.Quantity').val();
            if (IsCorporateItem == 1) {
                if (Quantity < CorporateMinQuantityOriginal) {
                    showCustomLoader();
                    $('.ProductPrice').val(ProductPriceOriginal);
                    $('.IsCorporateItem').val(0);
                    $('.MinimumQty').hide();
                    $('#PriceT').html(PriceTypeOriginal);
                    $('#PriceP').html(PriceTypeOriginal);
                    $('#PriceP').html(ProductPriceOriginal);
                    $('#PriceP').parent('h5').addClass('bordered');
                    hideCustomLoader();
                    showMessage('<?php echo lang('This_item_will_not'); ?>', 'danger');
                }
            } else if (IsCorporateItem == 0) {
                if (Quantity >= CorporateMinQuantityOriginal) {
                    showCustomLoader();
                    $('.ProductPrice').val(CorporateProductPriceOriginal);
                    $('.IsCorporateItem').val(1);
                    $('.MinimumQty').show();
                    $('#PriceT').html('<?php echo lang('price_type_kg'); ?>');
                    $('#PriceP').html(CorporateProductPriceOriginal);
                    $('#PriceP').parent('h5').removeClass('bordered');
                    hideCustomLoader();
                    showMessage('<?php echo lang('This_item_will_be'); ?>');
                }
            }
        }
    </script>
<?php } ?>
    <script>
        $(function () {
            $("#showRating").rateYo({
                normalFill: "#fff4d1",
                ratedFill: "#f9c834",
                starWidth: "24px",
                rating: <?php echo productAverageRating($product->ProductID); ?>,
                readOnly: true,
            });
        });

        $( document ).ready(function(){

            $('.check_availability').on('change',function(){
                    var city_id = $(this).find(':selected').val();
                    $('.check_availability_city').hide();

                    $('.city-'+city_id).show();
            });

        });

        
    </script>
<?php
if ($UserCanReviewRate) { ?>
    <script>
        $(function () {
            // $(".alreadyRated").rateYo({
                // rating: <?php // echo($rating ? $rating->Rating : 0); ?>,
                // readOnly: true
            // });
            $(".alreadyRated").rateYo({
                rating: <?php echo($rating ? $rating->Rating : 0); ?>,
                // readOnly: true
                fullStar: true,
                normalFill: "#fff4d1",
                ratedFill: "#f9c834",
                starWidth: "24px",
                onSet: function (rating, rateYoInstance) {
                    // alert("Rating is set to: " + rating);
                    showCustomLoader();
                    $.ajax({
                        type: "POST",
                        url: base_url + 'product/giveRating',
                        data: {
                            'ProductID': <?php echo $product->ProductID; ?>,
                            'UserID': <?php echo $this->session->userdata['user']->UserID; ?>,
                            'Rating': rating
                        },
                        dataType: "json",
                        success: function (result) {
                            console.log(result);
                            hideCustomLoader();
                            showMessage(result.message);
                        }
                    });
                }
            });
        });
        $(function () {
            $(".giveRating").rateYo({
                normalFill: "#fff4d1",
                ratedFill: "#f9c834",
                starWidth: "24px",
                fullStar: true,
                onSet: function (rating, rateYoInstance) {
                    // alert("Rating is set to: " + rating);
                    showCustomLoader();
                    $.ajax({
                        type: "POST",
                        url: base_url + 'product/giveRating',
                        data: {
                            'ProductID': <?php echo $product->ProductID; ?>,
                            'UserID': <?php echo $this->session->userdata['user']->UserID; ?>,
                            'Rating': rating
                        },
                        dataType: "json",
                        success: function (result) {
                            console.log(result);
                            hideCustomLoader();
                            showMessage(result.message);
                        }
                    });
                }
            });
        });
    </script>
<?php } ?>

<style>
    /*Modal Css*/
/*.modWl {
    max-width: 510px;
    max-height: 875px;
}*/

div#chk-avail {
    box-shadow: 0px 8px 16px #00000029;
    border-radius: 5px;
    opacity: 1;
}

.mod-cont {
    margin: 10% ;
}

div.mod_hed {
    border: none;
    padding: 0px !important;
  }
  .mod-bod {
    padding: 0px !important; 
  }
  div.mod_fot {
    border: none;
    padding: 0px !important;
  }
h5.hdTxt {
    font-size: 31px !important;
    color: #50456D;
    width: 87px;
    height: 42px;
    padding: 7% 10% 9% 10%;
}

h5.hdTxt span {
    font-size: 41px;
    color: #CE8D8D;
    position: relative;
    bottom: 20px;
}

.edSelectstyle select.fmod.form-control {
    background-color: #FFFFFF;
    box-shadow: 0px 3px 11px #0000000D;
    border: 1px solid #E1E1E1 !important;
    border-radius: 4px;
    opacity: 1;
    height: 37px;
    color: #B28C67;
    padding: 8px 0 5px 22px;
    font-size: 15px !important;
}

.frmcity p strong{
    font-size: 15px;
    color: #555555;
    letter-spacing: 0;
    opacity: 1;
    width: 90px;
    height:25px;
}

.frmcity {
    padding: 13% 10% 5% 10%;
    text-align: left;
}

.rsltTxt h6 {
    font-size: 18px;
    color: #CE8D8D;
    border-top: 1px solid #E2E2E2;
    border-bottom: 1px solid #E2E2E2;
    opacity: 1; 
    font-weight: bolder;
    text-align: left;
    padding: 2% 10% 2% 10%;
}
.branchTXT {
    border-bottom: 1px solid #E2E2E2;  
    line-height: 0.8;
    padding: 3% 10% 5% 10%;
}
.gTxt {
    color: #21AD00;
    letter-spacing: 0;
}

.redTxt {
    color: #C30000;
    letter-spacing: 0;
}
.orangeTxt {
    color: #FF7600;
    letter-spacing: 0;
}
p.hTxt {
    font-size: 15px !important;
    color: #000 !important;
    text-align: left !important;
}

button.btn-success.close-icon {
    background-color: #50456D;
    border-radius: 6px;
    opacity: 1;
    width: 166px;
    height: 37px;
    color: #fff;
    border: none;
    font-size: 15px;
}
button.ck.btn-secondary {
    background-color: #f4ebd3 !important;
    font-size: 20px !important;
    font-weight: normal !important;
    max-width: 250px;
    height: 42px;
    border-radius: 8px;
    color: #BD9371;
    border: none;
    width: 250px;
}

.cls-bt {
    padding: 5%; 
}
.content.single-products .product-info .input-group {
    width: 160px !important;
}

button.ck.btn-secondary.noStocl {
    color: red;
}
.product-info h2 {
  font-size: 41px;
  font-weight: 400;
  text-transform: none;
  margin: 0 0 10px;
  color: #CE8D8D;
}

/*my css*/
.mi-rate {
    height: 46px;
    padding: 0;
    width: 150px;
}
.mi-rate:not(:checked) > input {
    position:absolute;
    top:-9999px;
}
.mi-rate:not(:checked) > label {
    float:right;
    width:1em;
    overflow:hidden;
    white-space:nowrap;
    cursor:pointer;
    font-size:30px;
    color:#ccc;
}
.mi-rate:not(:checked) > label:before {
    content: '★ ';
}
.mi-rate > input:checked ~ label {
    color: #ffc700;    
}
.mi-rate:not(:checked) > label:hover,
.mi-rate:not(:checked) > label:hover ~ label {
    color: #deb217;  
}
.mi-rate > input:checked + label:hover,
.mi-rate > input:checked + label:hover ~ label,
.mi-rate > input:checked ~ label:hover,
.mi-rate > input:checked ~ label:hover ~ label,
.mi-rate > label:hover ~ input:checked ~ label {
    color: #c59b08;
}
.content.single-products .product-info .ratings > p {
    width: 170px;
}
.content.single-products .product-info .no-border {
    border-bottom: none !important;
    font-weight: bold;
    padding-top: 10px;
}

.content.single-products .product-info .mi-ratings{
    margin-bottom: 0 ;
}
.content.single-products .product-info .mi-txt-price{
    color: #bd9371;
    font-weight: bold;
    font-size: 1.9rem;
    margin: 10px 0 10px;
}
.content.single-products .product-info h5 span.mi-currency{
    color: #9c5114;
    font-weight: 700;
    font-size: 16px;
    padding: 0 5px;
}

.content.single-products .product-info h5 span.mi-PriceP {
    font-size: 25px !important;
    color: #9c5114;
    font-weight: bold;
}
.content.single-products .product-info h5.mi-txt-price span{
    padding: 0;
}
span.stock-normal {
    color: #00ae42;
    padding-bottom: 1rem;
}
button.nutration-facts-btn, button.check-availability-btn {
    background-color: #f4ebd3 !important;
    color: #BD9371;
    border: none;
    padding: .8rem;
}
button.nutration-facts-btn {
    border-radius: 2rem;
    -webkit-border-radius: 2rem;
    -moz-border-radius: 2rem;
    -ms-border-radius: 2rem;
    -o-border-radius: 2rem;
    margin-top: 10px;
    font-size: 14px;
}
.nutrition-pad-right{
    padding-right: 3rem !important;
}
.nutrition-pad-left{
    padding-left: 3rem !important;
}
.mi-nutritionBtn{
    text-align: right;
}
.mi-nutritionBtn a.mi-pointer{
    margin:0;
    color: #bd9371;
    font-weight: bolder;
}
.mi-nutritionBtn a.mi-pointer:hover{
    text-decoration: none;
}
.content.single-products .product-info h5.mi-bordered {    
    margin: 12px 0 12px;
}
.content.single-products .product-info h6.mi-title {
    color: #bd9371;
    font-weight: bold;
    font-size: 1.9rem;
    line-height: 0;
}
.mi-package{
    border-radius: 20px;
    color: #bd9371 !important;
}
.mg-top-5 {
    margin-top: 5px;
}
.content.single-products .product-info .input-group-btn > .btn {
    border-radius: 25px;
    color: #62587c;
    font-weight:bold;
    margin: 5px;
    font-size: 20px;
}
.content.single-products .product-info .input-group-btn > .btn:hover {
    background: #f4ebd3;
    color: #62587c;
    font-weight:bold;
    margin: 5px;
    font-size: 20px;
}
.content.single-products .product-info .mi-quantity {
    color: #66321f!important;
    width: 8rem!important;
    height: 46px;
    border: none!important;
    border-radius: 0;
    -webkit-border-radius: 0;
    -moz-border-radius: 0;
    -ms-border-radius: 0;
    -o-border-radius: 0;
    text-align: center;
    font-size: 2.3rem;
    font-weight: bold;
    padding: 0;
    background-color: transparent;
}
button.mi-check-availability-btn {
    font-weight: bold;
    padding: .5rem .8rem;
    height: 50px;
    border-radius: 5rem;
    font-size: 1.5rem;
    border:none;
    background-color: #f4ebd3 !important;
    color: #BD9371;
    width: 100%
}
.product-info h2.mi-product-title {
    color: #bd9371 !important;
    font-size: 24px !important;
    font-weight: 500 !important;
    line-height: 1.1;
}
span.mi-favorite {
    float: right;
    font-size: 3rem;
    cursor: pointer;
    color: #ea7284;
}
.ar span.mi-favorite {
    float: left;
}
.ar .nutration-facts-btn {
    float: left;
}
.ar .ar-row {
    direction: rtl;
}
.product-img-Zoom {
    direction: ltr;
}
.mi-product-desc {
    margin: 0;
}
span.mi-favorite.glyphicon.glyphicon-heart-empty:hover {
    -ms-transform: scale(1.5);
    -webkit-transform: scale(1.5);
    transform: scale(1.2);
}
.btn.btn-primary:hover, .btn.btn-primary:focus {
    background-color: #50456d !important;
    opacity: .9;
}
.content.single-products .product-info .btn-primary {
    width: 100%;
    margin-top: 10px;
}
.content .title h3:before{
    /*height: 50px;*/
    height: 75px;
}
.content.rateproduct h6.mi-heading{
    font-size: 24px;
    font-weight: 400;
    margin: 0 0 15px;
    color: #000;
}

.rate {
    height: 47px;
    padding: 0;
}
.rate:not(:checked) > input {
    position:absolute;
    top:-9999px;
}
.rate:not(:checked) > label {
    float:right;
    width:1em;
    overflow:hidden;
    white-space:nowrap;
    cursor:pointer;
    font-size:30px;
    color:#ccc;
}
.rate:not(:checked) > label:before {
    content: '★ ';
}
.rate > input:checked ~ label {
    color: #ffc700;    
}
.rate:not(:checked) > label:hover,
.rate:not(:checked) > label:hover ~ label {
    color: #deb217;  
}
.rate > input:checked + label:hover,
.rate > input:checked + label:hover ~ label,
.rate > input:checked ~ label:hover,
.rate > input:checked ~ label:hover ~ label,
.rate > label:hover ~ input:checked ~ label {
    color: #c59b08;
}
.glyphicon {
    position: relative;
    top: 1px;
    display: inline-block;
    font-family: "Glyphicons Halflings";
    font-style: normal;
    font-weight: normal;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    margin-right: 5px;
    color: #bd9371;
}
.mi-rating-total {
    font-size: 16px;
    color: #bd9371;
    /*text-align:right;*/
}
.mi-pos1 {
    margin-left: -20px ;
    text-align:right;
}
.mi-pos2 {
    margin-left: -80px ;
    text-align:right;
}
/*.mi-pos1 {*/
/*    position: relative;*/
/*    right: 20px;*/
/*}*/
/*.mi-pos2 {*/
/*    position: relative;*/
/*    right: 80px;*/
/*}*/
.mi-disp-inline-block {
    display: inline-block!important;
}
.mi-disp-inline {
    display: inline!important;
}
.mi-pad-top-0-25 {
    padding-top: .25rem!important;
}
.mi-pad-right-0-25 {
    padding-right: .25rem!important;
}
.mi-mg-left-1-5 {
    margin-left: 1.5rem!important;
}
.mi-mg-right-0-5 {
    margin-right: .5rem!important;
}
.mi-product-action{
    /*padding: 1.5rem!important;*/
    border-radius: .25rem!important;
    border-right: 1px solid #dee2e6!important;
}
.mi-row {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
    margin-top:1.5rem !important;
}
span.mi-order-icon {
    color: #804b21 !important;
    font-size: 18px !important;
    margin-right: 3px;
    margin-left: 11px;
}
span.mi-order-title {
    color: #bd9371;
    font-weight: 800;
    display: block;
    font-family: "Lato", sans-serif !important;
    font-size: 16px;
}
.mi-buy-methods {
    margin-top: 10px;
    padding-right: 2.5rem;
    padding-left: 4.5rem;
}
div.mi-buy-methods .mi-method_option {
    height: 7rem;
    width: 7.5rem;
    display: flex;
    align-items: center;
    margin: auto;
    margin-bottom: .7rem;
    border-radius: .25rem!important
}
.transtion, .transtion:hover, .transtion:focus {
    transition: all .2s .1s ease-in-out;
    -webkit-transition: all .2s .1s ease-in-out;
    -moz-transition: all .2s .1s ease-in-out;
    -ms-transition: all .2s .1s ease-in-out;
    -o-transition: all .2s .1s ease-in-out;
}
.svg-inline--fa.fa-w-20 {
    width: 1.25em;
}
.svg-inline--fa {
    display: inline-block;
    font-size: inherit;
    height: 1em;
    overflow: visible;
    vertical-align: -.125em;
}
.mi-order-content{
    padding: .5rem 0;
    line-height: 1.3;
    font-size: 16px;
    margin:0 5px 20px 40px;
}
div.mi-share-on-social a.facebook:hover, div.mi-share-on-social a.twitter:hover {
    opacity: .9;
}
div.mi-share-on-social a {
    font-size: 13px;
    background-color: #f4ebd3;
    padding: 11px 11px;
    border-radius: 5rem!important;
    -webkit-border-radius: 5rem!important;
    -moz-border-radius: 5rem!important;
    -ms-border-radius: 5rem!important;
    -o-border-radius: 5rem!important;
    text-decoration: none;
    display: inline-block;
    text-align: center;
    font-size: 2rem;
}


.progress-bar-horizontal {
    width: 80%;
    min-height: 0px; 
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: end;
    -ms-flex-align: end;
    align-items: flex-end;
    margin: 0;
    float: right;
    background-color: #fff4d1;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    -ms-border-radius: 5px;
    -o-border-radius: 5px;
    border-radius: 5px;
    /* position: relative; */
    background-color: #f5f5f5;
    overflow: visible;
    margin: 5px 0;
}
.progress .mi-count {
    position: relative;
    right: 40px;
    font-size: 20px;
    text-align: center;
    bottom: 0;
}

.ma-rating-star {
    height: 50px;
    padding: 0;
    position: relative;
    right: 18%;
    top: 14px;
}
.ma-rating-star:not(:checked) > input {
    position:absolute;
    top:-9999px;
}
.ma-rating-star:not(:checked) > label {
    float:right;
    width:1em;
    overflow:hidden;
    white-space:nowrap;
    cursor:pointer;
    font-size:30px;
    color:#ccc;
}
.ma-rating-star:not(:checked) > label:before {
    content: '★ ';
}
.ma-rating-star > input:checked ~ label {
    color: #ffc700;    
}
.ma-rating-star:not(:checked) > label:hover,
.ma-rating-star:not(:checked) > label:hover ~ label {
    color: #deb217;  
}
.ma-rating-star > input:checked + label:hover,
.ma-rating-star > input:checked + label:hover ~ label,
.ma-rating-star > input:checked ~ label:hover,
.ma-rating-star > input:checked ~ label:hover ~ label,
.ma-rating-star > label:hover ~ input:checked ~ label {
    color: #c59b08;
}
.nutritionInfoModal.modal .modal-content .mi-txt-align {
    text-align: center;
}
.modal .mi-close {
    float: right;
    position: relative;
    right: 70px;
}
.content .title h3.mi-ratngProduct-heading {
    padding:20px 30px;
}

.input-group-btn > .btn {
    position: unset !important;
}
.input-group .form-control{
    z-index: 0 !important;
}

.review-title{
    font-weight: 600;
    margin-bottom: 21px;
}
.review-icon{
    float: left;
    width: 61px;
    height: 63px;
    margin-top: -7px;
    margin-left: -12px;
    display: inline-block;
}
.ar .review-icon {
    margin-right: -12px;
    margin-left: 12;
    float: right;
}
.review-state{
    font-size: 17px;
    line-height: 1.5;
}
.Write-review-btn{
    padding: 1rem;
    width: 200px;
    height: 69px;
    border: none;
    color: white;
    background-color: #50456d;
    border-radius: 8px;
}



/*a.pay-method {position:relative;}
.src-only {
    position: absolute;
    width: 1px;
    height: 1px;
    padding: 0;
    margin: -1px;
    overflow: hidden;
    clip: rect(0,0,0,0);
    border: 0;
}
.fa + .src-only {
    padding: 0.25em;
    margin: 0;
    color: #000;
    background: #eee;
    border: 1px solid #ccc;
    border-radius: 2px;
    font: 11px sans-serif;
    z-index: 2; 
}
a.pay-method:focus .fa + .src-only, 
a.pay-method:hover .fa + .src-only {
  clip: auto;
  width: auto;
  height: auto;
  bottom: 100%;
  left: 100%;
  background-color: red;
}*/
.form-group.edSelectStyle select.form-control {
    width: 130px;
}
.check_availability {
    min-width: 100%;
}
@media screen and (max-width: 1024px){
    .content.single-products .product-info .mi-quantity {
        width: 5rem !important;
    }
}
@media screen and (min-width: 1900px) {
    .ma-rating-star {
        height: 50px;
        padding: 0;
        position: relative;
        right: 15%;
        top: 14px;
    }
}
@media (min-width: 768px) and (max-width: 991px){
    .mi-modal-dialog {
        width: 550px !important;
    }
    .modal-dialog {
        width: 610px;
        margin: 0 auto;
    }
    .ma-rating-star {
        height: 50px;
        padding: 0;
        position: relative;
        right: 18%;
        top: 14px;
    }
}
@media (min-width: 992px) and (max-width: 1024px){
    .ma-rating-star {
        height: 50px;
        padding: 0;
        position: relative;
        right: 30%;
        top: 14px;
    }
}
@media (min-width: 375px) and (max-width: 425px){
    .ma-rating-star {
        height: 50px;
        padding: 0;
        position: relative;
        right: 36%;
        top: 14px;
    }
    .nutrition-pad-right{
        padding-right: 2rem !important;
    }
    .nutrition-pad-left{
        padding-left: 2rem !important;
    }
}
@media (max-width: 320px){
    .ma-rating-star {
        height: 50px;
        padding: 0;
        position: relative;
        right: 45%;
        top: 14px;
    }
    .nutrition-pad-right{
        padding-right: 2rem !important;
    }
    .nutrition-pad-left{
        padding-left: 2rem !important;
    }
}
/*@media (min-width: 1279px ) and (max-width: 1285px){*/
/*    .mi-pos1 {*/
/*        position: relative;*/
/*        right: 20px;*/
/*    }*/
/*}*/
/*@media (min-width: 1364px ) and (max-width: 1368px){*/
/*    .mi-pos1 {*/
/*        position: relative;*/
/*        right: 20px;*/
/*    }*/
/*}*/
/*@media (min-width: 991px ) and (max-width: 1024px){*/
/*    .mi-pos1 {*/
/*        position: relative;*/
/*        right: 20px;*/
/*    }*/
/*}*/
/*@media (min-width: 1400px) and (max-width: 1450px){*/
/*    .mi-pos1 {*/
/*        position: relative;*/
/*        right: 20px;*/
/*    }*/
/*}*/
/*@media (min-width: 1600px) and (max-width: 2560px){*/
/*    .mi-pos1 {*/
/*        position: relative;*/
/*        right: 20px;*/
/*    }*/
/*}*/
/*@media (min-width: 1919px ) and (max-width: 1922px){
    .mi-pos1 {
        position: relative;
        left: 130px;
    }
}
@media (min-width: 1678px ) and (max-width: 1681px){
    .mi-pos1 {
        position: relative;
        left: 130px;
    }
}*/
/*@media (min-width: 320px) and (max-width: 330px){
    .mi-rating-total {
        font-size: 16px;
        color: #bd9371;
        position: relative;
        left: 46%;
    }
}
@media (min-width: 360px) and (max-width: 365px){
    .mi-rating-total {
        font-size: 16px;
        color: #bd9371;
        position: relative;
        left: 50%;
    }
}
@media (min-width: 375px) and (max-width: 385px){
    .mi-rating-total {
        font-size: 16px;
        color: #bd9371;
        position: relative;
        left: 53%;
        width: 160px;
    }
}
@media (min-width: 410px) and (max-width: 416px){
    .mi-rating-total {
        font-size: 16px;
        color: #bd9371;
        position: relative;
        left: 58%;
        width: 150px;
    }
}
@media (min-width: 425px) and (max-width: 430px){
    .mi-rating-total {
        font-size: 16px;
        color: #bd9371;
        position: relative;
        left: 55%;
    }
}
@media (min-width: 768px) and (max-width: 773px){
    .mi-rating-total {
        font-size: 16px;
        color: #bd9371;
        position: relative;
        left: 78%;
    }
}
@media (min-width: 991px ) and (max-width: 1000px){
    .mi-rating-total {
        font-size: 16px;
        color: #bd9371;
        position: relative;
        left: 83.5%;
    }
}
@media (min-width: 1024px ) and (max-width: 1030px){
    .mi-rating-total {
        font-size: 16px;
        color: #bd9371;
        position: relative;
        left: 66%;
    }
}
@media (min-width: 1280px ) and (max-width: 1285px){
    .mi-rating-total {
        font-size: 16px;
        color: #bd9371;
        position: relative;
        left: 74%;
    }
}
@media (min-width: 1366px ) and (max-width: 1370px){
    .mi-rating-total {
        font-size: 16px;
        color: #bd9371;
        position: relative;
        left: 76%;
    }
}
@media (min-width: 1440px ) and (max-width: 1445px){
    .mi-rating-total {
        font-size: 16px;
        color: #bd9371;
        position: relative;
        left: 77%;
    }
}
@media (min-width: 1680px ) and (max-width: 1685px){
    .mi-rating-total {
        font-size: 16px;
        color: #bd9371;
        position: relative;
        left: 81%;
    }
}
@media (min-width: 2560px ) and (max-width: 2565px){
    .mi-rating-total {
        font-size: 16px;
        color: #bd9371;
        position: relative;
        left: 88%;
    }
}*/
</style>
<!-- Modal -->
<div id="nutritionInfo" class="modal fade nutritionInfoModal" role="dialog">
  <div class="modal-dialog mi-modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close mi-close" data-dismiss="modal">&times;</button>
        <h2 class="mi-txt-align"><?php echo lang('Nutrition_Facts'); ?></h2>
      </div>
      <div class="modal-body">
      <table class="table">
<colgroup>
<col style="width: 156pt" width="208">
<col style="width: 56pt" width="75">
<col style="width: 67pt" width="89">
</colgroup>
<tbody>
<tr>
<td colspan="4" width="372"><p><b><?php echo $result->Title; ?></b></p></td>
</tr>
<tr>
<td colspan="4" width="372"><p><b><?php echo lang('Serving_Size'); ?> </b> <b class="black"> <?php echo $result->ServingSize; ?></b><p></td>
</tr>
<!--<tr>
<td colspan="3" width="372"><span>Servings Per Container about 8</span></td>
</tr>
<tr>-->
<td class="text-center" colspan="3"><b class="gold"><?php echo lang('Nutrition'); ?> &nbsp;</b></td>
<td class="text-center" width="89"><b class="gold"> Value</b></td>
</tr>
<?php if(!empty($product_nutritions)){
    foreach ($product_nutritions as $key => $value) { ?>
    <tr>
        <td class="text-center" colspan="3"><b><?php echo $value['Title']; ?></b></td>
        <td  width="89"><span><?php echo $value['Quantity']; ?></span></td>
       
    </tr>

    <?php   
    }
} ?>

<!--<tr>
<td colspan="3"  width="372"><span>Vitamin D 4%• Potassium 2% • Calcium 2% • Iron 6%&nbsp;</span></td>
</tr>-->
<tr height="33" style="height: 24.75pt">
<td colspan="4" width="372">
<p class="goldBlack mi-goldBlack"><b>Ingredients:</b> <?php echo $result->Ingredients; ?></p>
</td>
</tr>
</tbody>
</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('Close'); ?></button>
      </div>
    </div>

  </div>
</div>
<?php
?>
<script>
var pro_type = "<?= @$product->PriceType ?>";
var get_tax = '<?= json_encode(getTaxShipmentCharges('Tax')); ?>';
get_tax = JSON.parse(get_tax);
function apply_tax(total){
    var total_tax = 0;
    var taxes = window.get_tax;
    for(var i = 0; i< taxes.length; i++) {
        var tax_title = taxes[i].Title;
        var tax_factor = taxes[i].Type == 'Fixed' ? '' : taxes[i].Amount + '%';
        var tax_amount = 0;
        if (taxes[i].Type == 'Fixed') {
            tax_amount = taxes[i].Amount;
        } else if (taxes[i].Type == 'Percentage') {
           // $tax_amount = ($tax->Amount / 100) * ($total + $shipping_amount);
            tax_amount = (taxes[i].Amount / 100) * (total);
        }
        total_tax += tax_amount;
    }
    return total_tax;
}
var price_with_tax = "<?= $Price1; ?>";
var IsOnOffer = "<?= $IsOnOffer; ?>";
var DiscountType = "<?= @$DiscountType; ?>";
var DiscountFactor = "<?= @$DiscountFactor; ?>";
$( document ).ready(function() {
    cal_total_price();
    setTimeout(function(){
        if(pro_type == "kg" && $('.Package').find(":selected").text() != "Select Package"){
            $(".Package").trigger("change");
        }
     }, 200);

    $(".Package").change(function () {
    
        var min = $( ".Package option:selected" ).data("min");
        var max = $( ".Package option:selected" ).data("max");
        var weight = $( ".Package option:selected" ).data("weight");
        var piece_weight = $( ".Package option:selected" ).data("piece-weight");
        var gram_price = $( ".Package option:selected" ).data("gram-price");
        var max_val = max;

        min = min/weight;
        max = max/weight;
        if(min < 1){
            if(weight < max_val){
                min = 1;    
            }else{
                min=0;
            }
        }
      
        var unit_price = 0;
        unit_price = (gram_price/piece_weight)*weight;
        console.log("tax "+apply_tax(unit_price)+" price"+unit_price);
        unit_price_without_tax = unit_price; 
        unit_price = apply_tax(unit_price) + unit_price;
        unit_price = unit_price.toFixed(2);
        $(".Quantity").val(min);
        $(".Quantity").prop('min',min);
        $(".Quantity").prop('max',max);
        $("#PriceP").html(unit_price);
        var dis_amount = 0;
        if(window.IsOnOffer){
            if(window.DiscountType == 'percentage'){
                dis_amount = ((unit_price/100) * parseFloat(window.DiscountFactor));
                unit_price = unit_price - dis_amount ;
            }else{
                dis_amount = parseFloat(window.DiscountFactor);
                unit_price = unit_price -  dis_amount;
            }
            $(".offered_product").html(unit_price.toFixed(2));
        }
        $(".min").val(min);
        $(".max").val(max);
        $("#prod_price").val(unit_price);
        $(".ProductPrice").val(unit_price_without_tax - dis_amount);
        $("#total").text((unit_price * min).toFixed(2));
    });
    
        $(document).on('click', '.input-group-btn', function () {
    var operator = $(this).find('button').text();
    if (operator == '+' || operator == '-') {
        setTimeout(function () {
           cal_total_price();
        }, 100);
    }
});
$(document).on('keyup', '.Quantity', function () {
        setTimeout(function () {
           cal_total_price();
        }, 100);
});
function cal_total_price(){
    console.log(" quantity " +$(".Quantity").val());
    console.log(" price " +$("#prod_price").val());
    $("#total").text(($(".Quantity").val() * $("#prod_price").val()).toFixed(2));
}
});
</script>

<script>
    
    $('#thumbs').delegate('img','click', function(){
    $('#main').attr('src',$(this).attr('src').replace('thumb','large'));
    $('#main').attr('data-zoom',$(this).attr('src'));
    });

    var demoTrigger = document.querySelector('.drift-demo-trigger');
    var paneContainer = document.querySelector('.product-img-Zoom');

    new Drift(demoTrigger, {
        paneContainer: paneContainer,
        inlinePane: false,
    });

    function scrollWin() {
      window.scrollBy(0, -200);
    }
</script>