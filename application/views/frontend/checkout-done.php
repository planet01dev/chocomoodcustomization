<section class="content checkout address done">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="alertbox">
                    <h5>
                        <i class="fa fa-check-circle ok" aria-hidden="true"></i>
                        <strong><?= lang('Your_Order_Has_been_Placed_Successfully')?>! </strong>
                        <span><?php echo lang('Order_No'); ?>. <?php echo $order->OrderNumber; ?> ,<?php echo lang('Transaction_ID'); ?> . <?php echo $order->TransactionID; ?></span>
                        <!--<a href="javascript:void(0);" onclick="window.print();" style="text-decoration: none;">-->
                        <a href="<?php echo base_url(); ?>page/invoice/<?php echo base64_encode($order->OrderID); ?>" target="_blank" style="text-decoration: none;">
                        <b>
                            <i class="fa fa-print" aria-hidden="true"></i>
                            <?= lang('Print') ?>
                        </b>
                        </a>
                    </h5>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="wbox side">
                    <?php
                    $order_items = getOrderItems($order->OrderID);
                    foreach ($order_items as $item) {
                        
                     ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <?php
                                if ($item->ItemType == 'Product') { ?>
                                    <img src="<?php echo base_url() . '/' . get_images($item->ProductID, 'product', false); ?>">
                                    <h5><a href="<?php echo base_url() . 'product/detail/' . productTitle($item->ProductID); ?>"
                                           target="_blank"><?php echo $item->Title; ?></a>
                                        <?php if($item->PriceType == 'kg'){ 
                                            $PerGramPrice = 0;
                                            $Perpieceprice = 0;
                                            $PackageQuantity = 0;
                                            $packageTotal = 0;
                                            $product_packages = get_product_packages($item->ProductID,$language);
                                            foreach (@$product_packages as $k => $v) {
                                                if ($item->package_id == $v['PackagesProductID']) {
                                                    $PerGramPrice = $v['PerGramPrice'];
                                                        $Perpieceprice = $v['PerPiecePrice'];
                                                        $PackageQuantity = $v['quantity'];
                                                        $packageTotal = ( $PerGramPrice / $Perpieceprice ) * $PackageQuantity;
                                                    ?>
                                                    <span><?= $v['Title'] ?> x  <?= $item->Quantity?></span>
                                             <!-- <span><?php echo $item->Quantity * 1000; ?> <?php echo($language == 'AR' ? 'غرام' : 'Grams'); ?></span> -->
                                         <?php
                                                }
                                            }
                                        }else{ ?>
                                             <span><?php echo $item->Quantity; ?>pcs</span>
                                        <?php  } ?>
                                    </h5>
                                <?php } elseif ($item->ItemType == 'Customized Shape') { ?>
                                    <a href="<?php echo base_url($item->CustomizedShapeImage); ?>" target="_blank">
                                        <img src="<?php echo base_url($item->CustomizedShapeImage); ?>">
                                        <h5>Customized Shape
                                            <?php if($item->PriceType == 'kg'){ 
                                               $product_packages = get_product_packages($item->ProductID,$language);
                                               foreach (@$product_packages as $k => $v) {
                                                   if ($item->package_id == $v['PackagesProductID']) {
                                               ?>
                                               <span><?= $v['Title'] ?> x  <?= $item->Quantity?></span>
                                             <!-- <span><?php echo $item->Quantity * 1000; ?> <?php echo ($language == 'AR' ? 'غرام' : 'Grams'); ?></span> -->
                                         <?php }
                                         }
                                        }else{ ?>
                                             <span><?php echo $item->Quantity; ?>pcs</span>
                                        <?php  } ?>
                                        </h5>
                                    </a>
                                <?php } else { ?>
                                <a href="javascript:void(0);" class="chocobox_detail" title="Click to view whats inside" data-box_id="<?php echo $item->CustomizedBoxID; ?>" data-pids="<?php echo $item->CustomizedOrderProductIDs; ?>" data-box_type="<?php echo $item->ItemType; ?>">
                                    <img src="<?php echo front_assets("images/".$item->ItemType.".png"); ?>">
                                    <h5>
                                        <?php echo $item->ItemType; ?>
                                      <?php if($item->PriceType == 'kg'){
                                          $product_packages = get_product_packages($item->ProductID,$language);
                                          foreach (@$product_packages as $k => $v) {
                                              if ($item->package_id == $v['PackagesProductID']) {
                                          ?>
                                          <span><?= $v['Title'] ?> x  <?= $item->Quantity?></span>
                                             <!-- <span><?php echo $item->Quantity * 1000; ?> <?php echo ($language == 'AR' ? 'غرام' : 'Grams'); ?></span> -->
                                         <?php }}}else{ ?>
                                             <span><?php echo $item->Quantity; ?>pcs</span>
                                        <?php  } ?>
                                    </h5>
                                </a>
                                <?php }
                                ?>
                                <div class="price">
                                <?php if($item->PriceType == 'kg'){ ?>
                                    <?php echo number_format($packageTotal, 2); ?>
                                    <?php }else{ ?>
                                    <?php echo number_format($item->Amount, 2); ?>
                                    <?php } ?>
                                    <?php echo lang('sar'); ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="row last">
                        <div class="col-md-12">
                            <?php
                            if ($order->CouponCodeUsed != '') { ?>
                                <p><span>Promo Applied:</span> <strong><?php echo $order->CouponCodeUsed; ?></strong></p>
                                <p><span><?php echo lang('promo_discount'); ?> %:</span>
                                    <strong><?php echo $order->CouponCodeDiscountPercentage; ?></strong></p>
                                <p><span><?php echo lang('promo_discount_availed'); ?>:</span> <strong><?php echo $order->DiscountAvailed; ?>
                                <?php echo lang('sar'); ?></strong></p>
                            <?php }
                            ?>
                            <ol>
                                <?php if($order->CollectFromStore != 1 && $order->ShipmentMethodID > 0){ 
                                    $val = 0;
                                    if($order->TotalAmount >= getShipmentMaxAmount() && getShipmentMaxAmount() != 0){
                                        $val = freeShippingTitle();
                                    }else{
                                        $val = number_format($order->TotalShippingCharges, 2).' '.lang('sar');
                                    }
                                ?>
                                <li>
                                    <?php
                                    $shipment_method = getSelectedShippingMethodDetail($order->ShipmentMethodID, $language);
                                    ?>
                                    <span><i class="fa fa-truck"
                                             aria-hidden="true"></i> <?php echo $shipment_method->Title; ?></span>
                                    <strong id="ShippingAmount"><?php echo $val; ?>
                                    </strong>
                                </li>
                            <?php } ?>
                                <li>
                                    <span><i class="fa fa-file-text-o" aria-hidden="true"></i> <?= lang('Total_Tax_Paid')?></span>
                                    <strong id="TaxAmount"><?php echo number_format($order->TotalTaxAmount, 2); ?>
                                    <?php echo lang('sar'); ?></strong>
                                </li>
                            </ol>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-secondary">
                                <span></span><strong><?php echo number_format($order->TotalAmount, 2); ?>
                                <?php echo lang('sar'); ?></strong></button>
                        </div>
                        <div class="col-md-12">
                            <div class="abox">
                                <?php
                                if (isset($order->CollectFromStore) && $order->CollectFromStore == 1) { ?>
                                    <h5><?php echo lang('Collect_From_Store'); ?></h5>
                                <?php } else { ?>
                                    <h5><?php echo lang('Delivery_Address'); ?></h5>
                                    <h6><?php echo $order->RecipientName; ?></h6>
                                    <p><?php echo $order->MobileNo; ?><span class="d-block"><a href="mailto:<?php echo $order->Email; ?>"><?php echo $order->Email; ?></a></span><span class="d-block"><?php echo $order->AddressDistrict; ?>, <?php echo $order->AddressCity; ?></span></p>
                                    <p>
                                        <!--<span>City: <?php echo $order->AddressCity; ?></span><br>
                                        <span>District: <?php echo $order->AddressDistrict; ?></span><br>-->
                                       
                                        <span><?php echo lang('street'); ?>: <?php echo $order->Street; ?></span><br>
                                        
                                    </p>
                                    <p>
                                        <a href="https://www.google.com/maps/search/?api=1&amp;query=<?php echo $order->Latitude; ?>,<?php echo $order->Longitude; ?>"
                                           target="_blank">
                                           <?php echo lang('google_pin_link'); ?>
                                        </a>
                                    </p>
                                    <br>
                                    <!--h5>Payment Collection Address</h5>-->
                                    <?php
                                    if ($order->AddressID == $order->AddressIDForPaymentCollection) { ?>
                                        <p>Same as shipping address</p>
                                    <?php }  ?>
                                        
                                <?php } ?>
                            </div>
                            <!--<label class="customcheck default">Register Me <a href="javascript:void(0)" data-toggle="modal" data-target="#TermsConditions">(I Agree to License Agreement)</a>
                                <input type="checkbox">
                                <span class="checkmark"></span>
                            </label>-->
                            <a href="<?php echo base_url(); ?>">
                                <button class="full btn btn-secondary"><?= lang('Continue')?></button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    // Code to disable browser back button
    $(document).ready(function () {
        window.history.pushState(null, "", window.location.href);
        window.onpopstate = function () {
            window.history.pushState(null, "", window.location.href);
        };
    });

    eraseCookie('CollectFromStore');
    function eraseCookie(name) {
        document.cookie = name + '=; Max-Age=-99999999;';
    }
</script>