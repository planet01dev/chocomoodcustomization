<?php
$option = '';
if(!empty($cities)){
    foreach($cities as $city){
        $option .= '<option value="'.$city->CityID.'" '.((isset($result[0]->CityID) && $result[0]->CityID == $city->CityID) ? 'selected' : '').'>'.$city->Title.' </option>';
    } }

$option2 = '<option value="0"> Select / Unselect All </option>';
if(!empty($stores)){
    foreach($stores as $store){
        $option2 .= '<option value="'.$store->StoreID.'" '.((isset($result[0]->StoreID) && $result[0]->StoreID == $store->StoreID) ? 'selected' : '').'>'.$store->Title.' </option>';
    } }

$option3 = ' <option value="0"> Select / Unselect All </option>';
if (!empty($districts)) {
    foreach ($districts as $district) {
        $option3 .= '<option value="' . $district->DistrictID . '">' . $district->Title . ' </option>';
    }
}
    


?>
<style type="text/css">
    .user_field, .user_field_delivery{
        display: none;
    }
    input.mob-numb {opacity: 0; outline: 0; border-bottom: 1px solid #999;}
    input.mob-numb:focus {opacity: 1; outline: 0; border-bottom: : 1px solid #999;}
    span.mob-numb-span {border-bottom: 1px solid #999; display: inline-block;width: 100%;}
</style>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Add Backend User</h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">



                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="RoleID"><?php /*echo lang('choose_user_role'); */?> *</label>
                                        <select id="RoleID" class="selectpicker" data-style="select-with-transition" required name="RoleID">

                                            <?php if(!empty($roles)){
                                                foreach($roles as $role){ ?>
                                                    <option value="<?php echo $role->RoleID; ?>"><?php echo $role->Title; ?> </option>
                                                <?php } } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                

                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title"><?php echo lang('name'); ?></label>
                                        <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title">
                                    </div>
                                </div>







                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Email"><?php echo lang('email'); ?></label>
                                        <input type="text" name="Email" parsley-trigger="change" required  class="form-control" id="Email">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Mobile">Mobile</label>
                                        <input type="hidden" id="MobileCode" name="MobileCode" value="+966">
                                        <!-- <input type="tel" placeholder="5xxxxxxxx" name="Mobile" class="form-control number-only phone required" id="Mobile"> -->
                                        <span class="mob-numb-span"><input type="text" placeholder="+9665xxxxxx"  name="Mobile" parsley-trigger="change"  class="form-control number-only mob-numb phone required" id="Mobile"></span>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Password"><?php echo lang('password'); ?> <?php echo lang('min_length'); ?></label>
                                        <input type="password" name="Password" parsley-trigger="change" required  class="form-control" id="Password">
                                    </div>
                                </div>



                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="ConfirmPassword"><?php echo lang('confirm_password'); ?></label>
                                        <input type="password" name="ConfirmPassword" parsley-trigger="change" required  class="form-control" id="ConfirmPassword">
                                    </div>
                                </div>
                                 <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" checked/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                </div>

                                <div class="row user_field">
                                     <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="GetStore"><?php echo lang('city'); ?> *</label>
                                        <select id="GetStore" class="selectpicker" data-style="select-with-transition" required name="CityID">

                                            <?php echo $option; ?>
                                        </select>
                                    </div>
                                </div>
                                    <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="StoreID"><?php echo lang('choose_store'); ?> *</label>
                                        <select id="StoreID" class="selectpicker" data-style="select-with-transition" required name="StoreID[]" data-size="8" multiple>

                                            <?php echo $option2; ?>
                                        </select>
                                    </div>
                                </div>
                                </div>
                                 <div class="row user_field_delivery">
                                   <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label for="DistrictID"><?php echo lang('district'); ?></label>
                                        <select id="DistrictID" class="form-control selectpicker" required="" data-size="8" name="DistrictID[]" multiple>
                                            <?php echo $option3; ?>
                                        </select>
                                    </div>
                                </div>
                                 </div>
                                 <div class="col-md-4 preferred_lang_field">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Language"><?php echo lang('language'); ?> *</label>
                                        <select class="selectpicker" data-style="select-with-transition" required name="PreferredLang">
                                            <option value="0" <?= (@$user->PreferredLang == 0)? 'selected' : ''; ?> >English</option>
                                            <option value="1" <?= (@$user->PreferredLang == 1)? 'selected' : ''; ?>>Arabic</option>
                                        </select>
                                    </div>
                                </div>







                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');?>
                                </button>
                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>

<script type="text/javascript">
    
   $(document).ready(function () {
    $('.preferred_lang_field').hide();

    $("#Mobile").keyup(function(){
        if($("#Mobile").val())
        {
            $("#Mobile").css('opacity',1);
        }else
        {
            $("#Mobile").css('opacity',0);
        }
    });
        $("#RoleID").on('change',function(){
            if($(this).val() != 1){

                $('.user_field').show();
                $('.preferred_lang_field').show();
            }else{
               $('.user_field').hide(); 
                $('.preferred_lang_field').hide();

            }

            if($(this).val() == 3){
                $('.user_field_delivery').show();
            }else{
                $('.user_field_delivery').hide();
            }
        });


         $('#GetStore').on('change',function(){
          
            var CityID = $(this).val();
            var RoleID = $('#RoleID').val();
            
            $('#StoreID').html('');

            if(CityID == '')
            {
                $('#StoreID').html('<option value=""><?php echo lang("choose_store");?></option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
            
            
                $.ajax({
                    type: "POST",
                    url: base_url + 'cms/store/getStore',
                    data: {
                        'CityID': CityID,
                        'District' : true
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#StoreID').html(result.html).selectpicker('refresh');
                        if(RoleID == 3){
                            var option = ' <option value="0"> Select / Unselect All </option>';
                            option += result.html_district;
                             $('#DistrictID').html(option).selectpicker('refresh');
                        }

                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }
           
       });
    $('#DistrictID').selectpicker().change(function(){toggleSelectAll($(this));}).trigger('change');
    $('#StoreID').selectpicker().change(function(){toggleSelectAll($(this));}).trigger('change');

   });

</script>