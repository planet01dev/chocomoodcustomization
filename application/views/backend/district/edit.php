<?php
$option = '';
if(!empty($cities)){ 
    foreach($cities as $city){ 
        $option .= '<option value="'.$city->CityID.'" '.((isset($result[0]->CityID) && $result[0]->CityID == $city->CityID) ? 'selected' : '').'>'.$city->Title.' </option>';
    } }

$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if(!empty($languages)){
    $lang_c =0;
    foreach($languages as $key => $language){
        $common_fields = '';
        $common_fields2 = '';
        if($key == 0){
           
        $common_fields = '<div class="row"><div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" '.((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '').'/> '.lang('is_active').'
                                            </label>
                                        </div>
                                    </div>
                                </div></div>';
        $common_fields2 = '<div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="CityID">Choose City</label>
                                                                <select id="CityID" class="selectpicker" data-style="select-with-transition" name="CityID">
                                                                    '.$option.'
                                                                </select>
                                                            </div>
                                                        </div>';  
        }

        $lang_tabs .= '<li class="'.($key == 0 ? 'active' : '').'">
                                        <a href="#'.$language->SystemLanguageTitle.'" id="lang_'.$lang_c.'" data-toggle="tab">
                                            '.$language->SystemLanguageTitle.'
                                        </a>
                                  </li>';

        $lang_data .= '<div class="tab-pane '.($key == 0 ? 'active' : '').'" id="'.$language->SystemLanguageTitle.'">
                      <form action="'.base_url().'cms/'.$ControllerName.'/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="'.base64_encode($language->SystemLanguageID).'">
                                                    <input type="hidden" name="'.$TableKey.'" value="'.base64_encode($result[0]->$TableKey).'">
                                                    <input type="hidden" name="IsDefault" value="'.$language->IsDefault.'">

                                                    
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title'.$key.'">'.lang('title').'</label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title'.$key.'" value="'.((isset($result[$key]->Title)) ? $result[$key]->Title : '').'">
                                                                <span class="text-danger ma-fontsize">Note: Select district according to the city</span>
                                                               
                                                            </div>
                                                        </div>
                                                        
                                                        '.$common_fields2.'
                                                        
                                                    </div>
                                                    
                                                    '.$common_fields.'
                                                    

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            '.lang('submit').'
                                                        </button>
                                                        <a href="'.base_url().'cms/'.$ControllerName.'">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         '.lang('back').'
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';
        
        
        
        
        $lang_c++;
    }
}


?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Languages</h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-2">
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                            </div>
                            <div class="col-md-10">
                                <div class="tab-content">
                                    <?php echo $lang_data; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="map" style="display: none;"></div>
<script>
  var input_id = 'Title0';
  function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: -33.8688, lng: 151.2195},
      zoom: 13
    });
    var input = document.getElementById(window.input_id);
    var options = {
      componentRestrictions: {country: "sa"}
     };

    var autocomplete = new google.maps.places.Autocomplete(input, options);
    autocomplete.bindTo('bounds', map);
    autocomplete.setFields(
        ['address_components', 'geometry', 'icon', 'name']);

    autocomplete.addListener('place_changed', function() {

      var place = autocomplete.getPlace();
      if (!place.geometry) {
        window.alert("No details available for input: '" + place.name + "'");
        return;
      }

      // If the place has a geometry, then present it on a map.
      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);  // Why 17? Because it looks good.
      }

      var address = '';
      if (place.address_components) {
        address = [
          (place.address_components[0] && place.address_components[0].short_name || ''),
          (place.address_components[1] && place.address_components[1].short_name || ''),
          (place.address_components[2] && place.address_components[2].short_name || '')
        ].join(' ');
      }
    });
  }

    var lang = "";
    function loadScript(lang) {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = 'https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyCJaZ8KGubGRNlTmqjTlDaizEEMojWTsA4&libraries=places&callback=initMap';
        if (lang) {
            script.src += '&language=' + lang;
        }
        
        script.id = "google-maps-script";
        document.body.appendChild(script);
    }
    
    window.onload = loadScript;
        
    function ChangeGoogleMapsLanguage(lang) {

        //var lang = window.lang,
            var oldScript = document.getElementById("google-maps-script");
            oldScript.parentNode.removeChild(oldScript);

        delete google.maps;
        
        loadScript(lang);
    }
    $("#lang_0").click(function(event) {
        lang = "eng";
        input_id = 'Title0';
        ChangeGoogleMapsLanguage(lang);
    });
    $("#lang_1").click(function(event) {
       lang = "ar";
       input_id = 'Title1';
       ChangeGoogleMapsLanguage(lang);
       console.log('loaded');
          
    });
</script>
