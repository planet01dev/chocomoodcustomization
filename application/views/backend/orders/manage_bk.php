<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Orders</h4>
                        <div class="toolbar">
                            <ul class="nav nav-pills">
                                <li class="active nav-pills-warning">
                                    <a href="#pending" data-toggle="tab" aria-expanded="false">Pending</a>
                                </li>
                                <li>
                                    <a href="#packed" data-toggle="tab" aria-expanded="false">Packed</a>
                                </li>
                                <li class="">
                                    <a href="#dispatched" data-toggle="tab" aria-expanded="false">Dispatched</a>
                                </li>
                                <li class="">
                                    <a href="#delivered" data-toggle="tab" aria-expanded="false">Delivered</a>
                                </li>
                                <li class="">
                                    <a href="#cancelled" data-toggle="tab" aria-expanded="false">Cancelled</a>
                                </li>
                                <?php if ($this->session->userdata['admin']['RoleID'] == 1) { ?>
                                    <li class="">
                                        <a href="#unopened" data-toggle="tab" aria-expanded="true">Un-Opened</a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="material-datatables tab-pane active" id="pending">
                                <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Full Name</th>
                                        <th>Mobile</th>
                                        <th>Email</th>
                                        <th>Order #</th>
                                        <th>Order Amount</th>
                                        <th>User City</th>
                                        <th>Assigned To</th>
                                        <th>Received At</th>
                                        <?php if (checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                            <th><?php echo lang('actions'); ?></th>
                                        <?php } ?>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($pending_orders) {
                                        foreach ($pending_orders as $order) { ?>
                                            <tr id="<?php echo $order->OrderID; ?>"
                                                style="<?php echo $order->IsRead == 0 ? "font-weight: bold;" : "" ?>">
                                                <td><?php echo $order->FullName; ?></td>
                                                <td><?php echo $order->Mobile; ?></td>
                                                <td><?php echo $order->Email; ?></td>
                                                <td><?php echo $order->OrderNumber; ?></td>
                                                <td><?php echo number_format($order->TotalAmount, 2); ?> SAR</td>
                                                <td><?php echo $order->UserCity; ?></td>
                                                <td><?php echo($order->AssignedDriverName != '' ? ucfirst($order->AssignedDriverName) : 'N/A'); ?></td>
                                                <td><?php echo date('d-m-Y h:i A', strtotime($order->CreatedAt)); ?></td>
                                                <?php if (checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                    <!--<td>
                                                    <?php /*if(checkUserRightAccess(65,$this->session->userdata['admin']['UserID'],'CanEdit')){*/ ?>
                                                        <a href="<?php /*echo base_url('cms/orders/edit/'.$order->OrderID);*/ ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">edit</i><div class="ripple-container"></div></a>
                                                    <?php /*} */ ?>

                                                    <?php /*if(checkUserRightAccess(65,$this->session->userdata['admin']['UserID'],'CanDelete')){*/ ?>
                                                        <a href="javascript:void(0);" onclick="deleteRecord('<?php /*echo $order->OrderID;*/ ?>','cms/<?php /*echo $ControllerName; */ ?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>
                                                    <?php /*} */ ?>
                                                </td>-->
                                                    <td>
                                                        <?php if (checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                            <?php
                                                            if ($this->session->userdata['admin']['RoleID'] == 1 || $this->session->userdata['admin']['RoleID'] == 2 || $this->session->userdata['admin']['RoleID'] == 4) { ?>
                                                                <a href="javascript:void(0);" data-order_id="<?php echo $order->OrderID; ?>" data-order_status="2" data-status_title="Packed"
                                                                   class="btn btn-simple btn-warning btn-icon edit changeOrderStatus">
                                                                    <i class="material-icons" title="Click to update status to packed">thumb_up</i>
                                                                    <div class="ripple-container"></div>
                                                                </a>
                                                            <?php } ?>
                                                            <?php
                                                            if ($this->session->userdata['admin']['RoleID'] == 1 || $this->session->userdata['admin']['RoleID'] == 4) { ?>
                                                                <a href="javascript:void(0);" data-toggle="modal"
                                                                   data-target="#exampleModal"
                                                                   data-order-id="<?php echo $order->OrderID; ?>"
                                                                   class="btn btn-simple btn-warning btn-icon edit assign_order"><i
                                                                            class="material-icons"
                                                                            title="Assign To Driver">supervisor_account</i>
                                                                </a>
                                                            <?php }
                                                            ?>
                                                            <a href="<?php echo base_url('cms/orders/view/' . $order->OrderID); ?>"
                                                               class="btn btn-simple btn-warning btn-icon edit"><i
                                                                        class="material-icons"
                                                                        title="View order details">assignment</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                            <a href="<?php echo base_url('cms/orders/invoice/' . $order->OrderID); ?>"
                                                               class="btn btn-simple btn-warning btn-icon edit"
                                                               target="_blank"><i
                                                                        class="material-icons"
                                                                        title="View order invoice">receipt</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                            <?php
                                                            if ($order->HasTicket == 1) { ?>
                                                                <a href="<?php echo base_url('cms/ticket/view/' . $order->TicketID); ?>"
                                                                   class="btn btn-simple btn-warning btn-icon"><i
                                                                            class="material-icons"
                                                                            title="View order ticket details">dvr</i>
                                                                    <div class="ripple-container"></div>
                                                                </a>
                                                            <?php }
                                                            ?>

                                                        <?php } ?>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                        }

                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                            <div class="material-datatables tab-pane" id="packed">
                                <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Full Name</th>
                                        <th>Mobile</th>
                                        <th>Email</th>
                                        <th>Order #</th>
                                        <th>Order Amount</th>
                                        <th>User City</th>
                                        <th>Assigned To</th>
                                        <th>Received At</th>
                                        <?php if (checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                            <th><?php echo lang('actions'); ?></th>
                                        <?php } ?>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($packed_orders) {
                                        foreach ($packed_orders as $order) { ?>
                                            <tr id="<?php echo $order->OrderID; ?>"
                                                style="<?php echo $order->IsRead == 0 ? "font-weight: bold;" : "" ?>">
                                                <td><?php echo $order->FullName; ?></td>
                                                <td><?php echo $order->Mobile; ?></td>
                                                <td><?php echo $order->Email; ?></td>
                                                <td><?php echo $order->OrderNumber; ?></td>
                                                <td><?php echo number_format($order->TotalAmount, 2); ?> SAR</td>
                                                <td><?php echo $order->UserCity; ?></td>
                                                <td><?php echo($order->AssignedDriverName != '' ? ucfirst($order->AssignedDriverName) : 'N/A'); ?></td>
                                                <td><?php echo date('d-m-Y h:i A', strtotime($order->CreatedAt)); ?></td>
                                                <?php if (checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                    <!--<td>
                                                    <?php /*if(checkUserRightAccess(65,$this->session->userdata['admin']['UserID'],'CanEdit')){*/ ?>
                                                        <a href="<?php /*echo base_url('cms/orders/edit/'.$order->OrderID);*/ ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">edit</i><div class="ripple-container"></div></a>
                                                    <?php /*} */ ?>

                                                    <?php /*if(checkUserRightAccess(65,$this->session->userdata['admin']['UserID'],'CanDelete')){*/ ?>
                                                        <a href="javascript:void(0);" onclick="deleteRecord('<?php /*echo $order->OrderID;*/ ?>','cms/<?php /*echo $ControllerName; */ ?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>
                                                    <?php /*} */ ?>
                                                </td>-->
                                                    <td>
                                                        <?php if (checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                            <?php
                                                            if ($this->session->userdata['admin']['RoleID'] == 1 || $this->session->userdata['admin']['RoleID'] == 2 || $this->session->userdata['admin']['RoleID'] == 4) { ?>
                                                                <a href="javascript:void(0);" data-order_id="<?php echo $order->OrderID; ?>" data-order_status="3" data-status_title="Dispatched"
                                                                   class="btn btn-simple btn-warning btn-icon edit changeOrderStatus">
                                                                    <i class="material-icons" title="Click to update status to dispatched">thumb_up</i>
                                                                    <div class="ripple-container"></div>
                                                                </a>
                                                            <?php } ?>
                                                            <a href="<?php echo base_url('cms/orders/view/' . $order->OrderID); ?>"
                                                               class="btn btn-simple btn-warning btn-icon edit"><i
                                                                        class="material-icons"
                                                                        title="View order details">assignment</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                            <a href="<?php echo base_url('cms/orders/invoice/' . $order->OrderID); ?>"
                                                               class="btn btn-simple btn-warning btn-icon edit"
                                                               target="_blank"><i
                                                                        class="material-icons"
                                                                        title="View order invoice">receipt</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                            <?php
                                                            if ($order->HasTicket == 1) { ?>
                                                                <a href="<?php echo base_url('cms/ticket/view/' . $order->TicketID); ?>"
                                                                   class="btn btn-simple btn-warning btn-icon"><i
                                                                            class="material-icons"
                                                                            title="View order ticket details">dvr</i>
                                                                    <div class="ripple-container"></div>
                                                                </a>
                                                            <?php }
                                                            ?>

                                                        <?php } ?>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                        }

                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                            <div class="material-datatables tab-pane" id="dispatched">
                                <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Full Name</th>
                                        <th>Mobile</th>
                                        <th>Email</th>
                                        <th>Order #</th>
                                        <th>Order Amount</th>
                                        <th>User City</th>
                                        <th>Assigned To</th>
                                        <th>Received At</th>
                                        <?php if (checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                            <th><?php echo lang('actions'); ?></th>
                                        <?php } ?>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($dispatched_orders) {
                                        foreach ($dispatched_orders as $order) { ?>
                                            <tr id="<?php echo $order->OrderID; ?>"
                                                style="<?php echo $order->IsRead == 0 ? "font-weight: bold;" : "" ?>">
                                                <td><?php echo $order->FullName; ?></td>
                                                <td><?php echo $order->Mobile; ?></td>
                                                <td><?php echo $order->Email; ?></td>
                                                <td><?php echo $order->OrderNumber; ?></td>
                                                <td><?php echo number_format($order->TotalAmount, 2); ?> SAR</td>
                                                <td><?php echo $order->UserCity; ?></td>
                                                <td><?php echo($order->AssignedDriverName != '' ? ucfirst($order->AssignedDriverName) : 'N/A'); ?></td>
                                                <td><?php echo date('d-m-Y h:i A', strtotime($order->CreatedAt)); ?></td>
                                                <?php if (checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                    <!--<td>
                                                    <?php /*if(checkUserRightAccess(65,$this->session->userdata['admin']['UserID'],'CanEdit')){*/ ?>
                                                        <a href="<?php /*echo base_url('cms/orders/edit/'.$order->OrderID);*/ ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">edit</i><div class="ripple-container"></div></a>
                                                    <?php /*} */ ?>

                                                    <?php /*if(checkUserRightAccess(65,$this->session->userdata['admin']['UserID'],'CanDelete')){*/ ?>
                                                        <a href="javascript:void(0);" onclick="deleteRecord('<?php /*echo $order->OrderID;*/ ?>','cms/<?php /*echo $ControllerName; */ ?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>
                                                    <?php /*} */ ?>
                                                </td>-->
                                                    <td>
                                                        <?php if (checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                            <a href="javascript:void(0);" data-order_id="<?php echo $order->OrderID; ?>" data-order_status="4" data-status_title="Delivered"
                                                               class="btn btn-simple btn-warning btn-icon edit changeOrderStatus">
                                                                <i class="material-icons" title="Click to update status to delivered">thumb_up</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                            <a href="<?php echo base_url('cms/orders/view/' . $order->OrderID); ?>"
                                                               class="btn btn-simple btn-warning btn-icon edit"><i
                                                                        class="material-icons"
                                                                        title="View order details">assignment</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                            <a href="<?php echo base_url('cms/orders/invoice/' . $order->OrderID); ?>"
                                                               class="btn btn-simple btn-warning btn-icon edit"
                                                               target="_blank"><i
                                                                        class="material-icons"
                                                                        title="View order invoice">receipt</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                            <?php
                                                            if ($order->HasTicket == 1) { ?>
                                                                <a href="<?php echo base_url('cms/ticket/view/' . $order->TicketID); ?>"
                                                                   class="btn btn-simple btn-warning btn-icon"><i
                                                                            class="material-icons"
                                                                            title="View order ticket details">dvr</i>
                                                                    <div class="ripple-container"></div>
                                                                </a>
                                                            <?php }
                                                            ?>

                                                        <?php } ?>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                        }

                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                            <div class="material-datatables tab-pane" id="delivered">
                                <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Full Name</th>
                                        <th>Mobile</th>
                                        <th>Email</th>
                                        <th>Order #</th>
                                        <th>Order Amount</th>
                                        <th>User City</th>
                                        <th>Assigned To</th>
                                        <th>Received At</th>
                                        <?php if (checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                            <th><?php echo lang('actions'); ?></th>
                                        <?php } ?>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($delivered_orders) {
                                        foreach ($delivered_orders as $order) { ?>
                                            <tr id="<?php echo $order->OrderID; ?>"
                                                style="<?php echo $order->IsRead == 0 ? "font-weight: bold;" : "" ?>">
                                                <td><?php echo $order->FullName; ?></td>
                                                <td><?php echo $order->Mobile; ?></td>
                                                <td><?php echo $order->Email; ?></td>
                                                <td><?php echo $order->OrderNumber; ?></td>
                                                <td><?php echo number_format($order->TotalAmount, 2); ?> SAR</td>
                                                <td><?php echo $order->UserCity; ?></td>
                                                <td><?php echo($order->AssignedDriverName != '' ? ucfirst($order->AssignedDriverName) : 'N/A'); ?></td>
                                                <td><?php echo date('d-m-Y h:i A', strtotime($order->CreatedAt)); ?></td>
                                                <?php if (checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                    <!--<td>
                                                    <?php /*if(checkUserRightAccess(65,$this->session->userdata['admin']['UserID'],'CanEdit')){*/ ?>
                                                        <a href="<?php /*echo base_url('cms/orders/edit/'.$order->OrderID);*/ ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">edit</i><div class="ripple-container"></div></a>
                                                    <?php /*} */ ?>

                                                    <?php /*if(checkUserRightAccess(65,$this->session->userdata['admin']['UserID'],'CanDelete')){*/ ?>
                                                        <a href="javascript:void(0);" onclick="deleteRecord('<?php /*echo $order->OrderID;*/ ?>','cms/<?php /*echo $ControllerName; */ ?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>
                                                    <?php /*} */ ?>
                                                </td>-->
                                                    <td>
                                                        <?php if (checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                            <a href="<?php echo base_url('cms/orders/invoice/' . $order->OrderID); ?>"
                                                               class="btn btn-simple btn-warning btn-icon edit"
                                                               target="_blank"><i
                                                                        class="material-icons"
                                                                        title="View order invoice">receipt</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                            <a href="<?php echo base_url('cms/orders/view/' . $order->OrderID); ?>"
                                                               class="btn btn-simple btn-warning btn-icon edit"><i
                                                                        class="material-icons"
                                                                        title="View order details">assignment</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                            <?php
                                                            if ($order->HasTicket == 1) { ?>
                                                                <a href="<?php echo base_url('cms/ticket/view/' . $order->TicketID); ?>"
                                                                   class="btn btn-simple btn-warning btn-icon"><i
                                                                            class="material-icons"
                                                                            title="View order ticket details">dvr</i>
                                                                    <div class="ripple-container"></div>
                                                                </a>
                                                            <?php }
                                                            ?>
                                                        <?php } ?>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                        }

                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                            <div class="material-datatables tab-pane" id="cancelled">
                                <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Full Name</th>
                                        <th>Mobile</th>
                                        <th>Email</th>
                                        <th>Order #</th>
                                        <th>Order Amount</th>
                                        <th>User City</th>
                                        <th>Assigned To</th>
                                        <th>Received At</th>
                                        <?php if (checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                            <th><?php echo lang('actions'); ?></th>
                                        <?php } ?>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($cancelled_orders) {
                                        foreach ($cancelled_orders as $order) { ?>
                                            <tr id="<?php echo $order->OrderID; ?>"
                                                style="<?php echo $order->IsRead == 0 ? "font-weight: bold;" : "" ?>">
                                                <td><?php echo $order->FullName; ?></td>
                                                <td><?php echo $order->Mobile; ?></td>
                                                <td><?php echo $order->Email; ?></td>
                                                <td><?php echo $order->OrderNumber; ?></td>
                                                <td><?php echo number_format($order->TotalAmount, 2); ?> SAR</td>
                                                <td><?php echo $order->UserCity; ?></td>
                                                <td><?php echo($order->AssignedDriverName != '' ? ucfirst($order->AssignedDriverName) : 'N/A'); ?></td>
                                                <td><?php echo date('d-m-Y h:i A', strtotime($order->CreatedAt)); ?></td>
                                                <?php if (checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                    <!--<td>
                                                    <?php /*if(checkUserRightAccess(65,$this->session->userdata['admin']['UserID'],'CanEdit')){*/ ?>
                                                        <a href="<?php /*echo base_url('cms/orders/edit/'.$order->OrderID);*/ ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">edit</i><div class="ripple-container"></div></a>
                                                    <?php /*} */ ?>

                                                    <?php /*if(checkUserRightAccess(65,$this->session->userdata['admin']['UserID'],'CanDelete')){*/ ?>
                                                        <a href="javascript:void(0);" onclick="deleteRecord('<?php /*echo $order->OrderID;*/ ?>','cms/<?php /*echo $ControllerName; */ ?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>
                                                    <?php /*} */ ?>
                                                </td>-->
                                                    <td>
                                                        <?php if (checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                            <a href="<?php echo base_url('cms/orders/invoice/' . $order->OrderID); ?>"
                                                               class="btn btn-simple btn-warning btn-icon edit"
                                                               target="_blank"><i
                                                                        class="material-icons"
                                                                        title="View order invoice">receipt</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                            <a href="<?php echo base_url('cms/orders/view/' . $order->OrderID); ?>"
                                                               class="btn btn-simple btn-warning btn-icon edit"><i
                                                                        class="material-icons"
                                                                        title="View order details">assignment</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                            <?php
                                                            if ($order->HasTicket == 1) { ?>
                                                                <a href="<?php echo base_url('cms/ticket/view/' . $order->TicketID); ?>"
                                                                   class="btn btn-simple btn-warning btn-icon"><i
                                                                            class="material-icons"
                                                                            title="View order ticket details">dvr</i>
                                                                    <div class="ripple-container"></div>
                                                                </a>
                                                            <?php }
                                                            ?>
                                                        <?php } ?>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                        }

                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                            <?php if ($this->session->userdata['admin']['RoleID'] == 1) { ?>
                                <div class="material-datatables tab-pane" id="unopened">
                                    <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                           cellspacing="0" width="100%" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>Full Name</th>
                                            <th>Mobile</th>
                                            <th>Email</th>
                                            <th>Order #</th>
                                            <th>Order Amount</th>
                                            <th>User City</th>
                                            <th>Assigned To</th>
                                            <th>Received At</th>
                                            <?php if (checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                <th><?php echo lang('actions'); ?></th>
                                            <?php } ?>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if ($unopened_orders) {
                                            foreach ($unopened_orders as $order) { ?>
                                                <tr id="<?php echo $order->OrderID; ?>"
                                                    style="<?php echo $order->IsRead == 0 ? "font-weight: bold;" : "" ?>">
                                                    <td><?php echo $order->FullName; ?></td>
                                                    <td><?php echo $order->Mobile; ?></td>
                                                    <td><?php echo $order->Email; ?></td>
                                                    <td><?php echo $order->OrderNumber; ?></td>
                                                    <td><?php echo number_format($order->TotalAmount, 2); ?> SAR</td>
                                                    <td><?php echo $order->UserCity; ?></td>
                                                    <td><?php echo($order->AssignedDriverName != '' ? ucfirst($order->AssignedDriverName) : 'N/A'); ?></td>
                                                    <td><?php echo date('d-m-Y h:i A', strtotime($order->CreatedAt)); ?></td>
                                                    <?php if (checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                        <!--<td>
                                                    <?php /*if(checkUserRightAccess(65,$this->session->userdata['admin']['UserID'],'CanEdit')){*/ ?>
                                                        <a href="<?php /*echo base_url('cms/orders/edit/'.$order->OrderID);*/ ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">edit</i><div class="ripple-container"></div></a>
                                                    <?php /*} */ ?>

                                                    <?php /*if(checkUserRightAccess(65,$this->session->userdata['admin']['UserID'],'CanDelete')){*/ ?>
                                                        <a href="javascript:void(0);" onclick="deleteRecord('<?php /*echo $order->OrderID;*/ ?>','cms/<?php /*echo $ControllerName; */ ?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>
                                                    <?php /*} */ ?>
                                                </td>-->
                                                        <td>
                                                            <?php if (checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                                <?php
                                                                if ($this->session->userdata['admin']['RoleID'] == 1 || $this->session->userdata['admin']['RoleID'] == 4) { ?>
                                                                    <a href="javascript:void(0);" data-toggle="modal"
                                                                       data-target="#exampleModal"
                                                                       data-order-id="<?php echo $order->OrderID; ?>"
                                                                       class="btn btn-simple btn-warning btn-icon edit assign_order"><i
                                                                                class="material-icons"
                                                                                title="Assign To Driver">supervisor_account</i>
                                                                    </a>
                                                                <?php }
                                                                ?>
                                                                <a href="<?php echo base_url('cms/orders/view/' . $order->OrderID); ?>"
                                                                   class="btn btn-simple btn-warning btn-icon edit"><i
                                                                            class="material-icons"
                                                                            title="View order details">assignment</i>
                                                                    <div class="ripple-container"></div>
                                                                </a>
                                                                <a href="<?php echo base_url('cms/orders/invoice/' . $order->OrderID); ?>"
                                                                   class="btn btn-simple btn-warning btn-icon edit"
                                                                   target="_blank"><i
                                                                            class="material-icons"
                                                                            title="View order invoice">receipt</i>
                                                                    <div class="ripple-container"></div>
                                                                </a>
                                                                <?php
                                                                if ($order->HasTicket == 1) { ?>
                                                                    <a href="<?php echo base_url('cms/ticket/view/' . $order->TicketID); ?>"
                                                                       class="btn btn-simple btn-warning btn-icon"><i
                                                                                class="material-icons"
                                                                                title="View order ticket details">dvr</i>
                                                                        <div class="ripple-container"></div>
                                                                    </a>
                                                                <?php }
                                                                ?>

                                                            <?php } ?>
                                                        </td>
                                                    <?php } ?>
                                                </tr>
                                                <?php
                                            }

                                        }
                                        ?>

                                        </tbody>
                                    </table>
                                </div>
                            <?php } ?>
                        </div><!-- tab-content -->
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
    <audio id="audiotag" src="<?php echo base_url('assets/bell.mp3'); ?>" preload="auto"></audio>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document" id="response_data">

    </div>
</div>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>
<script>
    $(document).ready(function () {
        $('table.datatable').DataTable({
            "ordering": false
        });
        $('.assign_order').on('click', function () {

            var OrderID = $(this).attr('data-order-id');


            $('#response_data').html('');

            $.blockUI({
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });


            $.ajax({
                type: "POST",
                url: base_url + 'cms/orders/get_drivers',
                data: {
                    'OrderID': OrderID
                },
                dataType: "json",
                cache: false,
                //async:false,
                success: function (result) {

                    $('#response_data').html(result.html);
                    $('#exampleModal').modal('show');

                },
                complete: function () {
                    $.unblockUI();
                }
            });

        });
    });
</script>

<script>

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('a796cb54d7c4b4ae4893', {
        cluster: 'ap2',
        forceTLS: true
    });

    var channel = pusher.subscribe('Chocomood_Order_Channel');
    channel.bind('Chocomood_Order_Event', function (data) {
        console.log(JSON.stringify(data));
        playAudio();
        setTimeout(function () {
            window.location.reload();
        }, 2000);
    });

    function playAudio() {
        var audiotag = document.getElementById('audiotag');
        audiotag.play();
    }
</script>