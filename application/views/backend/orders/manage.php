<style>
    .orderHeader .w-130 {width:130px;}
    .orderDtlRow td {
        display: table-cell !important;
        background: #f3ebd5;
        border: 0 !important;
    }
    .orderDtlRow {
        display:none;
    }
       
    .click4_OrderDtl {transition-duration:0.5s;}
    .click4_OrderDtl.open,
    .click4_OrderDtl:hover td {
        background: #f3ebd5;
    }
    .card-body {
        padding: 15px;
    }
    .card-body .wbox {
        margin-bottom: 25px;
        background-color: #ffffff;
        padding: 25px;
        border-radius: 6px;
        box-shadow: 3px 3px 13px -2px rgba(0, 0, 0, 0.59);
    }
</style>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Search</h4>
                        
                        <div class="material-datatables">
                            <form action="" method="post" style="">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group is-focused">
                                            <label class="control-label" for="Email ">User Email</label>
                                            <input type="text" name="Email" class="form-control" value="<?php echo (isset($post_data['Email']) ? $post_data['Email'] : '' ); ?>">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <div class="form-group is-focused">
                                            <label class="control-label" for="OrderTrackID ">Order Track ID</label>
                                            <input type="text" name="OrderTrackID" class="form-control" value="<?php echo (isset($post_data['OrderTrackID']) ? $post_data['OrderTrackID'] : '' ); ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group  is-focused">
                                            <label class="control-label" for="DepositDate ">From</label>
                                            <input type="text" name="From" class="form-control custom_datepicker" value="<?php echo (isset($post_data['From']) ? $post_data['From'] : '' ); ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group is-focused">
                                            <label class="control-label" for="DepositDate ">To</label>
                                            <input type="text" name="To" class="form-control custom_datepicker" value="<?php echo (isset($post_data['To']) ? $post_data['To'] : '' ); ?>">
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button class="btn btn-primary waves-effect waves-light" type="submit">
                                               Search
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        
        <div>
            Toggle column: 
            <a class="toggle-vis" data-column="0">Full Name</a> - 
            <a class="toggle-vis" data-column="1">Mobile</a> - 
            <a class="toggle-vis" data-column="2">Email</a> - 
            <a class="toggle-vis" data-column="3">Order #</a> - 
            <a class="toggle-vis" data-column="4">Transaction ID</a> - 
            <a class="toggle-vis" data-column="5">Order Amount</a> -
            
            <?php
            if (true || (isset($_GET['status']) && ($_GET['status'] == 'all' || $_GET['status'] == 'unopened'))) { ?>
                <a class="toggle-vis" data-column="6">Order Status</a> -
                <a class="toggle-vis" data-column="8">Delivery Type</a> -
                <a class="toggle-vis" data-column="7">Branch</a> -
                <a class="toggle-vis" data-column="9">User City</a> -
                <a class="toggle-vis" data-column="10">Assigned To</a> -
                <a class="toggle-vis" data-column="11">Received At</a> -
                <?php if (checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                    <a class="toggle-vis" data-column="11"><?php echo lang('actions'); ?></a>
                <?php }
            }else
            {
                ?>
                <a class="toggle-vis" data-column="6">Branch</a> -
                <a class="toggle-vis" data-column="8">Delivery Type</a> -
                <a class="toggle-vis" data-column="7">User City</a> -
                <a class="toggle-vis" data-column="9">Assigned To</a> -
                <a class="toggle-vis" data-column="10">Received At</a> -
                <?php if (checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                    <a class="toggle-vis" data-column="11"><?php echo lang('actions'); ?></a>
                <?php } 
            }
            ?>
            
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Orders</h4>
                        <div class="toolbar">
                            <ul class="nav nav-pills">
                                <li class="<?php echo($url_status == 'pending' ? 'active nav-pills-warning' : ''); ?>">
                                    <a href="<?php echo base_url('cms/orders?status=pending'); ?>">Pending</a>
                                </li>
                                <li class="<?php echo($url_status == 'packed' ? 'active nav-pills-warning' : ''); ?>">
                                    <a href="<?php echo base_url('cms/orders?status=packed'); ?>">Packed</a>
                                </li>
                                <li class="<?php echo($url_status == 'dispatched' ? 'active nav-pills-warning' : ''); ?>">
                                    <a href="<?php echo base_url('cms/orders?status=dispatched'); ?>">Dispatched</a>
                                </li>
                                <li class="<?php echo($url_status == 'delivered' ? 'active nav-pills-warning' : ''); ?>">
                                    <a href="<?php echo base_url('cms/orders?status=delivered'); ?>">Delivered</a>
                                </li>
                                <li class="<?php echo($url_status == 'cancelled' ? 'active nav-pills-warning' : ''); ?>">
                                    <a href="<?php echo base_url('cms/orders?status=cancelled'); ?>">Cancelled</a>
                                </li>
                                <li class="<?php echo($url_status == 'cancelled_not_collect' ? 'active nav-pills-warning' : ''); ?>">
                                    <a href="<?php echo base_url('cms/orders?status=cancelled_not_collect'); ?>">Cancelled Not Collected</a>
                                </li>
                                <li class="<?php echo($url_status == 'all' ? 'active nav-pills-warning' : ''); ?>">
                                    <a href="<?php echo base_url('cms/orders?status=all'); ?>">All</a>
                                </li>
                                <?php if ($this->session->userdata['admin']['RoleID'] == 1) { ?>
                                    <li class="<?php echo($url_status == 'unopened' ? 'active nav-pills-warning' : ''); ?>">
                                        <a href="<?php echo base_url('cms/orders?status=unopened'); ?>">Un-Opened</a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="material-datatables">
                            <table id="" class="datatables  table  table-striped table-no-bordered table-hover"
                                   cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr class="orderHeader">
                                    <th>Full Name</th>
                                    <th>Mobile</th>
                                    <th>Email</th>
                                    <th>Order #</th> 
                                    <th>Transaction ID </th> 
                                    <th>Order Amount</th>
                                    <?php
                                    if (true || (isset($_GET['status']) && ($_GET['status'] == 'all' || $_GET['status'] == 'unopened'))) { ?>
                                        <th>Order Status</th>
                                    <?php }
                                    ?>
                                    <th>Delivery Type</th>
                                    <th>Branch</th>
                                    <th>User City</th>
                                    <th>Assigned To</th>
                                    <th>Received At</th>
                                    <?php if (checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                        <th class="w-130"><?php echo lang('actions'); ?></th>
                                    <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                 if ($orders) {
                                    foreach ($orders as $order) {
                                        $show_status_change = false;
                                        if ($order->Status == 1)
                                        {
                                            $StatusID = 2;
                                            $StatusTitle = 'Packed';
                                            $show_status_change = true;
                                        } elseif ($order->Status == 2)
                                        {
                                            $StatusID = 3;
                                            $StatusTitle = 'Dispatched';
                                            $show_status_change = true;
                                        } elseif ($order->Status == 3)
                                        {
                                            $StatusID = 4;
                                            $StatusTitle = 'Delivered';
                                            $show_status_change = true;
                                        }
                                        ?>
                                        <tr class="click4_OrderDtl" id="<?php echo $order->OrderID; ?>"
                                            style="<?php echo $order->IsRead == 0 ? "font-weight: bold;" : "" ?>">
                                            <td><?php echo $order->FullName; ?></td>
                                            <td><?php echo $order->Mobile; ?></td>
                                            <td><?php echo $order->Email; ?></td>
                                            <td><?php echo $order->OrderNumber; ?></td>
                                            <td><?php echo $order->TransactionID; ?></td>
                                            <td><?php echo number_format($order->TotalAmount, 2); ?> SAR</td>
                                            <?php
                                            if (true || (isset($_GET['status']) && ($_GET['status'] == 'all' || $_GET['status'] == 'unopened'))) { ?>
                                                <td><?php echo $order->OrderStatusEn; ?></td>
                                            <?php }
                                            ?>
                                            <td><?php
                                            if($order->SemsaShippingAmount && $order->SemsaShippingAmount > 0){
                                                echo 'SMSA';

                                            }else{
                                                if(isset($order->CollectFromStore) && $order->CollectFromStore > 0){
                                                    echo "Pickup for Store";
                                                }else{
                                                    echo $order->ShipmentMethodTitle;
                                                }
                                            }
                                              ?>
                                                 
                                             </td>
                                            <td><?php echo $order->StoreTitle; ?></td>
                                            <td><?php echo $order->UserCity; ?></td>
                                            <td><?php echo($order->AssignedDriverName != '' ? ucfirst($order->AssignedDriverName) : 'N/A'); ?></td>
                                            <td><span style="display: none;"><?php echo strtotime($order->CreatedAt); ?></span><?php echo date('d-m-Y h:i A', strtotime($order->CreatedAt)); ?></td>
                                            <?php if (checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                <!--<td>
                                                    <?php /*if(checkUserRightAccess(65,$this->session->userdata['admin']['UserID'],'CanEdit')){*/ ?>
                                                        <a href="<?php /*echo base_url('cms/orders/edit/'.$order->OrderID);*/ ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">edit</i><div class="ripple-container"></div></a>
                                                    <?php /*} */ ?>

                                                    <?php /*if(checkUserRightAccess(65,$this->session->userdata['admin']['UserID'],'CanDelete')){*/ ?>
                                                        <a href="javascript:void(0);" onclick="deleteRecord('<?php /*echo $order->OrderID;*/ ?>','cms/<?php /*echo $ControllerName; */ ?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>
                                                    <?php /*} */ ?>
                                                </td>-->
                                                <td>
                                                    <?php if (checkUserRightAccess(65, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                        <?php
                                                        if (($this->session->userdata['admin']['RoleID'] == 1 || $this->session->userdata['admin']['RoleID'] == 2 || $this->session->userdata['admin']['RoleID'] == 4) && $show_status_change) { 
                                                              if (isset($order->Status) && $order->Status != 4){
                                                            ?>
                                                            <a href="javascript:void(0);"
                                                               data-order_id="<?php echo $order->OrderID; ?>"
                                                               data-order_status="<?php echo $StatusID; ?>" data-status_title="<?php echo $StatusTitle; ?>"
                                                               class="btn btn-simple btn-warning btn-icon edit changeOrderStatus">
                                                                <i class="material-icons"
                                                                   title="Click to update status to packed">thumb_up</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                        <?php } } ?>
                                                        <?php
                                                        if ($this->session->userdata['admin']['RoleID'] == 1 || $this->session->userdata['admin']['RoleID'] == 4) { 
                                                              if (isset($order->CollectFromStore) && $order->CollectFromStore != 1 && isset($order->Status) && $order->Status != 4 && $order->SemsaShippingAmount < 1){
                                                            ?>
                                                            <a href="javascript:void(0);" data-toggle="modal"
                                                               data-target="#exampleModal"
                                                               data-order-id="<?php echo $order->OrderID; ?>"
                                                               class="btn btn-simple btn-warning btn-icon edit assign_order"><i
                                                                        class="material-icons"
                                                                        title="Assign To Driver">supervisor_account</i>
                                                            </a>
                                                        <?php }
                                                                }
                                                        ?>
                                                        <a href="<?php echo base_url('cms/orders/view/' . $order->OrderID); ?>"
                                                           class="btn btn-simple btn-warning btn-icon edit"><i
                                                                    class="material-icons"
                                                                    title="View order details">assignment</i>
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                        <a href="<?php echo base_url('cms/orders/invoice/' . $order->OrderID); ?>"
                                                           class="btn btn-simple btn-warning btn-icon edit"
                                                           target="_blank"><i
                                                                    class="material-icons"
                                                                    title="View order invoice">receipt</i>
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                        <?php
                                                        if ($order->HasTicket == 1) { ?>
                                                            <a href="<?php echo base_url('cms/ticket/view/' . $order->TicketID); ?>"
                                                               class="btn btn-simple btn-warning btn-icon"><i
                                                                        class="material-icons"
                                                                        title="View order ticket details">dvr</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                        <?php }
                                                        ?>

                                                    <?php } ?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <!-- <tr class="orderDtlRow" id="orderDtl_<?php echo $order->OrderID; ?>">
                                            <td colspan="9">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <div class="card-title">
                                                                        <?php
                                                                        $show_cancel_button = false;
                                                                        $show_receipt_button = false;
                                                                        $status = $order->Status;
                                                                        if ($status == 1) {
                                                                            $class = "btn btn-sm";
                                                                        } else if ($status == 2) {
                                                                            $class = "btn btn-primary btn-sm";
                                                                        } else if ($status == 3) {
                                                                            $class = "btn btn-warning btn-sm";
                                                                        } else if ($status == 4) {
                                                                            $class = "btn btn-success btn-sm";
                                                                        } else if ($status == 5) {
                                                                            $class = "btn btn-danger btn-sm";
                                                                        }
                                                                        ?>
                                                                        Details For Order # <b><?php echo $order->OrderNumber; ?></b>
                                                                        &nbsp;&nbsp;<button class="<?php echo $class; ?>">
                                                                            <?php echo $order->OrderStatusEn; ?>
                                                                            <div class="ripple-container"></div>
                                                                        </button>
                                                                        <div class="pull-right">
                                                                            <?php
                                                                            if ($order->HasTicket == 1) { ?>
                                                                                <a href="<?php echo base_url('cms/ticket/view') . '/' . $order->TicketID; ?>">
                                                                                    <button class="btn btn-primary btn-sm">
                                                                                        View Ticket Details
                                                                                        <div class="ripple-container"></div>
                                                                                    </button>
                                                                                </a>
                                                                            <?php }
                                                                            ?>
                                                                            <?php
                                                                            if ($show_cancel_button) { ?>
                                                                                <button class="btn btn-danger btn-sm"
                                                                                        onclick="cancelOrder(<?php echo $order->OrderID; ?>, <?php echo $order->UserID; ?>);">
                                                                                    Cancel Order
                                                                                    <div class="ripple-container"></div>
                                                                                </button>
                                                                            <?php }
                                                                            ?>
                                                                            <?php
                                                                            if ($this->session->userdata['admin']['RoleID'] == 1 || $this->session->userdata['admin']['RoleID'] == 4) { ?>
                                                                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#exampleModal" class="assign_order">
                                                                                        <button class="btn btn-warning btn-sm">
                                                                                            Assign Order
                                                                                            <div class="ripple-container"></div>
                                                                                        </button>
                                                                                    </a>
                                                                            <?php }
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="card-content">
                                                                    <div class="row">
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group label-floating">
                                                                                <div class="form-line">
                                                                                    <label class="control-label">Customer Name</label>
                                                                                    <h5>
                                                                                        <a href="<?php echo base_url('cms/customer/edit') . '/' . $order->UserID; ?>"
                                                                                        target="_blank"><?php echo $order->FullName; ?></a></h5>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group label-floating">
                                                                                <div class="form-line">
                                                                                    <label class="control-label">Mobile No.</label>
                                                                                    <h5><?php echo($order->Mobile != '' ? '<a href="tel:' . $order->Mobile . '">' . $order->Mobile . '</a>' : 'N/A'); ?></h5>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group label-floating">
                                                                                <div class="form-line">
                                                                                    <label class="control-label">Email</label>
                                                                                    <h5><?php echo($order->Email != '' ? '<a href="mailto:' . $order->Email . '">' . $order->Email . '</a>' : 'N/A'); ?></h5>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group label-floating">
                                                                                <div class="form-line">
                                                                                    <label class="control-label">User City</label>
                                                                                    <h5><?php echo($order->UserCity != '' ? ucfirst($order->UserCity) : 'N/A'); ?></h5>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group label-floating">
                                                                                <div class="form-line">
                                                                                    <label class="control-label">Branch</label>
                                                                                    <h5><?php echo($order->StoreTitle != '' ? ucfirst($order->StoreTitle) : 'N/A'); ?></h5>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group label-floating">
                                                                                <div class="form-line">
                                                                                    <label class="control-label">Delivery District</label>
                                                                                    <h5><?php echo($order->DistrictTitle != '' ? ucfirst($order->DistrictTitle) : 'N/A'); ?></h5>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group label-floating">
                                                                                <div class="form-line">
                                                                                    <label class="control-label">Coupon Code Used</label>
                                                                                    <h5><?php echo($order->CouponCodeUsed != '' ? $order->CouponCodeUsed : 'N/A'); ?></h5>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group label-floating">
                                                                                <div class="form-line">
                                                                                    <label class="control-label">Coupon Discount %</label>
                                                                                    <h5><?php echo($order->CouponCodeDiscountPercentage > 0 ? $order->CouponCodeDiscountPercentage : 'N/A'); ?></h5>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group label-floating">
                                                                                <div class="form-line">
                                                                                    <label class="control-label">Discount Availed</label>
                                                                                    <h5><?php echo($order->DiscountAvailed > 0 ? $order->DiscountAvailed . ' SAR' : 'N/A') ?></h5>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group label-floating">
                                                                                <div class="form-line">
                                                                                    <label class="control-label">Total Shipping Charges</label>
                                                                                    <h5><?php echo number_format($order->TotalShippingCharges, 2); ?> SAR</h5>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group label-floating">
                                                                                <div class="form-line">
                                                                                    <label class="control-label">Total Tax Amount</label>
                                                                                    <h5><?php echo number_format($order->TotalTaxAmount, 2); ?> SAR</h5>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group label-floating">
                                                                                <div class="form-line">
                                                                                    <label class="control-label">Total Amount</label>
                                                                                    <h5><?php echo number_format($order->TotalAmount, 2); ?> SAR</h5>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group label-floating">
                                                                                <div class="form-line">
                                                                                    <label class="control-label">Payment Method</label>
                                                                                    <h5><?php echo $order->PaymentMethod; ?></h5>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group label-floating">
                                                                                <div class="form-line">
                                                                                    <label class="control-label">Shipment Method</label>
                                                                                    <h5><?php echo $order->ShipmentMethodTitle; ?></h5>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group label-floating">
                                                                                <div class="form-line">
                                                                                    <label class="control-label">Order Placed At</label>
                                                                                    <h5><?php echo date('d-m-Y h:i:s A', strtotime($order->CreatedAt)); ?></h5>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group label-floating">
                                                                                <div class="form-line">
                                                                                    <label class="control-label">Est Order Delivery</label>
                                                                                    <?php
                                                                                        $site_setting = site_settings();
                                                                                        $days_to_deliver = "+$site_setting->DaysToDeliver days";
                                                                                    ?>
                                                                                    <h5><?php echo date('d-m-Y', strtotime($days_to_deliver, strtotime($order->CreatedAt))); ?></h5>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <?php
                                                                        if (isset($order->CollectFromStore) && $order->CollectFromStore == 1) { ?>
                                                                            <h4>Collect From Store</h4>
                                                                        <?php } else { ?>
                                                                            <div class="col-md-12 col-sm-12">
                                                                                <div class="form-group label-floating">
                                                                                    <div class="form-line">
                                                                                        <label class="control-label">Delivery Address</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-sm-4"><div class="form-group label-floating"><div class="form-line"><label class="control-label form-group">Recipient Name: </label><h5><?php echo $order->RecipientName; ?></h5></div></div></div>
                                                                                    <div class="col-sm-4"><div class="form-group label-floating"><div class="form-line"><label class="control-label form-group">Recipient Mobile No: </label><h5><?php echo $order->MobileNo; ?></h5></div></div></div>
                                                                                    <div class="col-sm-4"><div class="form-group label-floating"><div class="form-line"><label class="control-label form-group">Email: </label><h5><?php echo $order->Email; ?></h5></div></div></div>
                                                                                    
                                                                                    <div class="col-sm-4"><div class="form-group label-floating"><div class="form-line"><label class="control-label form-group">City: </label><h5><?php echo $order->AddressCity; ?></h5></div></div></div>
                                                                                    <div class="col-sm-4"><div class="form-group label-floating"><div class="form-line"><label class="control-label form-group">District: </label><h5><?php echo $order->AddressDistrict; ?></h5></div></div></div>
                                                                                    <div class="col-sm-4"><div class="form-group label-floating"><div class="form-line"><label class="control-label form-group">Building No: </label><h5><?php echo $order->BuildingNo; ?></h5></div></div></div>
                                                                                    
                                                                                    <div class="col-sm-4"><div class="form-group label-floating"><div class="form-line"><label class="control-label form-group">Street: </label><h5><?php echo $order->Street; ?></h5></div></div></div>
                                                                                    <div class="col-sm-4"><div class="form-group label-floating"><div class="form-line"><label class="control-label form-group">P.O Box: </label><h5><?php echo $order->POBox; ?></h5></div></div></div>
                                                                                    <div class="col-sm-4"><div class="form-group label-floating"><div class="form-line"><label class="control-label form-group">Zip Code: </label><h5><?php echo $order->ZipCode; ?></h5></div></div></div>

                                                                                    <div class="col-sm-12">
                                                                                        <div class="form-group label-floating"><div class="form-line">
                                                                                            <label class="control-label form-group">See location on map: </label>
                                                                                            <h5>
                                                                                                <a href="https://www.google.com/maps/search/?api=1&amp;query=<?php echo $order->Latitude; ?>,<?php echo $order->Longitude; ?>" target="_blank">
                                                                                                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTHbVVHxTASKeTMj0H-JFtHN138p-i6Rx-UdC0VQ2l17yJcaRFVCQ" height="25" width="25" style="height: 40px !important;width: 40px !important;">
                                                                                                </a>
                                                                                            </h5>
                                                                                    </div>
                                                                                    </div>
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                            </div>
                                                                            
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6">
                                                        
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <h5 class="card-title">Extra Charges</h5>
                                                                </div>
                                                                <div class="card-content">
                                                                    <div class="material-datatables">
                                                                        <table class="table table-striped table-hover datatables" cellspacing="0" width="100%"
                                                                            style="width:100%">
                                                                            <thead>
                                                                            <tr>
                                                                                <th>Title</th>
                                                                                <th>Factor</th>
                                                                                <th>Amount</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            <?php 
                                                                                   $order_extra_charges =  orderExtraCharges($order->OrderID);

                                                                            if ($order_extra_charges) {
                                                                                foreach ($order_extra_charges as $extra_charge) { ?>
                                                                                    <tr>
                                                                                        <td><?php echo $extra_charge->Title; ?></td>
                                                                                        <td><?php echo $extra_charge->Factor; ?></td>
                                                                                        <td><?php echo number_format($extra_charge->Amount, 2); ?> SAR</td>
                                                                                    </tr>
                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <h5 class="card-title">Assigned To</h5>
                                                                </div>
                                                                <div class="card-content">
                                                                    <div class="row clearfix">
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group label-floating">
                                                                                <div class="form-line">
                                                                                    <label class="control-label" for="AssignedDriverName">Assigned Driver
                                                                                        Name</label>
                                                                                    <?php echo($order->AssignedDriverName != '' ? ucfirst($order->AssignedDriverName) : 'N/A'); ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group label-floating">
                                                                                <div class="form-line">
                                                                                    <label class="control-label" for="AssignedDriverEmail">Assigned Driver
                                                                                        Email</label>
                                                                                    <?php echo($order->AssignedDriverEmail != '' ? '<a href="mailto:' . $order->AssignedDriverEmail . '">' . $order->AssignedDriverEmail . '</a>' : 'N/A'); ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group label-floating">
                                                                                <div class="form-line">
                                                                                    <label class="control-label" for="AssignedDriverMobile">Assigned Driver
                                                                                        Mobile</label>
                                                                                    <?php echo($order->AssignedDriverMobile != '' ? '<a href="tel:' . $order->AssignedDriverMobile . '">' . $order->AssignedDriverMobile . '</a>' : 'N/A'); ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <h5 class="card-title">Change status</h5>
                                                                </div>
                                                                <div class="card-content">
                                                                    <div class="row clearfix">
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group label-floating">
                                                                                <div class="form-line">
                                                                                    <label class="control-label" for="Status">Status</label>
                                                                                    <select class="selectpicker coloredCssDdEd" id="Status" data-style="select-with-transition">
                                                                                        <?php foreach ($order_statuses as $order_status) { ?>
                                                                                            <option value="<?php echo $order_status->OrderStatusID; ?>" <?php echo($order_status->OrderStatusID == $order->Status ? 'selected disabled' : ''); ?>><?php echo $order_status->OrderStatusEn; ?></option>
                                                                                        <?php } ?>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <input type="hidden" id="OrderID" value="<?php echo $order->OrderID; ?>">
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group label-floating">
                                                                                <button class="btn btn-primary waves-effect waves-light pull-right"
                                                                                        type="button"
                                                                                        onclick="changeOrderStatus();">
                                                                                    <?php echo lang('submit'); ?>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <h5 class="card-title">Ordered Items</h5>
                                                                </div>
                                                                <div class="card-content">
                                                                    <div class="material-datatables">
                                                                        <table class="table table-striped table-hover datatables" cellspacing="0" width="100%"
                                                                            style="width:100%">
                                                                            <thead>
                                                                            <tr>
                                                                                <th>Product Image</th>
                                                                                <th>Product Title</th>
                                                                                <th>Quantity</th>
                                                                                <th>Price</th>
                                                                                <th>Total</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            <?php 
                                                                                $order_items = getOrderItems($order->OrderID);

                                                                            if ($order_items) {
                                                                                foreach ($order_items as $order_item) { ?>
                                                                                    <tr>
                                                                                        <td>
                                                                                    <?php
                                                                                    if ($order_item->ItemType == 'Product') { ?>
                                                                                            <a href="<?php echo base_url() . '/' . get_images($order_item->ProductID, 'product', false); ?>" target="_blank">
                                                                                                <img src="<?php echo base_url() . '/' . get_images($order_item->ProductID, 'product', false); ?>" style="width: 70px;height: 70px;">
                                                                                            </a>
                                                                                    <?php } elseif ($order_item->ItemType == 'Customized Shape') { ?>
                                                                                        <a href="<?php echo base_url($order_item->CustomizedShapeImage); ?>" target="_blank">
                                                                                            <img src="<?php echo base_url($order_item->CustomizedShapeImage); ?>" style="width: 70px;height: 70px;">
                                                                                        </a>
                                                                                    <?php } else { ?>
                                                                                        <a href="javascript:void(0);" class="chocobox_detail" title="Click to view whats inside" data-pids="<?php echo $order_item->CustomizedOrderProductIDs; ?>" data-box_type="<?php echo $order_item->ItemType; ?>" data-box_id="<?php echo $order_item->CustomizedBoxID; ?>">
                                                                                            <img src="<?php echo front_assets("images/".$order_item->ItemType.".png"); ?>" style="width: 70px;height: 70px;">
                                                                                        </a>
                                                                                    <?php } ?>
                                                                                        </td>
                                                                                        <td>
                                                                                            <?php
                                                                                            if ($order_item->ItemType == 'Product') { ?>
                                                                                                <a href="<?php echo base_url('cms/product/edit') . '/' . $order_item->ProductID; ?>"
                                                                                                target="_blank"><?php echo $order_item->Title; ?></a>
                                                                                            <?php } elseif ($order_item->ItemType == 'Customized Shape') { ?>
                                                                                                Customized Shape
                                                                                            <?php } else { ?>
                                                                                            <a href="javascript:void(0);" class="chocobox_detail" title="Click to view whats inside" data-pids="<?php echo $order_item->CustomizedOrderProductIDs; ?>" data-box_id="<?php echo $order_item->CustomizedBoxID; ?>" data-box_type="<?php echo $order_item->ItemType; ?>">
                                                                                                <?php echo $order_item->ItemType; ?>
                                                                                            </a>
                                                                                            <?php }
                                                                                            ?>
                                                                                        </td>
                                                                                        <td><?php echo $order_item->Quantity; ?><?php echo ($order_item->IsCorporateItem ? ' kg' : 'pcs'); ?></td>
                                                                                        <td><?php echo number_format($order_item->Amount, 2); ?> SAR</td>
                                                                                        <td><?php echo number_format($order_item->Quantity * $order_item->Amount, 2); ?>
                                                                                            SAR
                                                                                        </td>
                                                                                    </tr>
                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>        
                                        </tr> -->
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
    <audio id="audiotag" src="<?php echo base_url('assets/bell.mp3'); ?>" preload="auto"></audio>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document" id="response_data">

    </div>
</div>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>
<script>
    $(document).ready(function () {
        $('table.datatable').DataTable({
            // "ordering": false
            dom: 'Bfrtip',
            columnDefs: [
                {
                    targets: 1,
                    className: 'noVis'
                }
            ],
            buttons: [
                {
                    extend: 'colvis',
                    columns: ':not(.noVis)'
                }
            ]
        });
        $('a.toggle-vis').on('click', function(e) {
            e.preventDefault();
            var table = $('#DataTables_Table_0').DataTable();
            // Get the column API object
            var column = table.column($(this).attr('data-column'));

            // Toggle the visibility
            column.visible(!column.visible());
        });
        $('.assign_order').on('click', function () {

            var OrderID = $(this).attr('data-order-id');


            $('#response_data').html('');

            $.blockUI({
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });


            $.ajax({
                type: "POST",
                url: base_url + 'cms/orders/get_drivers',
                data: {
                    'OrderID': OrderID
                },
                dataType: "json",
                cache: false,
                //async:false,
                success: function (result) {

                    $('#response_data').html(result.html);
                    $('#exampleModal').modal('show');

                },
                complete: function () {
                    $.unblockUI();
                }
            });

        });
    });
</script>

<script>

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('a796cb54d7c4b4ae4893', {
        cluster: 'ap2',
        forceTLS: true
    });

    var channel = pusher.subscribe('Chocomood_Order_Channel');
    channel.bind('Chocomood_Order_Event', function (data) {
        console.log(JSON.stringify(data));
        playAudio();
        setTimeout(function () {
            window.location.reload();
        }, 2000);
    });

    function playAudio() {
        var audiotag = document.getElementById('audiotag');
        audiotag.play();
    }

    // dt-buttons
    $(document).ready(function () {
        $(".dt-buttons").children('button').removeClass();
        $(".dt-buttons").children('button').addClass('btn btn-primary btn-sm');
        $(".dt-buttons").children('button').children('span').text('Show / Hide Columns');
    });


//  table Rows Event
$('.click4_OrderDtl').click(function(e){
    //e.preventDefault();
    var value=$(this).attr('id');
    $('.click4_OrderDtl').removeClass('open');
    $(this).addClass('open');
    $('.orderDtlRow').slideUp();
    $('.orderDtlRow#orderDtl_'+value).slideDown();
    //  orderDtl_
    // alert(value);
});
</script>
<script>
    $(document).ready(function () {
        $('.assign_order').on('click', function () {
            var OrderID = $(this).attr('data-order-id');
            $('#response_data').html('');
            $.blockUI({
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
            $.ajax({
                type: "POST",
                url: base_url + 'cms/orders/get_drivers',
                data: {
                    'OrderID': OrderID
                },
                dataType: "json",
                cache: false,
                //async:false,
                success: function (result) {
                    $('#response_data').html(result.html);
                    $('#exampleModal').modal('show');
                },
                complete: function () {
                    $.unblockUI();
                }
            });
        });
    });
</script>
<script>
    $(document).on('click', '.chocobox_detail', function () {
        var pids = $(this).data('pids');
        var box_type = $(this).data('box_type');
        var box_id = $(this).data('box_id');
        showCustomLoader();
        $.ajax({
            type: "POST",
            url: base_url + 'customize/getChocoboxDetail',
            data: {'ProductIDs': pids, 'box_type': box_type, 'box_id': box_id},
            success: function (result) {
                hideCustomLoader();
                $('#ChocoboxDetailModalDescription').html(result);
                $('#ChocoboxDetailModal').modal('show');
            }
        });
    });
</script>