<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('add') . ' ' . lang($ControllerName); ?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url(); ?>cms/<?php echo $ControllerName; ?>/action" method="post"
                              onsubmit="return false;" class="form_data" enctype="multipart/form-data"
                              data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">


                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title"><?php echo lang('title'); ?></label>
                                        <input type="text" name="Title" required class="form-control" id="Title">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="quantity">Quantity</label>
                                        <input type="text" name="quantity" required  class="form-control number-with-decimals" id="quantity">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="RibbonSku">Length
                                          <!--   <small>(No. of pieces Ribbon can have in it)</small> -->
                                        </label>
                                        <input type="text" name="dimension_length" required class="form-control"
                                               id="dimension_length">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="dimension_width">Width</label>
                                        <input type="text" name="dimension_width" required class="form-control number-with-decimals" id="dimension_width">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="dimension_height">Height</label>
                                        <input type="text" name="dimension_height" required class="form-control number-with-decimals" id="dimension_height">
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="dimension_weight">Weight</label>
                                        <input type="text" name="dimension_weight" required class="form-control number-with-decimals" id="dimension_weight">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="inner_dimension_length">Inner Length</label>
                                        <input type="text" name="inner_dimension_length" required class="form-control number-with-decimals" id="inner_dimension_length">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="inner_dimension_weight">Inner Weight</label>
                                        <input type="text" name="inner_dimension_weight" required class="form-control number-with-decimals" id="inner_dimension_weight">
                                    </div>
                                </div>

                                <div class="col-md-6 checkRibbon-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive"
                                                       checked/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label for="ShapeImage">Ribbon Image</label>
                                        <input type="file" id="ShapeImage" name="ShapeImage" title="Select Ribbon Image">
                                    </div>
                                </div>
                            </div>
                            


                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit'); ?>
                                </button>
                                <a href="<?php echo base_url(); ?>cms/<?php echo $ControllerName; ?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back'); ?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>