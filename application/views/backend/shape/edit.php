<?php

$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if (!empty($languages)) {
    foreach ($languages as $key => $language) {
        $common_fields = '';
        $common_fields2 = '';
        if ($key == 0) {

            $common_fields2 = '
                                
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="quantity">Quantity</label>
                                        <input type="text" name="quantity" required  class="form-control number-with-decimals" id="quantity" value="' . $result[$key]->quantity . '">
                                    </div>
                                </div>

                                 <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="dimension_length">Length</label>
                                        <input type="text" name="dimension_length" required  class="form-control" id="dimension_length" value="' . $result[$key]->dimension_length . '">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="dimension_width">Width</label>
                                        <input type="text" name="dimension_width" required  class="form-control number-with-decimals" id="dimension_width" value="' . $result[$key]->dimension_width . '">
                                    </div>
                                </div>
                               
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="dimension_height">Height</label>
                                        <input type="text" name="dimension_height" required class="form-control number-with-decimals" id="dimension_height" value="' . $result[$key]->dimension_height . '">
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="dimension_weight">Weight</label>
                                        <input type="text" name="dimension_weight" required class="form-control number-with-decimals" id="dimension_weight" value="' . $result[$key]->dimension_weight . '">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="inner_dimension_length">Inner Length</label>
                                        <input type="text" name="inner_dimension_length" required class="form-control number-with-decimals" id="inner_dimension_length" value="' . $result[$key]->inner_dimension_length . '">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="inner_dimension_weight">Inner Weight</label>
                                        <input type="text" name="inner_dimension_weight" required class="form-control number-with-decimals" id="inner_dimension_weight" value="' . $result[$key]->inner_dimension_weight . '">
                                    </div>
                                </div>

                                <div class="col-sm-4 checkRibbon-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" ' . ((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '') . '/> ' . lang('is_active') . '
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                ';

            $common_fields = '<div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <img src="' . base_url() . $result[$key]->ShapeImage . '" width="200" height="200">
                                    </div>        
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label for="ShapeImage">Shape Image</label>
                                        <input type="file" id="ShapeImage" name="ShapeImage" title="Select Shape Image">
                                                </div>
                                            </div>
                                        </div>';
        }

        $lang_tabs .= '<li class="' . ($key == 0 ? 'active' : '') . '">
                                        <a href="#' . $language->SystemLanguageTitle . '" data-toggle="tab">
                                            ' . $language->SystemLanguageTitle . '
                                        </a>
                                  </li>';


        $lang_data .= '<div class="tab-pane ' . ($key == 0 ? 'active' : '') . '" id="' . $language->SystemLanguageTitle . '">
                      <form action="' . base_url() . 'cms/' . $ControllerName . '/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="' . base64_encode($language->SystemLanguageID) . '">
                                                    <input type="hidden" name="' . $TableKey . '" value="' . base64_encode($result[0]->$TableKey) . '">
                                                    <input type="hidden" name="IsDefault" value="' . $language->IsDefault . '">

                                                   
                                                    <div class="row">
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title' . $key . '">' . lang('title') . '</label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title' . $key . '" value="' . ((isset($result[$key]->Title)) ? $result[$key]->Title : '') . '">
                                                                <input type="hidden" name="Title_old" value="' . ((isset($result[$key]->Title)) ? $result[$key]->Title : '') . '">
                                                               
                                                            </div>
                                                        </div>
                                                        
                                                         ' . $common_fields2 . '
                                                    </div>
                                                    ' . $common_fields . '
                                                   
                                                    

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            ' . lang('submit') . '
                                                        </button>
                                                        <a href="' . base_url() . 'cms/' . $ControllerName . '">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         ' . lang('back') . '
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';


    }
}


?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Edit <?php echo $result[0]->Title; ?> </h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-2">
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                            </div>
                            <div class="col-md-10">
                                <div class="tab-content">
                                    <?php echo $lang_data; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>