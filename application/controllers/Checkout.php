<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkFrontendSession();
        $this->load->model([
            'Temp_order_model',
            'Semsa_shipment_model',
            'Semsa_shipment_text_model']);
        $this->load->model('User_address_model');
        $this->load->model('Tax_shipment_charges_model');
        $this->load->model('Order_model');
        $this->load->model('Product_model');
        $this->data['language'] = $this->language;

    }

    public function index()
    {

        redirect(base_url('address'));//login change to that page
        if (isset($_REQUEST['p']) && $_REQUEST['p'] == 'checkout') {
            $this->checkout_address();
        }

        $this->data['cart_items'] = $this->Temp_order_model->getCartItems($this->UserID, $this->language);
        if (empty($this->data['cart_items'])) {
            redirect('product');
        }
        $this->data['address'] = $this->User_address_model->getAddresses("user_address.IsDefault = 1 AND user_address.UserID = " . $this->UserID);
        $this->data['address_for_payment_collection'] = $this->User_address_model->getAddresses("user_address.UseForPaymentCollection = 1 AND user_address.UserID = " . $this->UserID);

        // checking here if collect from store cookie is not set and default address and payment collection address are not found then set flash message to select address properly and redirect to same page again
        if (!isset($_COOKIE['CollectFromStore'])) {
            if (empty($this->data['address']) || empty($this->data['address_for_payment_collection'])) {
                $this->session->set_flashdata('message', 'You must select address for delivery and payment collection in order to proceed with checkout');
                redirect(base_url('address'));
            }
        }

        $this->data['available_cities'] = $this->Product_model->getCities(false,$this->language);

        $this->data['view'] = 'frontend/checkout-payment';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function checkout_address()
    {
        $this->data['cart_items'] = $this->Temp_order_model->getCartItems($this->UserID, $this->language);
        if (empty($this->data['cart_items'])) {
            redirect(base_url());
        }
        $this->data['address'] = $this->User_address_model->getAddresses("user_address.IsDefault = 1 AND user_address.UserID = " . $this->UserID);
        $this->data['view'] = 'frontend/checkout-address';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function thank_you($order_id = '')
    {
        $this->session->unset_userdata('order_coupon');
        $this->session->unset_userdata('DeliveryStoreID');
        $this->session->unset_userdata('DeliveryStoreTitle');
        $order = new Order_model();
        // $this->data['order'] = $this->Order_model->getUserLastOrderDetails($this->UserID, $this->language);
        if($order_id == ''){
            $where = "orders.UserID = " . $this->UserID;
        }else{
            $where = "orders.OrderID = " . base64_decode($order_id);
        }
        


        $this->data['order'] = $order->getOrders($where, 1, 0, $this->language, 'DESC')[0];
        //$this->data['payment_address'] = $this->User_address_model->getAddresses("user_address.AddressID = " . $this->data['order']->AddressIDForPaymentCollection)[0];
        $this->data['view'] = 'frontend/checkout-done';
        $this->load->view('frontend/layouts/default', $this->data);
    }
    public function pay_tab($val)
    {
        //$this->session->unset_userdata('order_coupon');
        //$this->data['order'] = $this->Order_model->getUserLastOrderDetails($this->UserID, $this->language);
        /*$order_id = base64_decode($order_id);
        $order = new Order_model();
        $where = "orders.OrderID = " . $order_id ;
        $this->data['order'] = $order->getOrders($where, 1, 0, $this->language, 'DESC')[0];
        if(empty($this->data['order'])){
            redirect(base_url());
        
        }*/
        //print_rm( $this->session->userdata('StoreIDOrder'));die;
        $total_amount = base64_decode($val);
        $order = new Temp_order_model();
        $this->data['order'] = $order->getCartItems($this->UserID, $this->language);
        $this->data['order']['TotalAmount'] = $total_amount;
//print_rm($this->data);die;
        if(empty($this->data['order'])){
            redirect(base_url());
        
        }
        //if($this->data['order']->TransactionID != ''){
        //print_rm($this->session->userdata);
        //print_rm($this->data['order']);
        $this->data['view'] = 'frontend/checkout-payment-paytab';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function changeShipmentMethod()
    {
        $ShipmentMethodID = $this->input->post('ShipmentMethodID');
        $this->session->set_userdata('ShipmentMethodIDForBooking', $ShipmentMethodID);
         $this->unsetSemsaShipmentMethod();
        $where = "tax_shipment_charges.TaxShipmentChargesID = " . $ShipmentMethodID;
        $shipment_method = $this->Tax_shipment_charges_model->getAllJoinedData(false, 'TaxShipmentChargesID', $this->language, $where);
        $response['message'] = lang('shipment_method_changed');
        $response['amount'] = @$shipment_method[0]->Amount;
        echo json_encode($response);
        exit();
    }
    public function changeSemsaShipmentMethod()
    {
        $ShipmentMethodID = $this->input->post('ShipmentMethodID');
        /*if(!$this->session->userdata('SemsaShipmentID')){
            $this->unsetSemsaShipmentMethod();
        }else{*/
        if(!$this->session->userdata('SemsaShipmentID')){
            $this->session->set_userdata('SemsaShipmentID', trim($ShipmentMethodID, ' '));
        }
        //}
        $where = "semsa_shipment.IsActive = 1";
        $shipment_method = $this->Semsa_shipment_model->getAllJoinedDataWithTable('semsa_shipment',false, 'SemsaShipmentID', $this->language, $where);
        $response['message'] = lang('shipment_method_changed');
        $response['Title'] = @$shipment_method[0]->Title;
        $response['amount'] = @findSemsaShippingCharges();
        $response['FifteenKgPrice'] = @$shipment_method[0]->FifteenKgPrice;
        $response['AdditionalKgPrice'] = @$shipment_method[0]->AdditionalKgPrice;
        echo json_encode($response);
        exit();
    }
    public function unsetShipmentMethod()
    {
        
        $this->session->unset_userdata('ShipmentMethodIDForBooking', 0);
        
    }
    public function unsetSemsaShipmentMethod()
    {
        
        $this->session->unset_userdata('SemsaShipmentID');
        
    }
    public function unsetShipmentMethodID()
    {
       $this->session->unset_userdata('ShipmentMethodIDForBooking', 0);
        $this->unsetSemsaShipmentMethod();
        echo true;
        
    }


    public function changePaymentMethod()
    {
        $PaymentMethod = $this->input->post('PaymentMethod');
        $this->session->set_userdata('PaymentMethodForBooking', $PaymentMethod);
        $response['message'] = lang('payment_method_changed');
        echo json_encode($response);
        exit();
    }

    public function changeDeliveryStoreID()
    {
        $StoreID = $this->input->post('StoreID');
        $StoreTitle = $this->input->post('StoreTitle');
        $this->session->set_userdata('DeliveryStoreID', $StoreID);
        $this->session->set_userdata('DeliveryStoreTitle', $StoreTitle);
        $data = getCustomRow("Select * from stores where StoreID=". $StoreID);
        $response['payment_method'] = explode(',',$data['PaymentID']);
        $response['shipping_method'] = explode(',',$data['ShippingID']);
        $response['message'] = lang('delivery_store_id_selected');
        echo json_encode($response);
        exit();
    }

    public function unsetDeliveryStoreID()
    {
        
        $this->session->unset_userdata('DeliveryStoreID');
        $this->session->unset_userdata('DeliveryStoreTitle');
        $response['message'] = lang('payment_method_changed');
        echo json_encode($response);
        exit();
        
    }

}