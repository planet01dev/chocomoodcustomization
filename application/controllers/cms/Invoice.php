<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->model('Order_model');
        $this->data['language'] = $this->language;


    }

    public function index()
    {
        $this->data['invoices'] = $this->Order_model->getOrders();
        $this->data['view'] = 'backend/invoice/manage';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function download($OrderID)
    {
        $order_html = get_order_invoice($OrderID);
        generate_pdf($order_html, $OrderID);
    }

}