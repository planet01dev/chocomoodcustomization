<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->Model('User_model');
        $this->load->Model('User_text_model');
        $this->load->Model('City_model');
        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['TableKey'] = 'UserID';
        $this->data['Table'] = 'users';
    }

    public function index()
    {
        $this->data['users'] = $this->User_model->getJoinedData(false, 'UserID', 'users.RoleID = 3');
        $this->data['technicians'] = $this->User_model->getJoinedData(false, 'UserID', 'users.RoleID = 2');
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function sendNotification()
    {
        $post_data = $this->input->post();
        $user_type = $post_data['UsersType'];
        $title = $post_data['Title'];
        $message = $post_data['Message'];
        $notification_type = $post_data['NotificationType'];
        $users = false;

        if ($user_type == 'Stores') {
            $users = $this->User_model->getJoinedData(false, 'UserID', 'users.RoleID = 2'); // Store Warehouse User RoleID = 2
        } elseif ($user_type == 'Drivers') {
            $users = $this->User_model->getJoinedData(false, 'UserID', 'users.RoleID = 3'); // Driver User RoleID = 3
        } elseif ($user_type == 'Admins') {
            $users = $this->User_model->getJoinedData(false, 'UserID', 'users.RoleID = 4'); // Store Admin User RoleID = 4
        } elseif ($user_type == 'Customers') {
            $users = $this->User_model->getJoinedData(false, 'UserID', 'users.RoleID = 5'); // Customer User RoleID = 5
        }

        if ($users && count($users) > 0) {
            foreach ($users as $user) {
                if ($notification_type == 'SMS') {
                    if ($user->Mobile != '') {
                        $sms_text = $title . "\n" . $message;
                        // $sms_text = "Dear " . $user->FullName . "\n" . $message;
                        sendSms($user->Mobile, $sms_text);
                    }
                } elseif ($notification_type == 'Email') {
                    if ($user->Email != '') {
                        $data['to'] = $user->Email;
                        $data['subject'] = $title;
                        // $message = "Dear " . $user->FullName . ",<br>" . $message;
                        $data['message'] = email_format($message);
                        sendEmail($data);
                    }
                }
            }
        }

        $success['error'] = false;
        $success['success'] = 'Notifications sent successfully';
        $success['redirect'] = false;
        echo json_encode($success);
        exit;
    }

}