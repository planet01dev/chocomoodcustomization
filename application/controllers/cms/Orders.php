<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use SmsaSDK\Smsa;

class Orders extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->model('User_model');
        $this->load->model('Order_model');
        $this->load->model('Order_item_model');
        $this->load->model('City_model');
        $this->load->model('Model_general');
        $this->load->model('User_address_model');
        $this->load->model('Order_extra_charges_model');
        $this->load->model('Model_general');
        $this->data['language'] = $this->language;

        // https://stackoverflow.com/questions/31721254/codeigniter-initialize-function-not-working
    }


    public function email_test(){
        $data['to'] = 'sarfraz.cs10@gmail.com';
        $data['subject'] = 'Chocomood';
        $data['message'] = 'This is the formate email from chocomood';
        sendEmail($data);
    }

    public function index()
    {

        $this->data['view'] = 'backend/orders/manage';
        $where = '';


        if ($this->session->userdata['admin']['RoleID'] == 2 OR $this->session->userdata['admin']['RoleID'] == 4) {
            $where = ' AND user_address.CityID = ' . $this->session->userdata['admin']['CityID'];
            if($this->session->userdata['admin']['RoleID'] == 4){
                $where = ' AND orders.StoreID = ' . $this->session->userdata['admin']['StoreID'];
            }
        } elseif ($this->session->userdata['admin']['RoleID'] == 3) {
            $where = ' AND orders.DriverID = ' . $this->session->userdata['admin']['UserID'];

        }

        $post_data = $this->input->post();

        if(isset($post_data['OrderTrackID']) && $post_data['OrderTrackID'] != ''){
            $where .= ' AND orders.OrderNumber = '.$post_data['OrderTrackID'];
        }

        if(isset($post_data['From']) && isset($post_data['To']) && $post_data['From'] != '' && $post_data['To'] != ''){
            $where .= ' AND DATE(orders.CreatedAt)  BETWEEN "'.$post_data['From'].'" AND "'.$post_data['To'].'"';
        }


         if(isset($post_data['Email']) && $post_data['Email'] != ''){
            $where .= ' AND users.Email = "'.$post_data['Email'].'"';
        }


         $where .= ' AND orders.Hide = 0';


        $this->data['post_data'] = $post_data;



       $this->data['order_statuses'] = $this->Model_general->getAll('order_statuses', false, 'ASC', 'OrderStatusID');

        if (isset($_GET['status']) && $_GET['status'] !== '')
        {
            $status = $_GET['status'];
        } else {
            $status = 'pending';
        }

        if ($status == 'pending')
        {
            $this->data['orders'] = $this->Order_model->getOrders("orders.Status = 1 $where", false, 0, $this->language, 'DESC');

        } elseif ($status == 'packed')
        {
            $this->data['orders'] = $this->Order_model->getOrders("orders.Status = 2 $where", false, 0, $this->language, 'DESC');
        } elseif ($status == 'dispatched')
        {
            $this->data['orders'] = $this->Order_model->getOrders("orders.Status = 3 $where", false, 0, $this->language, 'DESC');
        } elseif ($status == 'delivered')
        {
            $this->data['orders'] = $this->Order_model->getOrders("orders.Status = 4 $where", false, 0, $this->language, 'DESC');
        } elseif ($status == 'cancelled')
        {
            $this->data['orders'] = $this->Order_model->getOrders("orders.Status = 5 $where", false, 0, $this->language, 'DESC');
        } elseif ($status == 'unopened')
        {
            $this->data['orders'] = $this->Order_model->getOrders("orders.IsRead = 0 $where", false, 0, $this->language, 'DESC');
        } elseif ($status == 'cancelled_not_collect')
        {
            $this->data['orders'] = $this->Order_model->getOrders("orders.CollectFromStore = 1 AND orders.Status = 5 $where", false, 0, $this->language, 'DESC');
        } elseif ($status == 'all')
        {
            $this->data['orders'] = $this->Order_model->getOrders("orders.OrderID > 0 $where", false, 0, $this->language, 'DESC');
        } else {
            $this->data['orders'] = $this->Order_model->getOrders("orders.Status = 1 $where", false, 0, $this->language, 'DESC');
            $status = 'pending';
        }

        //echo $this->db->last_query();exit;
        //print_rm($this->data['orders']);exit;

        $this->data['url_status'] = $status;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function view($order_id)
    {
        $this->Order_model->update(array('IsRead' => 1), array('OrderID' => $order_id));
        $language = 'EN';
        $this->data['view'] = 'backend/orders/view';
        $where = 'cities.IsActive = 1 AND system_languages.ShortCode = "' . $language . '"';
        $this->data['cities'] = $this->City_model->getJoinedData(false, 'CityID', $where);
        $this->data['order_statuses'] = $this->Model_general->getAll('order_statuses', false, 'ASC', 'OrderStatusID');
        $this->data['order'] = $this->Order_model->getOrders("orders.OrderID = $order_id");
        $this->data['order_extra_charges'] = $this->Order_extra_charges_model->getMultipleRows(array('OrderID' => $order_id));
        $this->data['payment_address'] = $this->User_address_model->getAddresses("user_address.AddressID = " . $this->data['order'][0]->AddressIDForPaymentCollection);
        $this->data['order_items'] = getOrderItems($this->data['order'][0]->OrderID);
        $data['language'] = $this->language;
        //print_rm($this->data);exit;

        $this->load->view('backend/layouts/default', $this->data);
    }


    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'delete':
                $this->delete();
                break;
            case 'assign_driver':
                $this->assignDriver();
                break;
        }
    }

    public function get_drivers()
    {

        $this->data['OrderID'] = $this->input->post('OrderID');
        $order_info = $this->Order_model->getOrders('orders.OrderID = ' .$this->data['OrderID']);
        
        $city  = @$order_info[0]->CityID;

        $this->data['users'] = $this->User_model->getBackendUsers('users.RoleID = 3 AND users.CityID = '.$city);

        $html = $this->load->view('backend/orders/assign_driver_popup', $this->data, true);

        $response = array();
        $response['html'] = $html;
        echo json_encode($response);


    }

    public function assignDriver()
    {
        $update = array();
        $update_by = array();
        $update_by['OrderID'] = $this->input->post('OrderID');
        $update['DriverID'] = $this->input->post('UserID');
        
       
        //print_rm($order_info);
        $update['DeliveryOTP'] = RandomString();
        $this->Order_model->update($update, $update_by);

        $order_info = $this->Order_model->getOrders('orders.OrderID = ' . $update_by['OrderID']);
        $order_info = $order_info[0];
        $order_info->OTP =  $update['DeliveryOTP'];


        $file_name = $this->generateQRCode($order_info->OTP,$this->input->post('OrderID'));
        $this->sendDeliveryOtpSMSToCustomer($order_info);
        $this->sendDeliveryOtpToCustomer($order_info,$file_name);


        

        
        if ($order_info->DriverID > 0) {
            
            $this->sendOrderAssignedSMSToDriver($order_info);
        }
        $success['error'] = false;
        $success['success'] = lang('update_successfully');
        $success['reload'] = true;
        // $success['url'] = 'cms/' . $this->router->fetch_class() . '/view/' . $OrderID;
        echo json_encode($success);
    }

    public function generateQRCode($info,$OrderID){
        $this->load->library('phpqrcode/qrlib');
        $SERVERFILEPATH = FCPATH.'uploads/images/qrcode/';
        
        $text = $info;
        $text1= substr($text, 0,9);
        
        $folder = $SERVERFILEPATH;
        $file_name1 = $OrderID."-Qrcode.png";
        $file_name = $folder.$file_name1;
        QRcode::png($text,$file_name);
        return $file_name;
    }

    public function get_otp()
    {

        $this->data['OrderID'] = $this->input->post('OrderID');

        
        $html = $this->load->view('backend/orders/otp_pop_up', $this->data, true);

        $response = array();
        $response['html'] = $html;
        echo json_encode($response);


    }

    public function update_status_with_otp()
    {

        $this->data['OrderID'] = $this->input->post('OrderID');
        $this->data['OTP'] = $this->input->post('OTP');

        $OrderData = $this->Order_model->get($this->data['OrderID'],true,'OrderID');

        if($OrderData['DeliveryOTP'] == $this->data['OTP']){
            $this->Order_model->update(array('Status' => 4),array('OrderID' => $this->data['OrderID']));
            $success['error'] = false;
            $success['success'] = lang('update_successfully');
            $success['reload'] = TRUE;
            // $success['url'] = 'cms/' . $this->router->fetch_class() . '/view/' . $OrderID;
            echo json_encode($success);
        }else{
            $success['error'] = 'Delivery OTP is not valid';
            $success['success'] = false;
           
           
            echo json_encode($success);

        }

        

        



    }
    public function update()
    {
        $post_data = $this->input->post(); // OrderID, Status
        if (isset($post_data['OrderID'])) {
            $order = $this->Order_model->get($post_data['OrderID'], false, 'OrderID');
            if ($order->DriverID > 0) {

                if($post_data['Status'] == '5') //cancelled
                {
                    if($order->SemsaShippingAmount != NULL || $order->SemsaShippingAmount != '' || $order->SemsaShippingAmount <= 0)
                    {
                        $result = Smsa::key('cCm@2982');
                        Smsa::nullValues(''); 

                        $shipmentData = [
                                'awbNo' => $order->AWBNumber, 
                                'reas' => 'SA', 
                            ];
                        $shipment = Smsa::cancelShipment($shipmentData);
                    }
                }
                $this->Order_model->update(array('Status' => $post_data['Status']), array('OrderID' => $post_data['OrderID']));
                $order_info = $this->Order_model->getOrders('orders.OrderID = ' . $post_data['OrderID']);
                $order_info = $order_info[0];
                $this->SendStatusChangedSmsToCustomer($order_info);
                $this->SendStatusChangedEmailToCustomer($order_info);
                $this->sendStatusChangedSMSToDriver($order_info);
                $this->sendStatusChangedEmailToDriver($order_info);
                $this->sendStatusChangedEmailToStoreAdmin($order_info);
                $success['error'] = false;
                $success['success'] = lang('update_successfully');
                echo json_encode($success);
                exit;
            } else {
                $errors['error'] = "Please first assign this order to a driver";
                $errors['success'] = false;
                echo json_encode($errors);
                exit;
            }
        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }

    private function SendStatusChangedEmailToStoreAdmin($order_info)
    {
        if ($order_info->StoreAdminEmail !== '') {
            $email_template = get_email_template(10);
            $subject = $email_template->Heading;
            $message = $email_template->Description;
            $message = str_replace("{{name}}", $order_info->StoreAdminFullName, $message);
            $message = str_replace("{{order_no}}", $order_info->OrderNumber, $message);
            $message = str_replace("{{order_status}}", $order_info->OrderStatusEn, $message);
           
            $data['to'] = $order_info->StoreAdminEmail;
            $data['subject'] = $subject;
            $data['message'] = email_format($message);
            sendEmail($data);
        }
    }

    public function invoice($OrderID)
    {

        $order_details = $this->Order_model->getOrders("orders.OrderID = $OrderID");
        $data['order'] = $order_details[0];
        //print_rm($data['order']);
        $data['language'] = $this->language;
        $data['barcode']  = generateBarcode($OrderID, "barcode", $type = 'image');
        $order_html = $this->load->view('frontend/invoice', $data, true);
        echo $order_html;
        exit();
        /*$order_html = get_order_invoice($OrderID, 'print');
        echo $order_html;
        exit();*/
    }

    private function delete()
    {
        if (!checkUserRightAccess(71, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;

            echo json_encode($errors);
            exit;
        }
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->$child->delete($deleted_by);
        $this->$parent->delete($deleted_by);
        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }

    private function SendStatusChangedSmsToCustomer($order_info)
    {
        if (isset($order_info->Mobile) && $order_info->Mobile != '') {
            if ($order_info->Status == 4) // Delivered order
            {
                $msg = "Dear " . $order_info->FullName . "\nYour order with order # " . $order_info->OrderNumber . " is delivered. Thank you for choosing Chocomood.";
            } else {
                $msg = "Dear " . $order_info->FullName . "\nStatus of your Order # " . $order_info->OrderNumber . " is changed to " . $order_info->OrderStatusEn . ".";
            }
            sendSms($order_info->Mobile, $msg);
        }
    }

    private function SendStatusChangedEmailToCustomer($order_info)
    {
        if ($order_info->Email !== '') {
            $email_template = get_email_template(5);
            $subject = $email_template->Heading;
            $message = $email_template->Description;
            $message = str_replace("{{name}}", $order_info->FullName, $message);
            $message = str_replace("{{order_no}}", $order_info->OrderNumber, $message);
            $message = str_replace("{{order_status}}", $order_info->OrderStatusEn, $message);
            $data['to'] = $order_info->Email;
            $data['subject'] = $subject;
            $data['message'] = email_format($message);
            sendEmail($data);
        }
    }

    private function sendStatusChangedSMSToDriver($order_info)
    {
        if (isset($order_info->AssignedDriverMobile) && $order_info->AssignedDriverMobile != '') {
            $msg = "Dear " . $order_info->AssignedDriverName . "\nStatus of your assigned Order # " . $order_info->OrderNumber . " is changed to " . $order_info->OrderStatusEn . ".";
            sendSms($order_info->AssignedDriverMobile, $msg);
        }
    }

    private function sendStatusChangedEmailToDriver($order_info)
    {
        if ($order_info->AssignedDriverEmail !== '') {
            $email_template = get_email_template(6);
            $subject = $email_template->Heading;
            $message = $email_template->Description;
            $message = str_replace("{{name}}", $order_info->AssignedDriverName, $message);
            $message = str_replace("{{order_no}}", $order_info->OrderNumber, $message);
            $message = str_replace("{{order_status}}", $order_info->OrderStatusEn, $message);
            $data['to'] = $order_info->AssignedDriverEmail;
            $data['subject'] = $subject;
            $data['message'] = email_format($message);
            sendEmail($data);
        }
    }

    private function sendOrderAssignedSMSToDriver($order_info)
    {

        if (isset($order_info->AssignedDriverMobile) && $order_info->AssignedDriverMobile != '') {

            $msg = "Dear " . $order_info->AssignedDriverName . "\nAn order is assigned to you with Order # " . $order_info->OrderNumber . " at Chocomood.";

            sendSms($order_info->AssignedDriverMobile, $msg);
        }

    }


    private function sendDeliveryOtpToCustomer($order_info,$file_name = false)
    {
        if ($order_info->Email !== '') {
            $email_template = get_email_template(7);
            $subject = $email_template->Heading;
            $message = $email_template->Description;
            $message = str_replace("{{name}}", $order_info->FullName, $message);
            $message = str_replace("{{OTP}}", $order_info->OTP, $message);
            $message = str_replace("{{order_number}}", $order_info->OrderNumber, $message);
            $data['to'] = $order_info->Email;
            $data['subject'] = $subject;
            $data['message'] = email_format($message);
            sendEmail($data,$file_name);
        }
    }

    private function sendDeliveryOtpSMSToCustomer($order_info)
    {
        if (isset($order_info->Mobile) && $order_info->Mobile != '') {
           // $msg = "Dear " . $order_info->AssignedDriverName . "\nAn order is assigned to you with Order # " . $order_info->OrderNumber . " at Chocomood.";
            $msg = "Your order # " . $order_info->OrderNumber . " is assigned to the delivery boy with delivery OTP ".$order_info->OTP.". Please give this OTP to the delivery boy who gives you parcel.";
            sendSms($order_info->Mobile, $msg);
        }
    }

}