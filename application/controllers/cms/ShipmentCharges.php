<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ShipmentCharges extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();
        checkAdminSession();
        $this->load->model('User_model');
        $this->load->model('Order_model');
        $this->load->model('Shipping_method_model');
        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();


    }

    public function index()
    {
        $this->data['view'] = 'backend/shipment_charges/manage';
        $this->data['data'] = $this->Shipping_method_model->getShippingMethod(true);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function edit($id = '')
    {
        if (!checkUserRightAccess(84, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $this->data['result'] = $this->Shipping_method_model->getShippingMethod(true, $id);


        if (!$this->data['result']) {
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $this->data['view'] = 'backend/shipment_charges/edit';
        $this->data['ShippingMethodID'] = $id;
        $this->load->view('backend/layouts/default', $this->data);

    }

    public function update()
    {
        $post_data = $this->input->post();        

        unset($post_data['form_type']);
        $update_by = array();
        $update_by['ShippingMethodID'] = $post_data['ShippingMethodID'];
        unset($post_data['form_type']);
        $this->Shipping_method_model->update($post_data, $update_by);

        $success['error'] = false;
        $success['success'] = 'Updated Successfully';
        $success['redirect'] = true;
        $success['url'] = 'cms/ShipmentCharges/';
        echo json_encode($success);
        exit;    
    }




}