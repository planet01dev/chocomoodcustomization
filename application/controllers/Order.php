<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use SmsaSDK\Smsa;
class Order extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();
        checkFrontendSession();
        $this->load->model('User_model');
        $this->load->model('Order_model');
        $this->load->model('Order_item_model');
        $this->load->model('Temp_order_model');
        $this->load->model('User_address_model');
        $this->load->model('Order_extra_charges_model');
        $this->load->model('Coupon_model');
        $this->load->model('Product_model');
        $this->load->model('Store_model');
        $this->load->model('Product_availability_model');
        $this->data['language'] = $this->language;

    }

    public function placeOrder()
    {
        $data['cart_items'] = $this->Temp_order_model->getCartItems($this->UserID, $this->language);
        if (empty($data['cart_items'])) {
            $response['message'] = lang('no_items_in_basket');
            $response['redirect'] = true;
            $response['url'] = 'cart';
            echo json_encode($response);
            exit();
        }
        $order_data['UserID'] = $this->UserID;
        $show_error = false;
        $error_message = '';
        $model_html = '';
        $show_model = false;

        foreach($data['cart_items'] as $product){
            if($product->PriceType == 'kg'){
                $product_data = $this->Product_model->getStoreAvailability('product_store_availability.StoreID = '.$_GET['StoreID'].' AND products.ProductID = '.$product->ProductID, $this->language, 1);

            }else{
             $product_data = $this->Product_model->getStoreAvailability('product_store_availability.StoreID = '.$_GET['StoreID'].' AND products.ProductID = '.$product->ProductID);
            }
            //echo $this->db->last_query();
            
            if($product->PriceType == 'kg'){
                    $quantity = $product->Quantity * 1000;
                    $unit = ($this->language == 'AR' ? 'غرام' : 'Grams');
                }else{
                    $quantity = $product->Quantity;
                    $unit = lang('pcs');
                }

            $product_image = get_images($product->ProductID, 'product',false);
            if($product_data && $product->Quantity <= $product_data['AvailableQuantity']){

                
                
                $model_html .= '<div class="cat-choc">
                                <div class="row">
                                    <div class="col-md-2">
                                        <img src="'.base_url($product_image).'" class="chcimg img-fliud rounded mx-auto d-block"> 
                                    </div>
                                    <div class="col-md-7">
                                        <div class="txt-detail">
                                            <p class="chc-nam">
                                            <strong> '.$product->Title.'<br> '.$quantity.' '.$unit.'</strong>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="txt-price">
                                            <p>'.($product->Quantity * $product->TempItemPrice).' '.lang('SR').'</p>
                                        </div>
                                    </div>
                                </div>
                            </div>';
            }elseif($product_data && $product_data['AvailableQuantity'] <  $product->Quantity && $product_data['AvailableQuantity'] > 0){
                $show_model = true;
                $this->Temp_order_model->update(array('Quantity' => $product_data['AvailableQuantity']),array('TempOrderID' => $product->TempOrderID));
                $model_html .= '<div class="cat-choc">
                                <div class="row">
                                    <div class="col-md-2">
                                        <img src="'.base_url($product_image).'" class="chcimg img-fliud rounded mx-auto d-block"> 
                                    </div>
                                    <div class="col-md-7">
                                        <div class="txt-detail">
                                            <p class="chc-nam">
                                            <strong>'.$product->Title.' <br> <span class="red">'.$quantity.' '.$unit.'</span> <span class="green">'.$product_data['AvailableQuantity'].' '.lang('pcs').' '.lang('only').' </span></strong>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="txt-price">
                                            <p><span class="red">'.($product->Quantity * $product->TempItemPrice).' SR</span></p>
                                            <p><span class="green">'.($product_data['AvailableQuantity'] * $product->TempItemPrice).' SR</span></p>
                                            <button href="javascript:void(0);" onclick="removeIt(\'cart/removeFromCart\', \'TempOrderID\', '.$product->TempOrderID.');" class="crs-btn btn-dark"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>';
            }elseif($product_data && $product_data['AvailableQuantity'] == 0){
                $show_model = true;
                $this->Temp_order_model->delete(array('TempOrderID' => $product->TempOrderID));
                 $model_html .= '<div class="cat-choc">
                                <div class="row">
                                    <div class="col-md-2">
                                        <img src="'.base_url($product_image).'" class="chcimg img-fliud rounded mx-auto d-block"> 
                                    </div>
                                    <div class="col-md-7">
                                        <div class="txt-detail">
                                            <p class="chc-nam">
                                            <strong>'.$product->Title.' <br> <span class="red">'.$quantity.' '.$unit.'</span> </strong>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="txt-price">
                                            <p><span class="green">'.lang('Sold_out').'</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>';
            }elseif(!$product_data){
                $show_model = true;
                $this->Temp_order_model->delete(array('TempOrderID' => $product->TempOrderID));
                 $model_html .= '<div class="cat-choc">
                                <div class="row">
                                    <div class="col-md-2">
                                        <img src="'.base_url($product_image).'" class="chcimg img-fliud rounded mx-auto d-block"> 
                                    </div>
                                    <div class="col-md-7">
                                        <div class="txt-detail">
                                            <p class="chc-nam">
                                            <strong>'.$product->Title.' <br> <span class="red">'.$quantity.' '.$unit.'</span> </strong>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="txt-price">
                                            <p><span class="green">'.lang('Sold_out').'</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>';

            }


            

        }

        if($show_model){
            $response['model_html'] = $model_html;
            $response['show_model'] = true;
            echo json_encode($response);
            exit();

        }

        $paymentMethod = $_GET['paymentMethod'];
        $online_pay = ($paymentMethod == "cod")? 0 : 1;
        /*$order = $this->orderEntry($_GET, $online_pay);
        if($order == false){
            $response['message'] = lang('something_went_wrong');
            $response['redirect'] = false;
            $response['show_model'] = false;
            echo json_encode($response);
            exit();
        }else{
            if($paymentMethod == "cod"){
                 $order['url'] = 'checkout/thank_you/'.base64_encode($order['insert_id']);
                 echo json_encode($order);
                 exit();
            }else if($paymentMethod == 'paytab'){
                $order['url'] = 'checkout/pay_tab/'.base64_encode($order['insert_id']);
                echo json_encode($order);
                exit();
            }
        }*/

        if($paymentMethod == "cod"){
            $order = $this->orderEntry($_GET, $online_pay);
            if($order == false){
                $response['message'] = lang('something_went_wrong');
                $response['redirect'] = false;
                $response['show_model'] = false;
                echo json_encode($response);
                exit();
            }else{
                $order['url'] = 'checkout/thank_you/'.base64_encode($order['insert_id']);
                echo json_encode($order);
                exit(); 
            }

        }else if($paymentMethod == 'paytab'){
            $order['url'] = 'checkout/pay_tab/'.base64_encode($_GET['total_price_val']);
            $this->session->set_userdata('StoreIDOrder', $_GET['StoreID']);
            $order['message'] = lang('thank_you_for_order');
            $order['redirect'] = true;
            $order['show_model'] = false;
            echo json_encode($order);
            exit();
        }
       
  
       
    }

    public function orderEntry($GET, $online_pay = 0){
         $order_data['UserID'] = $this->UserID;
         $data['cart_items'] = $this->Temp_order_model->getCartItems($this->UserID, $this->language);
         if (isset($_COOKIE['CollectFromStore']) && $_COOKIE['CollectFromStore'] == 1) {
            $order_data['CollectFromStore'] = 1;
            //$user_city_id = $this->session->userdata['admin']['CityID'];
        } else {
            $order_data['CollectFromStore'] = 0;
            $fetch_address_by['UserID'] = $this->UserID;
            $fetch_address_by['IsDefault'] = 1;
            $address = $this->User_address_model->getWithMultipleFields($fetch_address_by);
            $order_data['AddressID'] = $address->AddressID;
            $user_city_id = $address->CityID;

            // saving address to collect payment
            /*$fetch_payment_address_by['UserID'] = $this->UserID;
            $fetch_payment_address_by['UseForPaymentCollection'] = 1;
            $address_for_payment = $this->User_address_model->getWithMultipleFields($fetch_payment_address_by);
            if ($address_for_payment) {
                $order_data['AddressIDForPaymentCollection'] = $address_for_payment->AddressID;
            } else {
                $order_data['AddressIDForPaymentCollection'] = $address->AddressID;
            }*/

            $order_data['ShipmentMethodID'] = ($this->session->userdata('ShipmentMethodIDForBooking'))? $this->session->userdata('ShipmentMethodIDForBooking') : 0;
        }
        //$order_data['BranchDeliveryDistrictID'] = $_GET['BranchDeliveryDistrictID'];
        $order_data['StoreID'] = $GET['StoreID'];
        $order_data['CreatedAt'] = date('Y-m-d H:i:s');
        $order_data['OrderNumber'] = time() . $this->UserID;
        $order_data['PaymentMethod'] = $this->session->userdata('PaymentMethodForBooking');
        $insert_id = $this->Order_model->save($order_data);
        if ($insert_id > 0) {
            $response['insert_id'] = $insert_id;
            // $this->sendThankyouEmailToCustomer($insert_id);
            $get_store_data = $this->Store_model->get($GET['StoreID'],false,'StoreID');
            $order_item_data = array();
            $total_amount = 0;
            foreach ($data['cart_items'] as $product) {
                $order_item_data[] = [
                    'OrderID' => $insert_id,
                    'ProductID' => $product->ProductID,
                    'Quantity' => $product->Quantity,
                    'Amount' => $product->TempItemPrice,
                    'ItemType' => $product->ItemType,
                    'CustomizedOrderProductIDs' => $product->CustomizedOrderProductIDs,
                    'CustomizedShapeImage' => $product->CustomizedShapeImage,
                    'CustomizedBoxID' => $product->CustomizedBoxID,
                    'IsCorporateItem' => $product->IsCorporateItem,
                    'Ribbon' => $product->Ribbon,
                    'package_id' => $product->Package
                ];
                $total_amount += $product->Quantity * $product->TempItemPrice;

                // increase product purchase count here
                $product_dt_for_purchase_count = $this->Product_model->get($product->ProductID, false, 'ProductID');
                $PurchaseCount = (int)$product_dt_for_purchase_count->PurchaseCount + (int)$product->Quantity;
                $this->Product_model->update(array('PurchaseCount' => $PurchaseCount), array('ProductID' => $product->ProductID));


                //update product quantity

                if($get_store_data){

                    $availability_product = $this->Product_availability_model->getWithMultipleFields(array('StoreID' => $get_store_data->StoreID,'ProductID' => $product->ProductID));
                    if($availability_product){
                        if($product->PriceType == 'kg'){
                            $quantity = $availability_product->GramQuantity - ($product->Quantity * $product->package_weight);
                            $this->Product_availability_model->update(array('GramQuantity' => $quantity),array('StoreID' => $get_store_data->StoreID,'ProductID' => $product->ProductID));
                        }else{
                            $quantity = $availability_product->Quantity - $product->Quantity;
                            $this->Product_availability_model->update(array('Quantity' => $quantity),array('StoreID' => $get_store_data->StoreID,'ProductID' => $product->ProductID));
                        }

                    }

                }


            }

            if ($this->session->userdata('order_coupon')) {
                $order_coupon = $this->session->userdata('order_coupon');
                $coupon_code = $order_coupon['CouponCode'];
                $coupon_discount_percentage = $order_coupon['DiscountPercentage'];
                $coupon_discount_availed = ($order_coupon['DiscountPercentage'] / 100) * $total_amount;
                $total_amount = $total_amount - $coupon_discount_availed;

                // reducing coupon usage count
                $coupon_detail = $this->Coupon_model->getWithMultipleFields(array('CouponID' => $order_coupon['CouponID']), true);
                $update['UsageCount'] = $coupon_detail['UsageCount'] - 1;
                $update_by['CouponID'] = $order_coupon['CouponID'];
                $this->Coupon_model->update($update, $update_by);
            } else {
                $coupon_code = '';
                $coupon_discount_percentage = 0;
                $coupon_discount_availed = 0;
            }

            $ShipmentMethodIDForBooking = $this->session->userdata('ShipmentMethodIDForBooking');
            $shipping_amount = 0;
            $semsa_shipping_amount = 0;
            $freeshipment = false;
            
            $total_tax = 0;
            $taxes = getTaxShipmentCharges('Tax');
            foreach ($taxes as $tax) {
                $tax_id = $tax->TaxShipmentChargesID;
                $tax_title = $tax->Title;
                $tax_factor = $tax->Type == 'Fixed' ? number_format($tax->Amount, 2) . ' SAR' : $tax->Amount . '%';
                if ($tax->Type == 'Fixed') {
                    $tax_amount = $tax->Amount;
                } elseif ($tax->Type == 'Percentage') {
                    // $tax_amount = ($tax->Amount / 100) * ($total_amount + $shipping_amount + $semsa_shipping_amount);
                    $tax_amount = ($tax->Amount / 100) * ($total_amount);
                }
                $total_tax += $tax_amount;
                $order_extra_charges['OrderID'] = $insert_id;
                $order_extra_charges['TaxShipmentChargesID'] = $tax_id;
                $order_extra_charges['Title'] = $tax_title;
                $order_extra_charges['Factor'] = $tax_factor;
                $order_extra_charges['Amount'] = $tax_amount;
                $this->Order_extra_charges_model->save($order_extra_charges);
            }
            $get_total =  $total_amount + $total_tax;
            if($order_data['CollectFromStore'] != 1){
                $shipment_method = getSelectedShippingMethodDetail($ShipmentMethodIDForBooking, $this->language);
                if ($shipment_method) {
                    $freeshipment = true;
                    $shipping_id = $shipment_method->TaxShipmentChargesID;
                    $shipping_title = $shipment_method->Title;
                    $shipping_factor = $shipment_method->Type == 'Fixed' ? number_format($shipment_method->Amount, 2) . ' SAR' : $shipment_method->Amount . '%';
                    if ($shipment_method->Type == 'Fixed') {
                        $shipping_amount = $shipment_method->Amount;
                    } elseif ($shipment_method->Type == 'Percentage') {
                        $shipping_amount = ($shipment_method->Amount / 100) * ($total_amount);
                    }
                    $apply_free_shipping = false;
                    if ($get_total >= getShipmentMaxAmount() && getShipmentMaxAmount() != 0){
                        $shipping_factor = freeShippingTitle();
                        $shipping_amount = 0.00;
                    }
                    $order_extra_charges['OrderID'] = $insert_id;
                    $order_extra_charges['TaxShipmentChargesID'] = $shipping_id;
                    $order_extra_charges['Title'] = $shipping_title;
                    $order_extra_charges['Factor'] = $shipping_factor;
                    $order_extra_charges['Amount'] = $shipping_amount;
                    $this->Order_extra_charges_model->save($order_extra_charges);
                }
               
            }

            if($freeshipment && $get_total >= getShipmentMaxAmount() && getShipmentMaxAmount() != 0){
                $shipping_amount = 0; 
            }
            $total_amount = $total_amount + $shipping_amount + $total_tax + $semsa_shipping_amount;

            $order_data['CouponCodeUsed'] = $coupon_code;
            $order_data['CouponCodeDiscountPercentage'] = $coupon_discount_percentage;
            $order_data['DiscountAvailed'] = $coupon_discount_availed;
            $order_data['TotalShippingCharges'] = $shipping_amount;
            $order_data['TotalTaxAmount'] = $total_tax;
            $order_data['TotalAmount'] = $total_amount;
            $order_data['OrderNumber'] = str_pad($insert_id, 5, '0', STR_PAD_LEFT);
            $this->Order_item_model->insert_batch($order_item_data);
            $this->Order_model->update($order_data, array('OrderID' => $insert_id));

            //semsa
            $awb = "";
            if($order_data['CollectFromStore'] != 1){
             if($this->session->userdata('SemsaShipmentID') && $this->session->userdata('SemsaShipmentID') == 'semsa'){
                    $semsa_shipping_amount = findSemsaShippingCharges();
                    $total_amount =  number_format(($total_amount + $semsa_shipping_amount), 2);
                    $amount = ($online_pay != 1)? $total_amount : 0;
                    $semsa = $this->semsa_shipment(['amount' => $amount]);
                    if($semsa['status'] == 'DATA RECEIVED'){
                        $this_data['TotalAmount'] = $total_amount;
                        $this_data['TotalShippingCharges'] = $semsa_shipping_amount;
                        $this_data['AWBNumber'] = $semsa['awbNumber'];
                        $this_data['ShippedThroughApi'] = 1;
                        $awb = $semsa['awbNumber'];
                        $this_data['SemsaShippingAmount'] = $semsa_shipping_amount;
                        $this->Order_model->update($this_data, array('OrderID' => $insert_id));
                        $this->session->unset_userdata('SemsaShipmentID');
                    }
                }
            }

            //if($online_pay != 1){
                $deleted_by['UserID'] = $this->UserID;
                $this->Temp_order_model->delete($deleted_by);
                $this->sendOrderConfirmationToCustomer($insert_id, $awb);
           // }
            $order_data_pusher = $this->Order_model->get($insert_id, true, 'OrderID');
            pusher($order_data_pusher, "Chocomood_Order_Channel", "Chocomood_Order_Event");
           // $response['message'] = lang('payment_page_redirect');
            $response['message'] = lang('thank_you_for_order');
            $response['redirect'] = true;
            $response['show_model'] = false;
            $response['insert_id'] = $insert_id;
            
            //$response['url'] = 'order/paymentMethod/'.base64_encode($insert_id);
           
            //$response['url'] = 'checkout/pay_tab/'.base64_encode($insert_id);
           // $response['url'] = 'checkout/thank_you/'.base64_encode($insert_id);
            return $response;
           
        } else {
            return false;
            
        }
    }


    public function paymentMethod($id){
        $this->data['id'] = $id;
        $this->data['paytab'] = 'checkout/pay_tab/'.$id;
        $this->data['cod'] = 'checkout/thank_you/'.$id;
        $this->data['view'] = 'frontend/payment_method';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function updatePaymentStatus(){
        $GET['StoreID'] = $this->session->userdata('StoreIDOrder');
        $response = $this->orderEntry($GET, 1);
        if($response == false){
            $response['message'] = lang('something_went_wrong');
            $response['redirect'] = false;
            $response['show_model'] = false;
            echo json_encode($response);
            return false;
        }

        //if(isset($_REQUEST['transaction_id']) && isset($_REQUEST['order_id'])){
        if(isset($_REQUEST['transaction_id']) && isset($response['insert_id'])){
            $this->Order_model->update(array('TransactionID' => $_REQUEST['transaction_id'],'Hide' => 0, 'PaymentMethod' => 'PayTab'),array('OrderID' => $response['insert_id']));
            $deleted_by['UserID'] = $this->UserID;
            $this->Temp_order_model->delete($deleted_by);
            //$this->sendOrderConfirmationToCustomer($insert_id);
            $this->sendOrderConfirmationToCustomer($response['insert_id']);
            $order_data_pusher = $this->Order_model->get($response['insert_id'], true, 'OrderID');
            pusher($order_data_pusher, "Choco0mood_Order_Channel", "Chocomood_Order_Event");
           
            $this->session->set_flashdata('message', lang('thank_you_for_order'));
            $this->session->set_flashdata('message_type','info');
            redirect(base_url('checkout/thank_you/'.base64_encode($response['insert_id'])));

        }
    }

    public function semsa_shipment($data){
        $result = Smsa::key('cCm@2982');
        Smsa::nullValues(''); 
        $user = $this->session->userdata('user');
        $fetch_address_by['UserID'] = $this->UserID;
        $fetch_address_by['IsDefault'] = 1;
        $address = $this->User_address_model->getWithMultipleFields($fetch_address_by);
        if($this->language == 'EN'){
            $lang_id = 1;
        }else{
            $lang_id = 2;
        }
        $city_data = getCustomRow("Select * from cities_text where CityID = ".$address->CityID." AND SystemLanguageID = ".$lang_id);
        $shipmentData = [
                'refNo' => 'my_app_name' . time(), // shipment reference in your application
                'cName' =>  $user->FullName , // customer name
                'cntry' => 'SA', // shipment country
                'cCity' => $city_data['Title'], // shipment city, try: Smsa::getRTLCities() to get the supported cities
                'cMobile' =>  $user->Mobile, // customer mobile
                'cAddr1'  =>  $address->Street, // customer address
                'cAddr2'  =>  $address->Street, // customer address 2
                'shipType' => 'DLV', // shipment type
                'PCs' => 1, // quantity of the shipped pieces
                'cEmail' => $user->Email, // customer email
                'codAmt' => $data['amount'], // payment amount if it's cash on delivery, 0 if not cash on delivery
                'weight' => findTotalCartWeight(), // pieces weight
                'itemDesc' => 'Thank You for choosing our product.', // extra description will be printed
            ];

        /** @var SmsaSDK\Methods\addShipmentResponse $shipment */
        $shipment = Smsa::addShipment($shipmentData);
        $awbNumber = $shipment->getAddShipmentResult();

        //echo "shipment AWB: " . $awbNumber ;
        //echo "\r\n";

        $status = Smsa::getStatus(['awbNo' => $awbNumber])->getGetStatusResult();
        $data['status'] = $status;
        $data['awbNumber'] = $awbNumber;
        //echo "shipment Status: " . $status;
        //die();
        return $data;
    }

    public function CancelPaymentPaymentOfOrder(){
       /* if(isset($_REQUEST['order_id'])){

            $this->Order_item_model->delete(array('OrderID' => $_REQUEST['order_id']));
            $this->Order_model->delete(array('OrderID' => $_REQUEST['order_id']));           

        }*/
        $this->session->set_flashdata('message', lang('Transaction_Failed'));
        $this->session->set_flashdata('message_type','danger');
        redirect(base_url('cart'));
    }

    public function sendOrderConfirmationToCustomer($OrderID, $awb = "")
    {
        $user = $this->User_model->getJoinedData(false, 'UserID', "users.UserID = " . $this->UserID);
        //print_rm($user);
        $order = $this->Order_model->get($OrderID, false, 'OrderID');
        if ($user) {
            // sending email
            if ($user[0]->Email != '') {
                $data['to'] = $user[0]->Email;
                $data['subject'] = ($user[0]->PreferredLang == 0)? 'Order received at chocomood' : 'تم استلام الطلب في chocomood';
                $data['message'] = get_order_invoice($OrderID, 'print');
                //print_rm($data);
               // $data['message'] = get_order_invoice_new($OrderID);
                sendEmail($data);
            }

            // sending sms
            if ($user[0]->Mobile != '') {
                $msg = ($user[0]->PreferredLang == 0)? "Dear " : "العزيز ";
                $tag = ($user[0]->PreferredLang == 0)? "Your order is placed successfully with order" : "تم وضع طلبك بنجاح مع الطلب";
                $msg .=  $user[0]->FullName . ", ".$tag." # " . $order->OrderNumber . "\n";
                if($awb != ""){
                     $msg .=  "Tracking ID # " . $awb . "\n";

                }
                //$msg .= ($user[0]->PreferredLang == 0)? "Invoice" : "فاتورة";
                //$msg .= ": " . base_url() . "page/invoice/" . base64_encode($OrderID);
                sendSms($user[0]->Mobile, $msg);
            }
            $admin = $this->User_model->getBackendUsers('users.StoreID = 1 AND users.RoleID = 4 ');
            if (!empty($admin)) {
               foreach($admin as $v){
                    if ($v->Mobile != '') {
                        $msg = ($v->PreferredLang == 0)? "Dear Admin" : "عزيزي المشرف";
                        $tag = ($v->PreferredLang == 0)? "an order is placed at your " : "يتم وضع الطلب على";
                        $with = ($v->PreferredLang == 0)? " with" : "مع";
                        $msg .=  /*$v->FullName*/ ", ".$tag." ".$v->StoreTitle.$with." # " . $order->OrderNumber . "\n";
                        //$msg .= ($v->PreferredLang == 0)? "Invoice" : "فاتورة";
                        //$msg .= ": " . base_url() . "page/invoice/" . base64_encode($OrderID);
                        sendSms($v->Mobile, $msg);
                    }
               } 
            }
        }
    }

    public function cancelOrder()
    {
        $OrderID = $this->input->post('OrderID');
        $this->Order_model->update(array('Status' => 5), array('OrderID' => $OrderID));
        $this->sendOrderCancellationToAdmin($OrderID);
        $response['message'] = lang('order_cancelled');
        $response['redirect'] = true;
        $response['url'] = 'account/profile?p=orders';
        echo json_encode($response);
        exit();
    }

    private function sendOrderCancellationToAdmin($OrderID)
    {
        $admin_users = $this->User_model->getJoinedData(false, 'UserID', "users.RoleID = 1");
        $order = $this->Order_model->get($OrderID, false, 'OrderID');
        if ($admin_users) {
            foreach ($admin_users as $user) {
                // sending email
                if ($user->Email != '') {
                    $data['to'] = $user->Email;
                    $data['subject'] = 'Order cancelled at chocomood';
                    $data['message'] = email_format("Dear " . $user->FullName . ", an order is cancelled by user with order # " . $order->OrderNumber);
                    sendEmail($data);
                }

                // sending sms
                if ($user->Mobile != '') {
                    $msg = "Dear " . $user->FullName . ", an order is cancelled by user with order # " . $order->OrderNumber;
                    sendSms($user[0]->Mobile, $msg);
                }
            }
        }
    }

}