$(document).ready(function () {
    tinymce.init({
        forced_root_block: "",
        selector: 'textarea',
        extended_valid_elements: 'span',
        plugins: "code,textcolor",
        theme: "modern",
        mode: "exact",
        menu: {
            file: {title: 'File', items: 'newdocument'},
            edit: {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall'},
            insert: {title: 'Insert', items: 'link media | template hr'},
            view: {title: 'View', items: 'visualaid'},
            table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
            tools: {title: 'Tools', items: 'spellchecker code'}
        },
        toolbar: "forecolor,backcolor, undo,redo,cleanup,bold,italic,underline,strikethrough,separator,"
            + "justifyleft,justifycenter,justifyright,justifyfull,"
            + "bullist,numlist,outdent,indent",
        theme_advanced_toolbar_location: "top",
        theme_advanced_buttons1: "bold,italic,underline,strikethrough,separator,"
            + "justifyleft,justifycenter,justifyright,justifyfull,"
            + "bullist,numlist,outdent,indent",
        theme_advanced_buttons2: "link,unlink,anchor,image,separator,"
            + "undo,redo,cleanup,code,separator,sub,sup,charmap",
        theme_advanced_buttons3: "",
        height: "250px",
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        }
    });
});
