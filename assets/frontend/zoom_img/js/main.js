
var selector            = document.getElementById("thumbnails"),     // parent of product gallary
    selectorImgs        = "",
    mainSelector        = document.getElementById("main"),           // image view
    quantityCount       = document.getElementById("quantity-count"), // quantity input
    quantityIncrease    = document.getElementById("increase"),       // increase quantity   
    quantityDecrease    = document.getElementById("decrease"),       // decrease quantity
    defaultQuantity     = 1;                                         // default quantity
    if(document.getElementById("thumbnails")){
        selectorImgs    = selector.getElementsByTagName("img");     // all gallary
    }
    


/**
* this loop for product gallaries 
* if click any element will show it in view section
*/
for (let i = 0; i < selectorImgs.length; i++) {
    // get all images
    let img = selectorImgs[i]
    // add event click for all images  
    selectorImgs[0].classList = "rounded p-1 w-100 h-auto transtion active-product-img"
    img.addEventListener("click", function () {
        // get default image selected
        var currentSelector            = document.getElementsByClassName("active-product-img");
        // set view src equal image src which you clicked
        mainSelector.src               = this.src;
        // set view data-zoom equal image src which you clicked
        mainSelector.dataset.zoom      = this.src;
        // remove active from default image selected 
        currentSelector[0].className   = currentSelector[0].className.replace(" active-product-img", " ");
        // add class list for image you will click
        this.classList                 = "rounded p-1 w-100 h-auto transtion active-product-img";
    })
};

/**
* this function to increase button value ( increase the amount  of weight product )
*/


// quaantity input = 1
if(quantityCount!= null){
    quantityCount.value = defaultQuantity;

    // when click increase button 
    quantityIncrease.onclick = () =>  {
        // if default quantity > 10 
        // if(defaultQuantity < 10 ){
            // increase default quantity 
            defaultQuantity++
            // and set this value in quantity input
            return quantityCount.value = defaultQuantity
            
        //else
        // }else{
        //     // default wuantity = 10
        //     defaultQuantity = 10
        //     //and set this value in quantity input
        //     return quantityCount.value = defaultQuantity
        // }
    }


    /**
    * this function to decrease button value ( decrease the amount  of weight product )
    */

    // when click decrease button 
    quantityDecrease.onclick = () =>  {
        // if default quantity > 1 
        if(defaultQuantity >  1){
            // decrease default quantity 
            defaultQuantity--
            // and set this value in quantity input
            return quantityCount.value = defaultQuantity
        //else
        }else{
            // default wuantity = 1
            defaultQuantity = 1
            //and set this value in quantity input
            return quantityCount.value = defaultQuantity
        }
    }

}






